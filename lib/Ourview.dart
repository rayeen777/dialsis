import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:woocommerce/Services.dart';
import 'package:woocommerce/WebView.dart';
import 'package:woocommerce/src/app.dart';
import 'package:woocommerce/src/blocs/home_bloc.dart';
import 'package:woocommerce/src/models/UserRepo.dart';
import 'package:woocommerce/src/models/category_model.dart';
import 'package:woocommerce/src/models/customer_model.dart';
import 'package:woocommerce/src/ui/accounts/account/account.dart';
import 'package:woocommerce/src/ui/home/drawer/drawer.dart';
import 'package:woocommerce/src/ui/home/search.dart';
import 'package:woocommerce/src/ui/options.dart';
import 'package:woocommerce/src/ui/products/product_detail/product_detail.dart';
import 'package:woocommerce/src/ui/products/product_list.dart';
import 'package:html/parser.dart';
import 'dart:async';
import 'src/ui/accounts/language/app_localizations.dart';


class OurView extends StatefulWidget {
  final HomeBloc homeBloc;
  final GalleryOptions options;
  final ValueChanged<GalleryOptions> handleOptionsChanged;
  final Customer user;
  OurView({Key key, this.homeBloc, this.options, this.handleOptionsChanged,this.user}) : super(key: key);

  @override
  _OurViewState createState() => _OurViewState();
}

class _OurViewState extends State<OurView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    final List imgList = [
      'http://live.maktechgroup.in/services/imageslider/ads.png',
      'http://live.maktechgroup.in/services/imageslider/ads1.png',
    ];

    return Scaffold(
      key: _scaffoldKey,
//      drawer:Drawer(
//        child:  Container(
//          padding: EdgeInsets.only(top: 16.0),
//          color: Colors.black87,
//          child: CustomScrollView(
//            slivers: <Widget>[
//              SliverList(
//                  delegate: SliverChildListDelegate(
//                      [
//                        Container(
//                          padding:EdgeInsets.only(top: 12),
//                          child: ListTile(
//                            title: Container(
//                                height: 40,
//                                child: Row(
//                                  mainAxisAlignment: MainAxisAlignment.start,
//                                  children: <Widget>[
//                                    Text(
//                                      'MY',
//                                      style: TextStyle(
//                                        fontWeight: FontWeight.w800,
//                                        fontFamily: 'Lexend_Deca',
//                                        letterSpacing: 0.5,
//                                        color: Colors.cyan,
//                                        fontSize: 20,
//                                      ),),
//                                    Text(
//                                      ' STORE',
//                                      style: TextStyle(
//                                          fontWeight: FontWeight.w800,
//                                          fontFamily: 'Lexend_Deca',
//                                          letterSpacing: 0.5,
//                                          color: Colors.yellow,
//                                          fontSize: 20
//                                      ),),
//                                  ],
//                                )
//                            ),
//                            leading: Padding(
//                              padding: const EdgeInsets.only(top: 2.0),
//                              child: Container(
//                                decoration: BoxDecoration(
//                                    color: Colors.cyan,
//                                    borderRadius: BorderRadius.all(Radius.circular(2.0))
//                                ),
//                                width: 30,
//                                height: 30,
//                                child: Icon(FontAwesomeIcons.shoppingBasket, size: 20, color: Colors.orangeAccent[100]),
//                              ),
//                            ),
//                            subtitle: Padding(
//                              padding: const EdgeInsets.only(bottom: 8.0),
//                              child: Text(
//                                  '5% Discount on all orders',
//                                  style: TextStyle(
//                                      fontWeight: FontWeight.w800,
//                                      fontFamily: 'Lexend_Deca',
//                                      letterSpacing: 0.5,
//                                      color: Colors.grey,
//                                      fontSize: 12
//                                  )
//                              ),
//                            ),
//                            onTap: () {
//                              // Update the state of the app
//                              // ...
//                              // Then close the drawer
//                              Navigator.pop(context);
//                            },
//                          ),
//                        ),
//                        Divider(
//                          color: Colors.grey,
//                          thickness: 0.4,
//                          height: 3,
//                        ),
//                      ]
//                  )
//              ),
//              SliverList(
//                delegate: SliverChildListDelegate(
//                    [
//                      ListTile(
//                        leading: _buildLeadingIcon(icon: FontAwesomeIcons.comment, bgColor: Colors.white, iconColor: Colors.red),
//                        title: Text("chat",
//                            style: menuItemStyle()),
//                        onTap: () => _openWhatsApp(
//                            '+917007229963','How may i Help you'),
//                      ),
//                      ListTile(
//                        leading: _buildLeadingIcon(icon: Icons.person, bgColor: Colors.white, iconColor: Colors.red),
//                        title: Text("my_account",
//                            style: menuItemStyle()),
//                        onTap: () {
//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                builder: (context) =>
//                                    UserAccount(options: widget.options,
//                                        handleOptionsChanged: widget.handleOptionsChanged,
//                                        homeBloc: widget.homeBloc),
//                              ));
//                        },
//                      ),
//                      ListTile(
//                        leading: _buildLeadingIcon(icon: Icons.call, bgColor: Colors.red, iconColor: Colors.white),
//                        title: Text("customer_support",
//                            style: menuItemStyle()),
//                        onTap: () => _callNumber(
//                            '+917007229963'),
//                      ),
//                      Divider(
//                        color: Colors.grey,
//                        thickness: 0.4,
//                        height: 3,
//                      ),
//                      ListTile(
//                        leading: _buildLeadingIcon(icon: Icons.home, bgColor: Colors.red, iconColor: Colors.white),
//                        title: Text("home",
//                            style: menuItemStyle()),
//                        onTap: () {
//                          Navigator.pop(context);
//                        },
//                      ),
//                      ListTile(
//                        leading: _buildLeadingIcon(icon: FontAwesomeIcons.tag, bgColor: Colors.orangeAccent, iconColor: Colors.white),
//                        title: Text("on_sale",
//                            style: menuItemStyle()),
//                        onTap: () {
//                          var filter = new Map<String, dynamic>();
//                          filter['on_sale'] = '1';
//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (context) => ProductList(
//                                      filter: filter,
//                                      name: AppLocalizations.of(context).translate("on_sale"),
//                                      homeBloc: widget.homeBloc)));
//                        },
//                      ),
//                      ListTile(
//                        leading: _buildLeadingIcon(icon: Icons.lens, bgColor: Colors.lightGreen, iconColor: Colors.white),
//                        title: Text("new",
//                            style: menuItemStyle()),
//                        onTap: () {
//                          var filter = new Map<String, dynamic>();
////                          Navigator.push(
////                              context,
////                              MaterialPageRoute(
////                                  builder: (context) => ProductList(
////                                      filter: filter,
////                                      name: AppLocalizations.of(context).translate("new"),
////                                      homeBloc: widget.homeBloc)));
//                        },
//                      ),
//                      ListTile(
//                        leading: _buildLeadingIcon(icon: FontAwesomeIcons.shoppingBasket, bgColor: Colors.red, iconColor: Colors.white),
//                        title: Text("favourite_store",
//                            style: menuItemStyle()),
//                        onTap: () {
////                          _onTapTag(tag: 'favourite', name: AppLocalizations.of(context).translate("favourite_store"));
//                        },
//                      ),
//                      Divider(
//                        color: Colors.grey,
//                        thickness: 0.4,
//                        height: 3,
//                      ),
//                    ]
//                ),
//              ),
////              SliverList(
////                delegate: SliverChildBuilderDelegate(
////                      (BuildContext context, int index) {
////                    return _buildTile(mainCategories[index]);
////                  },
////                  childCount: mainCategories.length,
////                ),
////              ),
//              SliverToBoxAdapter(
//                child: Divider(
//                  color: Colors.grey,
//                  thickness: 0.4,
//                  height: 3,
//                ),
//              )
//            ],
//          ),
//        ),
//      ),
    drawer: MyDrawer(homeBloc: widget.homeBloc, options: widget.options, handleOptionsChanged: widget.handleOptionsChanged,user : widget.user),
      appBar: AppBar(
        leading: Builder(
            builder: (BuildContext context){
              return IconButton(
                icon: Icon(Icons.menu,color: Colors.black),
                onPressed: () =>_scaffoldKey.currentState.openDrawer(),
              );
            }),
        title: Text( "Dial SIS",
        style: TextStyle(color: Colors.black),
        ),


        backgroundColor: Colors.white,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.message,color: Colors.black),
            onPressed: () => _openWhatsApp(
                '+917007229963','Hi I need Help From DialSIS'),

          ),

          IconButton(
            icon: Icon(Icons.person_outline,color: Colors.black),

            onPressed: () async {
              var user = await UserRepo().getUser();
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        UserAccount(options: widget.options,
                            handleOptionsChanged: widget.handleOptionsChanged,
                            homeBloc: widget.homeBloc,user: user,),
                  ));
            },
    ),


        ],
      ),
      body: SingleChildScrollView(
        child: Container(


          child: Column(

            children: <Widget>[

                  Hero(
                    tag: 'logo',
                    child: new Image.asset(
                      'lib/assets/images/dlogo.png',
                      width: 200,
                    ),
                  ),

//


              new Container(

                padding: const EdgeInsets.all(10.0),
                child : Container(
                    width: 320,
                    padding: EdgeInsets.all(10.0),
                    child: TextField(
                      autocorrect: true,
                      decoration: InputDecoration(
                        hintText: 'May I help you',
                        suffixIcon: Icon(Icons.mic),
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          borderSide: BorderSide(color: Colors.green, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.green, width: 2),
                        ),
                      ),
                      onTap: () {
                        showSearch(
                            context: context,
                            delegate: SearchProducts(onProductClick,'INR'));

                      },
                    )
                    )
              ),

              Container(
                height: 128,
                width: 300,
                child: new Swiper(
                  itemBuilder: (BuildContext context,int index){
                    return new Image.network(imgList[index],
                      fit: BoxFit.fill,);
                  
                    
                    
                  },
                  itemCount: imgList.length.toInt(),
                  pagination: new SwiperPagination(),
                
                ),
              ),
//              new Image.asset(
//                'lib/assets/images/ads.png',
//
//              ),
              SizedBox(height: 15.0),
              Center(
                child: Column(
                  children: <Widget>[
                    new Row(

                      children: <Widget>[
                        new Padding(
                          padding: const EdgeInsets.only(right: 15.0),
                        ),
                        InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>App2(
                              homeBloc: widget.homeBloc,
                              options: widget.options,
                              onOptionsChanged: widget.handleOptionsChanged,
                            )));
                          },
                          child: Image.asset("lib/assets/images/product.jpeg",
                          width: 150,
                            height: 150,
                          ),
                        ),
                        new Padding(
                          padding: const EdgeInsets.only(right: 20.0),
                        ),
                        InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>Service(
                              homeBloc: widget.homeBloc,options:widget.options,handleOptionsChanged: widget.handleOptionsChanged,
                            )));
                          },
                          child: Image.asset("lib/assets/images/services.jpeg",
                            width: 150,
                            height: 150,
                          ),
                        ),

                      ],

                    ),
                    SizedBox(height: 16.0,),
                    new Row(

                      children: <Widget>[
                        new Padding(
                          padding: const EdgeInsets.only(right: 15.0),
                        ),
                        InkWell(
                          onTap: () {
                            _openWhatsApp(
                                '+917007229963','I am looking for Books Delivery from dialsis');
                          },
                          child: Image.asset("lib/assets/images/nur.jpeg",
                            width: 150,
                            height: 150,
                          ),
                        ),
                        new Padding(
                          padding: const EdgeInsets.only(right: 20.0),
                        ),
                        InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>WebView()));
                          },
                          child: Image.asset("lib/assets/images/lib.jpeg",
                            width: 150,
                            height: 150,
                          ),
                        ),

                      ],

                    ),
                    SizedBox(height: 16.0,),
                    new Row(
                      children: <Widget>[
                        new Padding(
                          padding: const EdgeInsets.only(right: 15.0),
                        ),
                        InkWell(
                          onTap: ()  {
                            _openWhatsApp(
                                '+917007229963','I am looking for Medicine Delivery services from dialsis');
                          },
                          child: Image.asset("lib/assets/images/medicion.jpeg",
                            width: 150,
                            height: 150,
                          ),
                        ),
                        new Padding(
                          padding: const EdgeInsets.only(right: 20.0),
                        ),
                        InkWell(
                          onTap: () {
                            _openWhatsApp(
                                '+917007229963','I am looking for Job from dialsis');
                          },
                          child: Image.asset("lib/assets/images/job.jpeg",
                            width: 150,
                            height: 150,
                          ),
                        ),

                      ],


                    ),
                  ],
                ),

              )

            ],
          ),



        ),
      ),

    );


  }

  onProductClick(product) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ProductDetail(
        product: product,
        homeBloc: widget.homeBloc,
      );
    }));
  }


  Widget _buildTile(Category category) {
    return Padding(
      padding: EdgeInsets.all(0.0),
      child: Column(
        children: <Widget>[
          ListTile(
            onTap: () {
              _onTap(category);
            },
            leading: leadingIcon(category),
            trailing: Icon(Icons.arrow_forward_ios, size: 14, color: Colors.white,),
            title: Text(
                _parseHtmlString(category.name),
                style: menuItemStyle()
            ),
          ),
          _divider(context),
        ],
      ),
    );
  }

  _divider(BuildContext context) {
    return Divider(
      color: Theme.of(context).brightness == Brightness.light ? Colors.black45 : Colors.white,
      thickness: 0.2,
      height: 2,
      indent: 60,
    );
  }
  Container leadingIcon(Category category) {
    return Container(
      width: 30,
      height: 30,
      child: CachedNetworkImage(
        imageUrl: category.image != null ? category.image : '',
        imageBuilder: (context, imageProvider) => Card(
          clipBehavior: Clip.antiAlias,
          elevation: 0.0,
          margin: EdgeInsets.all(0.0),
          //shape: StadiumBorder(),
          child: Ink.image(
            child: InkWell(
              onTap: () {
                //onCategoryClick(category);
              },
            ),
            image: imageProvider,
            fit: BoxFit.cover,
          ),
        ),
        placeholder: (context, url) => Card(
          clipBehavior: Clip.antiAlias,
          elevation: 0.0,
          //shape: StadiumBorder(),
        ),
        errorWidget: (context, url, error) => Card(
          elevation: 0.0,
          color: Colors.white,
          //shape: StadiumBorder(),
        ),
      ),
    );
  }

  TextStyle menuItemStyle() {
    return TextStyle(
        fontWeight: FontWeight.w300,
        letterSpacing: 0.5,
        color: Colors.white,
        fontSize: 18
    );
  }

  _onTap(Category category) {
    var filter = new Map<String, dynamic>();
    filter['id'] = category.id.toString();
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductList(
                filter: filter,
                name: category.name,
                homeBloc: widget.homeBloc)));
  }

  _buildLeadingIcon({IconData icon, Color bgColor, Color iconColor}) {
    return Container(
        width: 30,
        height: 30,
        padding: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
            color: bgColor,
            borderRadius: BorderRadius.all(Radius.circular(2.0))
        ),
        child: Icon(icon, color: iconColor, size: 20, )
    );
  }

// ignore: non_constant_identifier_names
  Future _openWhatsApp(String number,String Text) async {


    final url = 'https://wa.me/' + number +'?text=' +Text;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _onTapTag({String tag, String name}) {
    var filter = new Map<String, dynamic>();
    filter['tag'] = tag;
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductList(
                filter: filter,
                name: name,
                homeBloc: widget.homeBloc)));
  }


  Future _callNumber(String number) async {
    final url = 'tel:' + number;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  String _parseHtmlString(String htmlString) {
    var document = parse(htmlString);

    String parsedString = parse(document.body.text).documentElement.text;

    return parsedString;
  }
}
