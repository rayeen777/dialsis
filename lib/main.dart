import 'package:flutter/material.dart';
import 'package:woocommerce/SplashWidget.dart';
import 'src/app.dart';
import 'src/blocs/home_bloc.dart';

void main() {
  final homeBloc = HomeBloc();
 runApp(App(homeBloc: homeBloc));

//  runApp(SplashWidget());

}

//void main() => runApp(MyApp());
//
//class MyApp extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return MaterialApp(
//      debugShowCheckedModeBanner: false,
//      title: 'Flutter Demo',
//      theme: ThemeData(
//        primarySwatch: Colors.blue,
//      ),
//      home: SplashWidget(),
//    );
//  }
//}