// Copyright 2018-present the Flutter authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


import 'package:flutter/foundation.dart' hide Category;
import 'package:scoped_model/scoped_model.dart';
import 'dart:convert';


import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../src/ui/scales.dart';
import '../../src/ui/themes/themes.dart';
import '../../src/ui/options.dart';
import '../../src/models/cart/cart_model.dart';
import '../../src/models/category_model.dart';
import '../../src/models/product_model.dart';
import '../../src/resources/api_provider.dart';

double _salesTaxRate = 0.06;
double _shippingCostPerItem = 7.0;

class AppStateModel extends Model {

  Locale _appLocale = Locale('en');
  GalleryOptions options;

  Locale get appLocal => _appLocale ?? Locale("en");
  fetchLocale() async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.getString('language_code') == null) {
      _appLocale = Locale('en');
    } else _appLocale = Locale(prefs.getString('language_code'));

    if (prefs.getString('is_dark') != null) {
      options = GalleryOptions(
        theme: prefs.getString('is_dark') == 'true' ? kDarkGalleryTheme: kLightGalleryTheme,
        platform: defaultTargetPlatform,
        textScaleFactor: kAllGalleryTextScaleValues[0],
      );
    } else {
      options = GalleryOptions(
        theme: kLightGalleryTheme,
        platform: defaultTargetPlatform,
        textScaleFactor: kAllGalleryTextScaleValues[0],
      );
    };

    notifyListeners();
    return Null;
  }


  void changeLanguage(Locale type) async {
    var prefs = await SharedPreferences.getInstance();
    if (_appLocale == type) {
      return;
    }
    if (type == Locale("ar")) {
      _appLocale = Locale("ar");
      await prefs.setString('language_code', 'ar');
      await prefs.setString('countryCode', '');
    } else {
      _appLocale = Locale("en");
      await prefs.setString('language_code', 'en');
      await prefs.setString('countryCode', 'US');
    }
    notifyListeners();
  }

  // All the available products.
  List<Product> _availableProducts;

  final apiProvider = ApiProvider();

  // The currently selected category of products.
  Category _selectedCategory;

  // The IDs and quantities of products currently in the cart.
  //Map<int, int> _productsInCart = {};
  CartModel shoppingCart = CartModel(
      cartContents: new List<CartContent>(), cartTotals: CartTotals());
  int count = 0;
  bool isCartLoading = false;

  //Map<int, int> get productsInCart => Map.from(_productsInCart);

  // Total number of items in the cart.
  //int get totalCartQuantity => _productsInCart.values.fold(0, (v, e) => v + e);

  Category get selectedCategory => _selectedCategory;

  // Totaled prices of the items in the cart.
  /*double get subtotalCost => _productsInCart.keys
      .map((id) => _availableProducts[id].price * _productsInCart[id])
      .fold(0.0, (sum, e) => sum + e);*/

  // Total shipping cost for the items in the cart.
  /*double get shippingCost =>
      _shippingCostPerItem *
      _productsInCart.values.fold(0.0, (sum, e) => sum + e);*/

  // Sales tax for the items in the cart
 // double get tax => subtotalCost * _salesTaxRate;

  // Total cost to order everything in the cart.
  //double get totalCost => subtotalCost + shippingCost + tax;


  // Adds a product to the cart.
  Future<void> addProductToCart(Product product) async {

    //***** This will give cart hang error, Do not uncomment this *****//
    /*if (!shoppingCart.cartContents.any((item) => item.productId == product.id)) {
      shoppingCart.cartContents.add(new CartContent(
          productId: product.id,
          name: product.name,
          quantity: 1,
          price: product.price.toDouble(),
          formattedPrice: product.formattedPrice,
          thumb: product.images[0].src));
    } else {
      shoppingCart.cartContents.singleWhere((content) => content.productId == product.id).quantity += 1;
    }*/
    isCartLoading = true;
    notifyListeners();
    _addToCart(product);
  }

  void _addToCart(product) async {
    var params = new Map<String, dynamic>();
    params['product_id'] = product.id.toString();
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-add_to_cart', params);
    if (response.statusCode == 200) {
      // On Success Add to cart
    } else {
      isCartLoading = false;
      notifyListeners();
      throw Exception('Failed to load cart');
    }
    getCart();
  }

  void getCart() async {
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-cart', Map());
    isCartLoading = false;
    notifyListeners();
    if (response.statusCode == 200) {
      shoppingCart = CartModel.fromJson(json.decode(response.body));
      notifyListeners();
      updateCartCount();
    } else {
      throw Exception('Failed to load cart');
    }
  }

  void updateCartCount() {
    count = 0;
    for (var i = 0; i < shoppingCart.cartContents.length; i++) {
      count = count + shoppingCart.cartContents[i].quantity;
    }
    isCartLoading = false;
    notifyListeners();
  }

  // Removes an item from the cart.
  void removeCartItem(CartContent cartContent) {
    if (shoppingCart.cartContents.contains(cartContent)) {
      shoppingCart.cartContents.remove(cartContent);
      if(cartContent.key != null)
      removeItemFromCart(cartContent.key);
    }
    updateCartCount();
  }

  Future<void> removeItemFromCart(String key) async {
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-remove_cart_item&item_key=' + key, Map());
  }

  // Returns the Product instance matching the provided id.
  Product getProductById(int id) {
    return _availableProducts.firstWhere((p) => p.id == id);
  }

  // Removes everything from the cart.
  void clearCart() {
    shoppingCart.cartContents.clear();
    notifyListeners();
  }

  void setCategory(Category newCategory) {
    _selectedCategory = newCategory;
    notifyListeners();
  }
}
