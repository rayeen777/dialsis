// To parse this JSON data, do
//
//     final blocksModel = blocksModelFromJson(jsonString);

import 'dart:convert';
import 'category_model.dart';
import '../../src/models/product_model.dart';
import 'customer_model.dart';

BlocksModel blocksModelFromJson(String str) => BlocksModel.fromJson(json.decode(str));

class BlocksModel {
  List<Block> blocks;
  List<Child> pages;
  Settings settings;
  Dimensions dimensions;
  List<Product> featured;
  List<Product> onSale;
  List<Category> categories;
  int maxPrice;
  String loginNonce;
  String currency;
  String language;
  List<Language> languages;
  List<Currency> currencies;
  bool status;
  List<Product> recentProducts;
  Customer user;


  BlocksModel({
    this.blocks,
    this.pages,
    this.settings,
    this.dimensions,
    this.featured,
    this.onSale,
    this.categories,
    this.maxPrice,
    this.loginNonce,
    this.currency,
    this.languages,
    this.currencies,
    this.status,
    this.recentProducts,
    this.user,
    this.language

  });

  factory BlocksModel.fromJson(Map<String, dynamic> json) => BlocksModel(
    blocks: json["blocks"] == null ? null : List<Block>.from(json["blocks"].map((x) => Block.fromJson(x))),
    recentProducts: json["recentProducts"] == null ? null : List<Product>.from(json["recentProducts"].map((x) => Product.fromJson(x))),
    pages: json["pages"] == null ? [] : List<Child>.from(json["pages"].map((x) => Child.fromJson(x))),
    settings: json["settings"] == null ? null : Settings.fromJson(json["settings"]),
    dimensions: json["dimensions"] == null ? null : Dimensions.fromJson(json["dimensions"]),
    featured: json["featured"] == null ? null : List<Product>.from(json["featured"].map((x) => Product.fromJson(x))),
    onSale: json["on_sale"] == null ? null : List<Product>.from(json["on_sale"].map((x) => Product.fromJson(x))),
    categories: json["categories"] == null ? null : List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
    maxPrice: json["max_price"] == null ? null : json["max_price"],
    loginNonce: json["login_nonce"] == null ? null : json["login_nonce"],
    currency: json["currency"] == null ? null : json["currency"],
    languages: json["languages"] == null ? [] : List<Language>.from(json["languages"].map((x) => Language.fromJson(x))),
    currencies: json["currencies"] == null ? [] : List<Currency>.from(json["currencies"].map((x) => Currency.fromJson(x))),
    user: json["user"] == null ? null : Customer.fromJson(json["user"]),
      language: 'en'
  );

}

class Block {
  int id;
  List<Child> children;
  List<Product> products;
  String title;
  String headerAlign;
  String titleColor;
  String style;
  String bannerShadow;
  String padding;
  String margin;
  int paddingTop;
  int paddingRight;
  int paddingBottom;
  int paddingLeft;
  int marginTop;
  int marginRight;
  int marginBottom;
  int marginLeft;
  String bgColor;
  String blockType;
  int linkId;
  double borderRadius;
  double childWidth;
  String blockBgColor;
  //String sort;
  String blockBlockType;
  String filterBy;
  double paddingBetween;
  //String blockChildWidth;
  double childHeight;
  double elevation;
  int itemPerRow;
  String saleEnds;
  int layoutGridCol;

  Block({
    this.id,
    this.children,
    this.products,
    this.title,
    this.headerAlign,
    this.titleColor,
    this.style,
    this.bannerShadow,
    this.padding,
    this.margin,
    this.paddingTop,
    this.paddingRight,
    this.paddingBottom,
    this.paddingLeft,
    this.marginTop,
    this.marginRight,
    this.marginBottom,
    this.marginLeft,
    this.bgColor,
    this.blockType,
    this.linkId,
    this.borderRadius,
    this.childWidth,
    this.blockBgColor,
    //this.sort,
    this.blockBlockType,
    this.filterBy,
    this.paddingBetween,
    //this.blockChildWidth,
    this.childHeight,
    this.elevation,
    this.itemPerRow,
    this.saleEnds,
    this.layoutGridCol,
  });

  factory Block.fromJson(Map<String, dynamic> json) => Block(
    id: json["id"] == null ? null : json["id"],
    children: json["children"] == null || json["children"] == '' ? null : List<Child>.from(json["children"].map((x) => Child.fromJson(x))),
    products: json["products"] == null ? null : List<Product>.from(json["products"].map((x) => Product.fromJson(x))),
    title: json["title"] == null ? null : json["title"],
    headerAlign: json["header_align"] == null ? null : json["header_align"],
    titleColor: json["title_color"] == null ? null : json["title_color"],
    style: json["style"] == null ? null : json["style"],
    bannerShadow: json["banner_shadow"] == null ? null : json["banner_shadow"],
    padding: json["padding"] == null ? null : json["padding"],
    margin: json["margin"] == null ? null : json["margin"],
    paddingTop: json["paddingTop"] == null ? null : json["paddingTop"],
    paddingRight: json["paddingRight"] == null ? null : json["paddingRight"],
    paddingBottom: json["paddingBottom"] == null ? null : json["paddingBottom"],
    paddingLeft: json["paddingLeft"] == null ? null : json["paddingLeft"],
    marginTop: json["marginTop"] == null ? null : json["marginTop"],
    marginRight: json["marginRight"] == null ? null : json["marginRight"],
    marginBottom: json["marginBottom"] == null ? null : json["marginBottom"],
    marginLeft: json["marginLeft"] == null ? null : json["marginLeft"],
    bgColor: json["bgColor"] == null ? null : json["bgColor"],
    blockType: json["blockType"] == null ? null : json["blockType"],
    linkId: json["linkId"] == null ? null : json["linkId"],
    borderRadius: json["borderRadius"] == null ? null : json["borderRadius"].toDouble(),
    childWidth: json["childWidth"] == null ? null : json["childWidth"].toDouble(),
    blockBgColor: json["bg_color"] == null ? null : json["bg_color"],
    //sort: json["sort"] == null ? null : json["sort"],
    blockBlockType: json["block_type"] == null ? null : json["block_type"],
    filterBy: json["filter_by"] == null ? null : json["filter_by"],
    paddingBetween: json["paddingBetween"] == null ? null : json["paddingBetween"].toDouble(),
    //blockChildWidth: json["child_width"] == null ? null : json["child_width"],
    childHeight: json["childHeigth"] == null ? null : json["childHeigth"].toDouble(),
    elevation: json["elevation"] == null ? null : json["elevation"].toDouble(),
    itemPerRow: json["itemPerRow"] == null ? null : json["itemPerRow"],
    saleEnds: json["sale_ends"] == null ? null : json["sale_ends"],
    layoutGridCol: json["itemPerRow"] == null ? null : json["itemPerRow"],
  );

}

class Child {
  String title;
  String description;
  String url;
  String sort;
  String attachmentId;
  String thumb;
  String image;
  String height;
  String width;

  Child({
    this.title,
    this.description,
    this.url,
    this.sort,
    this.attachmentId,
    this.thumb,
    this.image,
    this.height,
    this.width,
  });

  factory Child.fromJson(Map<String, dynamic> json) => Child(
    title: json["title"] == null ? null : json["title"],
    description: json["description"] == null ? null : json["description"],
    url: json["url"] == null ? null : json["url"],
    sort: json["sort"] == null ? null : json["sort"],
    attachmentId: json["attachment_id"] == null ? null : json["attachment_id"],
    thumb: json["thumb"] == null ? null : json["thumb"],
    image: json["image"] == null ? null : json["image"],
    height: json["height"] == null ? null : json["height"],
    width: json["width"] == null ? null : json["width"],
  );
}

class Settings {
  int maxPrice;
  String currency;
  int showFeatured;
  int showOnsale;
  int showLatest;
  int pullToRefresh;
  String onesignalAppId;
  String googleProjectId;
  String googleWebClientId;
  String rateAppIosId;
  String rateAppAndroidId;
  String rateAppWindowsId;
  String shareAppAndroidLink;
  String shareAppIosLink;
  String supportEmail;
  int enableProductChat;
  int enableHomeChat;
  String whatsappNumber;
  String appDir;
  int switchLocations;
  String language;
  String productShadow;
  int enableSoldBy;
  int enableSoldByProduct;
  int enableVendorChat;
  int enableVendorMap;
  int enableWallet;
  int enableRefund;
  int switchWpml;
  int switchAddons;
  int switchRewardPoints;

  Settings({
    this.maxPrice,
    this.currency,
    this.showFeatured,
    this.showOnsale,
    this.showLatest,
    this.pullToRefresh,
    this.onesignalAppId,
    this.googleProjectId,
    this.googleWebClientId,
    this.rateAppIosId,
    this.rateAppAndroidId,
    this.rateAppWindowsId,
    this.shareAppAndroidLink,
    this.shareAppIosLink,
    this.supportEmail,
    this.enableProductChat,
    this.enableHomeChat,
    this.whatsappNumber,
    this.appDir,
    this.switchLocations,
    this.language,
    this.productShadow,
    this.enableSoldBy,
    this.enableSoldByProduct,
    this.enableVendorChat,
    this.enableVendorMap,
    this.enableWallet,
    this.enableRefund,
    this.switchWpml,
    this.switchAddons,
    this.switchRewardPoints,
  });

  factory Settings.fromJson(Map<String, dynamic> json) => Settings(
    maxPrice: json["max_price"] == null ? null : json["max_price"],
    currency: json["currency"] == null ? null : json["currency"],
    showFeatured: json["show_featured"] == null ? null : json["show_featured"],
    showOnsale: json["show_onsale"] == null ? null : json["show_onsale"],
    showLatest: json["show_latest"] == null ? null : json["show_latest"],
    pullToRefresh: json["pull_to_refresh"] == null ? null : json["pull_to_refresh"],
    onesignalAppId: json["onesignal_app_id"] == null ? null : json["onesignal_app_id"],
    googleProjectId: json["google_project_id"] == null ? null : json["google_project_id"],
    googleWebClientId: json["google_web_client_id"] == null ? null : json["google_web_client_id"],
    rateAppIosId: json["rate_app_ios_id"] == null ? null : json["rate_app_ios_id"],
    rateAppAndroidId: json["rate_app_android_id"] == null ? null : json["rate_app_android_id"],
    rateAppWindowsId: json["rate_app_windows_id"] == null ? null : json["rate_app_windows_id"],
    shareAppAndroidLink: json["share_app_android_link"] == null ? null : json["share_app_android_link"],
    shareAppIosLink: json["share_app_ios_link"] == null ? null : json["share_app_ios_link"],
    supportEmail: json["support_email"] == null ? null : json["support_email"],
    enableProductChat: json["enable_product_chat"] == null ? null : json["enable_product_chat"],
    enableHomeChat: json["enable_home_chat"] == null ? null : json["enable_home_chat"],
    whatsappNumber: json["whatsapp_number"] == null ? null : json["whatsapp_number"],
    appDir: json["app_dir"] == null ? null : json["app_dir"],
    switchLocations: json["switchLocations"] == null ? null : json["switchLocations"],
    language: json["language"] == null ? null : json["language"],
    productShadow: json["product_shadow"] == null ? null : json["product_shadow"],
    enableSoldBy: json["enable_sold_by"] == null ? null : json["enable_sold_by"],
    enableSoldByProduct: json["enable_sold_by_product"] == null ? null : json["enable_sold_by_product"],
    enableVendorChat: json["enable_vendor_chat"] == null ? null : json["enable_vendor_chat"],
    enableVendorMap: json["enable_vendor_map"] == null ? null : json["enable_vendor_map"],
    enableWallet: json["enable_wallet"] == null ? null : json["enable_wallet"],
    enableRefund: json["enable_refund"] == null ? null : json["enable_refund"],
    switchWpml: json["switchWpml"] == null ? null : json["switchWpml"],
    switchAddons: json["switchAddons"] == null ? null : json["switchAddons"],
    switchRewardPoints: json["switchRewardPoints"] == null ? null : json["switchRewardPoints"],
  );
}

class Dimensions {
  int imageHeight;
  int productSliderWidth;
  int latestPerRow;
  int productsPerRow;
  int searchPerRow;
  int productBorderRadius;
  int suCatBorderRadius;
  int productPadding;

  Dimensions({
    this.imageHeight,
    this.productSliderWidth,
    this.latestPerRow,
    this.productsPerRow,
    this.searchPerRow,
    this.productBorderRadius,
    this.suCatBorderRadius,
    this.productPadding,
  });

  factory Dimensions.fromJson(Map<String, dynamic> json) => Dimensions(
    imageHeight: json["imageHeight"] == null ? null : json["imageHeight"],
    productSliderWidth: json["productSliderWidth"] == null ? null : json["productSliderWidth"],
    latestPerRow: json["latestPerRow"] == null ? null : json["latestPerRow"],
    productsPerRow: json["productsPerRow"] == null ? null : json["productsPerRow"],
    searchPerRow: json["searchPerRow"] == null ? null : json["searchPerRow"],
    productBorderRadius: json["productBorderRadius"] == null ? null : json["productBorderRadius"],
    suCatBorderRadius: json["suCatBorderRadius"] == null ? null : json["suCatBorderRadius"],
    productPadding: json["productPadding"] == null ? null : json["productPadding"],
  );
}

class User {
  Data data;
  int id;
  Caps caps;
  String capKey;
  List<String> roles;
  dynamic filter;

  User({
    this.data,
    this.id,
    this.caps,
    this.capKey,
    this.roles,
    this.filter,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
    id: json["ID"] == null ? null : json["ID"],
    //caps: json["caps"] == null ? null : Caps.fromJson(json["caps"]),
    //capKey: json["cap_key"] == null ? null : json["cap_key"],
    //roles: json["roles"] == null ? null : List<String>.from(json["roles"].map((x) => x)),
    //filter: json["filter"],
  );
}

class Caps {
  bool administrator;

  Caps({
    this.administrator,
  });

  factory Caps.fromJson(Map<String, dynamic> json) => Caps(
    administrator: json["administrator"] == null ? null : json["administrator"],
  );
}

class Data {
  String id;
  String userLogin;
  String userPass;
  String userNicename;
  String userEmail;
  String userUrl;
  DateTime userRegistered;
  String userActivationKey;
  String userStatus;
  String displayName;
  bool status;
  String url;
  String avatar;
  String avatarUrl;
  int points;
  String pointsVlaue;

  Data({
    this.id,
    this.userLogin,
    this.userPass,
    this.userNicename,
    this.userEmail,
    this.userUrl,
    this.userRegistered,
    this.userActivationKey,
    this.userStatus,
    this.displayName,
    this.status,
    this.url,
    this.avatar,
    this.avatarUrl,
    this.points,
    this.pointsVlaue,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["ID"] == null ? null : json["ID"],
    userLogin: json["user_login"] == null ? null : json["user_login"],
    userPass: json["user_pass"] == null ? null : json["user_pass"],
    userNicename: json["user_nicename"] == null ? null : json["user_nicename"],
    userEmail: json["user_email"] == null ? null : json["user_email"],
    userUrl: json["user_url"] == null ? null : json["user_url"],
    userRegistered: json["user_registered"] == null ? null : DateTime.parse(json["user_registered"]),
    userActivationKey: json["user_activation_key"] == null ? null : json["user_activation_key"],
    userStatus: json["user_status"] == null ? null : json["user_status"],
    displayName: json["display_name"] == null ? null : json["display_name"],
    status: json["status"] == null ? null : json["status"],
    url: json["url"] == null ? null : json["url"],
    avatar: json["avatar"] == null ? null : json["avatar"],
    avatarUrl: json["avatar_url"] == null ? null : json["avatar_url"],
    points: json["points"] == null ? null : json["points"],
    pointsVlaue: json["points_vlaue"] == null ? null : json["points_vlaue"],
  );

  Map<String, dynamic> toJson() => {
    "ID": id == null ? null : id,
    "user_login": userLogin == null ? null : userLogin,
    "user_pass": userPass == null ? null : userPass,
    "user_nicename": userNicename == null ? null : userNicename,
    "user_email": userEmail == null ? null : userEmail,
    "user_url": userUrl == null ? null : userUrl,
    "user_registered": userRegistered == null ? null : userRegistered.toIso8601String(),
    "user_activation_key": userActivationKey == null ? null : userActivationKey,
    "user_status": userStatus == null ? null : userStatus,
    "display_name": displayName == null ? null : displayName,
    "status": status == null ? null : status,
    "url": url == null ? null : url,
    "avatar": avatar == null ? null : avatar,
    "avatar_url": avatarUrl == null ? null : avatarUrl,
    "points": points == null ? null : points,
    "points_vlaue": pointsVlaue == null ? null : pointsVlaue,
  };
}

class Language {
  String code;
  String id;
  String nativeName;
  String major;
  dynamic active;
  String defaultLocale;
  String encodeUrl;
  String tag;
  String translatedName;
  String url;
  String countryFlagUrl;
  String languageCode;

  Language({
    this.code,
    this.id,
    this.nativeName,
    this.major,
    this.active,
    this.defaultLocale,
    this.encodeUrl,
    this.tag,
    this.translatedName,
    this.url,
    this.countryFlagUrl,
    this.languageCode,
  });

  factory Language.fromJson(Map<String, dynamic> json) => Language(
    code: json["code"] == null ? null : json["code"],
    id: json["id"] == null ? null : json["id"],
    nativeName: json["native_name"] == null ? null : json["native_name"],
    major: json["major"] == null ? null : json["major"],
    active: json["active"],
    defaultLocale: json["default_locale"] == null ? null : json["default_locale"],
    encodeUrl: json["encode_url"] == null ? null : json["encode_url"],
    tag: json["tag"] == null ? null : json["tag"],
    translatedName: json["translated_name"] == null ? null : json["translated_name"],
    url: json["url"] == null ? null : json["url"],
    countryFlagUrl: json["country_flag_url"] == null ? null : json["country_flag_url"],
    languageCode: json["language_code"] == null ? null : json["language_code"],
  );
}

class Currency {
  //Languages languages;
  dynamic rate;
  String position;
  String thousandSep;
  String decimalSep;
  String numDecimals;
  String rounding;
  int roundingIncrement;
  int autoSubtract;
  String code;
  DateTime updated;
  int previousRate;

  Currency({
    //this.languages,
    this.rate,
    this.position,
    this.thousandSep,
    this.decimalSep,
    this.numDecimals,
    this.rounding,
    this.roundingIncrement,
    this.autoSubtract,
    this.code,
    this.updated,
    this.previousRate,
  });

  factory Currency.fromJson(Map<String, dynamic> json) => Currency(
    //languages: json["languages"] == null ? null : Languages.fromJson(json["languages"]),
    //rate: json["rate"],
    //position: json["position"] == null ? null : json["position"],
    //thousandSep: json["thousand_sep"] == null ? null : json["thousand_sep"],
    //decimalSep: json["decimal_sep"] == null ? null : json["decimal_sep"],
    //numDecimals: json["num_decimals"] == null ? null : json["num_decimals"],
    //rounding: json["rounding"] == null ? null : json["rounding"],
    //roundingIncrement: json["rounding_increment"] == null ? null : json["rounding_increment"],
    //autoSubtract: json["auto_subtract"] == null ? null : json["auto_subtract"],
    code: json["code"] == null ? null : json["code"],
    //updated: json["updated"] == null ? null : DateTime.parse(json["updated"]),
    //previousRate: json["previous_rate"] == null ? null : json["previous_rate"],
  );
}
