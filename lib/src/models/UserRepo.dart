import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:woocommerce/src/models/customer_model.dart';

class UserRepo{
  Future<Customer> getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var u = await prefs.getString(
      'USER',
    );
    if (u != null)
      return Customer.fromJson(await json.decode(u));
    else
      return null;
  }

  Future<bool> clearPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.clear();
  }

  Future<bool> saveUser(Customer userData) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString('USER', json.encode(userData.toJson()));
  }
}