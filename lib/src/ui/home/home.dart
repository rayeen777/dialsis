import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../ui/accounts/language/app_localizations.dart';
import '../../models/product_model.dart';
import '../../ui/blocks/banner_slider.dart';
import '../../ui/blocks/banner_slider2.dart';
import '../../ui/blocks/banner_slider3.dart';
import '../../ui/blocks/banner_grid_list.dart';
import '../../ui/blocks/banner_scroll_list.dart';
import '../../ui/blocks/banner_slider1.dart';
import '../../ui/blocks/category_grid_list.dart';
import '../../ui/blocks/category_scroll_list.dart';
import '../../ui/blocks/product_grid_list.dart';
import '../../ui/blocks/product_scroll_list.dart';
import '../../blocs/home_bloc.dart';
import '../../models/category_model.dart';
import '../../ui/home/search.dart';
import '../products/product_detail/product_detail.dart';
import '../../ui/products/product_item.dart';
import '../../ui/products/product_list.dart';
import '../../models/blocks_model.dart' hide Image, Key, Theme;
import 'package:flutter/rendering.dart';
import 'package:url_launcher/url_launcher.dart';
import '../options.dart';
import 'drawer/drawer.dart';

class Home extends StatefulWidget {
  final HomeBloc homeBloc;
  final GalleryOptions options;
  final ValueChanged<GalleryOptions> handleOptionsChanged;
  Home({Key key, this.homeBloc, this.options, this.handleOptionsChanged}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        widget.homeBloc.loadMoreRecentProducts();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MyDrawer(homeBloc: widget.homeBloc, options: widget.options, handleOptionsChanged: widget.handleOptionsChanged),
      appBar: AppBar(
          title: Container(
              height: 30,
              width: 30,
              child: Image.asset('lib/assets/images/logo.png', fit: BoxFit.cover)),
          elevation: 1.0,
          actions: [
            StreamBuilder<BlocksModel>(
                stream: widget.homeBloc.allBlocks,
                builder: (context, snapshot) {
                  return snapshot.hasData
                      ? IconButton(
                          icon: Icon(
                            Icons.search,
                            semanticLabel: 'search',
                          ),
                          onPressed: () {
                            showSearch(
                                context: context,
                                delegate: SearchProducts(onProductClick,
                                    snapshot.data.settings.currency));
                          },
                        )
                      : Container();
                }),
          ]),
      body: StreamBuilder(
          stream: widget.homeBloc.allBlocks,
          builder: (context, AsyncSnapshot<BlocksModel> snapshot) {
            return snapshot.hasData
                ? CustomScrollView(
                    controller: _scrollController,
                    slivers: buildLisOfBlocks(snapshot),
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  );
          },
        ),
      floatingActionButton: StreamBuilder<BlocksModel>(
          stream: widget.homeBloc.allBlocks,
          builder: (context, snapshot) {
            if (snapshot.hasData &&
                snapshot.data.settings.enableHomeChat == 1) {
              return FloatingActionButton(
                onPressed: () => _openWhatsApp(
                    snapshot.data.settings.whatsappNumber.toString()),
                tooltip: 'Chat',
                child: Icon(Icons.chat_bubble),
              );
            } else {
              return Container();
            }
          }),
    );
  }

  List<Widget> buildLisOfBlocks(AsyncSnapshot<BlocksModel> snapshot) {
    List<Widget> list = new List<Widget>();

    for (var i = 0; i < snapshot.data.blocks.length; i++) {

      if (snapshot.data.blocks[i].blockType == 'banner_block') {

        if (snapshot.data.blocks[i].style == 'grid') {
          list.add(buildGridHeader(snapshot, i));
          list.add(BannerGridList(
              block: snapshot.data.blocks[i], onBannerClick: onBannerClick));
        }

        if (snapshot.data.blocks[i].style == 'scroll') {
          list.add(BannerScrollList(
              block: snapshot.data.blocks[i], onBannerClick: onBannerClick));
        }

        if(snapshot.data.blocks[i].style == 'slider') {
          list.add(BannerSlider(
              block: snapshot.data.blocks[i], onBannerClick: onBannerClick));
        }

        if(snapshot.data.blocks[i].style == 'slider1') {
          list.add(BannerSlider1(
              block: snapshot.data.blocks[i], onBannerClick: onBannerClick));
        }

        if(snapshot.data.blocks[i].style == 'slider2') {
          list.add(BannerSlider2(
              block: snapshot.data.blocks[i], onBannerClick: onBannerClick));
        }

        if(snapshot.data.blocks[i].style == 'slider3') {
          list.add(BannerSlider3(
              block: snapshot.data.blocks[i], onBannerClick: onBannerClick));
        }

      }

      if (snapshot.data.blocks[i].blockType == 'category_block' &&
          snapshot.data.blocks[i].style == 'scroll') {
        if (snapshot.data.blocks[i].borderRadius == 50) {
          list.add(CategoryScrollStadiumList(
              block: snapshot.data.blocks[i],
              categories: snapshot.data.categories,
              onCategoryClick: onCategoryClick));
        } else {
          list.add(CategoryScrollList(
              block: snapshot.data.blocks[i],
              categories: snapshot.data.categories,
              onCategoryClick: onCategoryClick));
        }
      }

      if (snapshot.data.blocks[i].blockType == 'category_block' &&
          snapshot.data.blocks[i].style == 'grid') {
        list.add(buildGridHeader(snapshot, i));
        if (snapshot.data.blocks[i].borderRadius == 50) {
          list.add(CategoryStadiumGridList(
              block: snapshot.data.blocks[i],
              categories: snapshot.data.categories,
              onCategoryClick: onCategoryClick));
        } else {
          list.add(CategoryGridList(
              block: snapshot.data.blocks[i],
              categories: snapshot.data.categories,
              onCategoryClick: onCategoryClick));
        }
      }

      if (snapshot.data.blocks[i].blockType == 'product_block' &&
          snapshot.data.blocks[i].style == 'scroll') {
        list.add(ProductScrollList(
            block: snapshot.data.blocks[i], onProductClick: onProductClick));
      }

      if (snapshot.data.blocks[i].blockType == 'product_block' &&
          snapshot.data.blocks[i].style == 'grid') {
        list.add(buildGridHeader(snapshot, i));
        list.add(ProductGridList(
            block: snapshot.data.blocks[i], onProductClick: onProductClick));
      }
    }

    if (snapshot.data.recentProducts != null) {
      list.add(buildRecentProductGridList(snapshot));

      list.add(SliverPadding(
          padding: EdgeInsets.all(0.0),
          sliver: SliverList(
              delegate: SliverChildListDelegate([
            Container(
                height: 60,
                child: StreamBuilder(
                    stream: widget.homeBloc.hasMoreItems,
                    builder: (context, AsyncSnapshot<bool> snapshot) {
                      return snapshot.hasData && snapshot.data == false
                          ? Center(child: Text(AppLocalizations.of(context).translate("no more products"),),)
                          : Center(child: CircularProgressIndicator());
                    }
                    ))
          ]))));
    }

    return list;
  }

  double _headerAlign(String align) {
    switch (align) {
      case 'top_left':
        return -1;
      case 'top_right':
        return 1;
      case 'top_center':
        return 0;
      case 'floating':
        return 2;
      case 'none':
        return null;
      default:
        return -1;
    }
  }

  Widget buildRecentProductGridList(AsyncSnapshot<BlocksModel> snapshot) {
    return SliverPadding(
      padding: const EdgeInsets.all(8.0),
      sliver: SliverGrid(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          mainAxisSpacing: 2.0,
          crossAxisSpacing: 5.0,
          childAspectRatio: 0.68,
          crossAxisCount: 2,
        ),
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return ProductItem(
                product: snapshot.data.recentProducts[index],
                onProductClick: onProductClick);
          },
          childCount: snapshot.data.recentProducts.length,
        ),
      ),
    );
  }

  Widget buildListHeader(AsyncSnapshot<BlocksModel> snapshot, int childIndex) {
    Color bgColor = HexColor(snapshot.data.blocks[childIndex].bgColor);
    double textAlign =
        _headerAlign(snapshot.data.blocks[childIndex].headerAlign);
    return textAlign != null
        ? SliverToBoxAdapter(
            child: Container(
                padding: EdgeInsets.fromLTRB(
                    snapshot.data.blocks[childIndex].paddingBetween,
                    double.parse(
                        snapshot.data.blocks[childIndex].paddingTop.toString()),
                    snapshot.data.blocks[childIndex].paddingBetween,
                    16.0),
                color: widget.options.theme.name != 'Dark'
                    ? bgColor
                    : Theme.of(context).canvasColor,
                alignment: Alignment(textAlign, 0),
                child: Text(
                  snapshot.data.blocks[childIndex].title,
                  textAlign: TextAlign.start,
                  style: widget.options.theme.name != 'Dark'
                      ? Theme.of(context).textTheme.title.copyWith(
                            fontSize: 17,
                            fontWeight: FontWeight.w800,
                            letterSpacing: 0.5,
                            color: HexColor(
                                snapshot.data.blocks[childIndex].titleColor),
                          )
                      : Theme.of(context).textTheme.title,
                )))
        : SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.all(0),
              padding: EdgeInsets.fromLTRB(
                  snapshot.data.blocks[childIndex].paddingBetween,
                  double.parse(
                      snapshot.data.blocks[childIndex].paddingTop.toString()),
                  snapshot.data.blocks[childIndex].paddingBetween,
                  0.0),
              color: widget.options.theme.name != 'Dark'
                  ? bgColor
                  : Theme.of(context).canvasColor,
            ),
          );
  }

  Widget buildGridHeader(AsyncSnapshot<BlocksModel> snapshot, int childIndex) {
    double textAlign =
    _headerAlign(snapshot.data.blocks[childIndex].headerAlign);
    TextStyle subhead = Theme.of(context).brightness != Brightness.dark
        ? Theme.of(context).textTheme.subhead
        .copyWith(fontWeight: FontWeight.w600, color: HexColor(
        snapshot.data.blocks[childIndex].titleColor)) : Theme.of(context).textTheme.subhead
        .copyWith(fontWeight: FontWeight.w600);
    return textAlign != null
        ? SliverToBoxAdapter(
        child: Container(
            padding: EdgeInsets.fromLTRB(
                double.parse(snapshot.data.blocks[childIndex].paddingLeft.toString()) + 4,
                double.parse(
                    snapshot.data.blocks[childIndex].paddingTop.toString()),
                double.parse(snapshot.data.blocks[childIndex].paddingRight.toString()) + 4,
                16.0),
            color: Theme.of(context).scaffoldBackgroundColor,
            alignment: Alignment(textAlign, 0),
            child: Text(
              snapshot.data.blocks[childIndex].title,
              textAlign: TextAlign.start,
              style: subhead,
            )))
        : SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.fromLTRB(
            snapshot.data.blocks[childIndex].paddingBetween,
            double.parse(
                snapshot.data.blocks[childIndex].paddingTop.toString()),
            snapshot.data.blocks[childIndex].paddingBetween,
            0.0),
        color: Theme.of(context).scaffoldBackgroundColor,
      ),
    );
  }

  onProductClick(product) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ProductDetail(
        product: product,
        homeBloc: widget.homeBloc,
      );
    }));
  }

  onBannerClick(Child data) {
    //Naviaget yo product or product list depend on type
    if(data.url.isNotEmpty) {
      if(data.description == 'category') {
        var filter = new Map<String, dynamic>();
        filter['id'] = data.url;
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductList(
                    filter: filter,
                    name: data.title,
                    homeBloc: widget.homeBloc)));
      };
      if(data.description == 'product') {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductDetail(
                    product:  Product(id: int.parse(data.url), name: data.title,),
                    homeBloc: widget.homeBloc)));
      };
    }
  }

  Future _openWhatsApp(String number) async {
    final url = 'https://wa.me/' + number;
    print(url);
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  onCategoryClick(Category category, List<Category> categories) {
    var filter = new Map<String, dynamic>();
    filter['id'] = category.id.toString();
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductList(
                filter: filter,
                name: category.name,
                homeBloc: widget.homeBloc)));
  }
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
