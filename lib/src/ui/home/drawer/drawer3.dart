import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../../blocs/home_bloc.dart';
import '../../../models/blocks_model.dart';
import '../../../models/category_model.dart';
import '../../../ui/products/product_list.dart';
import 'package:html/parser.dart';
import '../../options.dart';
import '../category_list.dart';

class MyDrawer extends StatefulWidget {
  final HomeBloc homeBloc;
  final GalleryOptions options;
  final ValueChanged<GalleryOptions> handleOptionsChanged;
  MyDrawer({Key key, this.homeBloc, this.options, this.handleOptionsChanged})
      : super(key: key);
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 10,
      child: Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: Padding(
          padding: const EdgeInsets.only(top: 16.0),
          child: StreamBuilder<BlocksModel>(
              stream: widget.homeBloc.allBlocks,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<Category> mainCategories = snapshot.data.categories.where((cat) => cat.parent == 0).toList();
                  return CustomScrollView(
                  slivers: <Widget>[
                    SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          ListTile(
                            title: Container(
                                padding:EdgeInsets.only(top: 10),
                                height: 60,
                                //color: Colors.blueAccent,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      'MY',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        fontFamily: 'Lexend_Deca',
                                        letterSpacing: 0.5,
                                        color: Colors.amber,
                                        fontSize: 20,
                                      ),),
                                    Text(
                                      ' STORE',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w800,
                                          fontFamily: 'Lexend_Deca',
                                          letterSpacing: 0.5,
                                          color: Colors.redAccent,
                                          fontSize: 20
                                      ),),
                                  ],
                                )
                            ),
                            trailing: IconButton(
                                icon:Icon(Icons.close) ,
                                onPressed: null),
                            subtitle: Padding(
                              padding: const EdgeInsets.only(bottom: 8.0),
                              child: Text(
                                  '5% Discount on all orders',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      fontFamily: 'Lexend_Deca',
                                      letterSpacing: 0.5,
                                      color: Colors.grey,
                                      fontSize: 12
                                  )
                              ),
                            ),
                            onTap: () {
                              // Update the state of the app
                              // ...
                              // Then close the drawer
                              Navigator.pop(context);
                            },
                          ),
                          /*ListTile(
                            title: Container(
                                padding:EdgeInsets.only(top: 10),
                                height: 60,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Image.asset('assets/AjioLogo.png',width: 35, height: 35,),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Image.asset('assets/Ajio.png',width: 80, height: 45,)
                                  ],
                                )
                            ),
                            leading: IconButton(
                                icon:Icon(Icons.clear,size: 28,) ,
                                onPressed: null),
                            onTap: () {
                              Navigator.pop(context);
                            },
                          ),*/
                          Divider(
                            color: Theme.of(context).brightness == Brightness.light ? Colors.black45 : Colors.white,
                            thickness: 0.2,
                            height: 2,
                          ),
                        ]
                      )
                    ),
                    SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                          return _buildTile(mainCategories[index]);
                        },
                        childCount: mainCategories.length,
                      ),
                    ),
                    SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          ListTile(
                            leading: Icon(Icons.ac_unit, size: 25, color: leadingIconColor(),),
                            title: Text(
                              'FEATURED',
                              style: menuItemStyle()
                            ),
                            trailing: Icon(Icons.arrow_forward_ios, size: 16),
                              onTap: () {
                                var filter = new Map<String, dynamic>();
                                filter['featured'] = '1';
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProductList(
                                            filter: filter,
                                            name: 'FEATURED',
                                            homeBloc: widget.homeBloc)));
                              },
                          ),
                          _divider(context),
                          ListTile(
                            leading: Icon(FontAwesomeIcons.windowRestore, size: 20, color: leadingIconColor(),),
                            title: Text(
                              'COLLECTIONS',
                              style: menuItemStyle()
                            ),
                            trailing: Icon(Icons.arrow_forward_ios, size: 16),
                            onTap: () {
                              _onTapTag(tag: 'collections', name: 'COLLECTIONS');
                            },
                          ),
                          _divider(context),
                          ListTile(
                            leading: Icon(
                              Icons.fiber_new,
                              size: 20,
                              color: leadingIconColor(),
                            ),
                            title: Text(
                              'NEW ARRIVALS',
                              style: menuItemStyle()
                            ),
                            trailing: Icon(Icons.arrow_forward_ios, size: 16),
                            onTap: () {
                              var filter = new Map<String, dynamic>();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ProductList(
                                          filter: filter,
                                          name: 'NEW ARRIVALS',
                                          homeBloc: widget.homeBloc)));
                            },
                          ),
                          _divider(context),
                          ListTile(
                            leading: Icon(
                              FontAwesomeIcons.tags,
                              size: 20,
                              color: leadingIconColor(),
                            ),
                            title: Text(
                              'SALE',
                              style: menuItemStyle()
                            ),
                            trailing: Icon(Icons.arrow_forward_ios, size: 16),
                            onTap: () {
                              var filter = new Map<String, dynamic>();
                              filter['on_sale'] = '1';
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ProductList(
                                          filter: filter,
                                          name: 'SALE',
                                          homeBloc: widget.homeBloc)));
                            },
                          ),
                          _divider(context),
                          ListTile(
                            leading: Icon(
                              FontAwesomeIcons.store,
                              size: 20,
                              color: leadingIconColor(),
                            ),
                            title: Text(
                              'FAVOURITE STORE',
                              style: menuItemStyle()
                            ),
                            trailing: Icon(Icons.arrow_forward_ios, size: 16),
                            onTap: () {
                              _onTapTag(tag: 'favourite', name: 'FAVOURITE');
                            },
                          ),
                          Divider(
                            color: Theme.of(context).brightness == Brightness.light ? Colors.black45 : Colors.white,
                            thickness: 0.2,
                            height: 2,
                          ),
                        ],
                      ),
                    ),
                  ],
                );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              }),
        ),
      ),
    );
  }

  Color leadingIconColor() => Theme.of(context).brightness == Brightness.light ? Colors.blueGrey : Colors.grey;
  Color trailingIconColor() => Theme.of(context).brightness == Brightness.light ? Colors.grey : Colors.grey;

  Widget _buildTile(Category category) {
    return Padding(
      padding: EdgeInsets.all(0.0),
      child: Column(
        children: <Widget>[
          ListTile(
            onTap: () {
              _onTap(category);
            },
            leading: leadingIcon(category),
            trailing: Icon(Icons.arrow_forward_ios, size: 16),
            title: Text(
              _parseHtmlString(category.name).toUpperCase(),
              style: menuItemStyle()
            ),
          ),
          _divider(context),
        ],
      ),
    );
  }

  TextStyle menuItemStyle() {
    return TextStyle(
                fontWeight: FontWeight.w800,
                fontFamily: 'Lexend_Deca',
                letterSpacing: 0.5,
                color: Theme.of(context).brightness == Brightness.light ? Colors.black.withOpacity(0.8) : Colors.grey,
            );
  }

  Container leadingIcon(Category category) {
    return Container(
      width: 24,
      height: 24,
      child: CachedNetworkImage(
        imageUrl: category.image != null ? category.image : '',
        imageBuilder: (context, imageProvider) => Card(
          clipBehavior: Clip.antiAlias,
          elevation: 0.0,
          margin: EdgeInsets.all(0.0),
          //shape: StadiumBorder(),
          child: Ink.image(
            child: InkWell(
              onTap: () {
                //onCategoryClick(category);
              },
            ),
            image: imageProvider,
            fit: BoxFit.cover,
          ),
        ),
        placeholder: (context, url) => Card(
          clipBehavior: Clip.antiAlias,
          elevation: 0.0,
          shape: StadiumBorder(),
        ),
        errorWidget: (context, url, error) => Card(
          elevation: 0.0,
          color: Colors.white,
          shape: StadiumBorder(),
        ),
      ),
    );
  }

  _onTap(Category category) {
    var filter = new Map<String, dynamic>();
    filter['id'] = category.id.toString();
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductList(
                filter: filter,
                name: category.name,
                homeBloc: widget.homeBloc)));
  }

  _divider(BuildContext context) {
    return Divider(
      color: Theme.of(context).brightness == Brightness.light ? Colors.black45 : Colors.white,
      thickness: 0.2,
      height: 2,
      indent: 60,
    );
  }

  void _onTapTag({String tag, String name}) {
    var filter = new Map<String, dynamic>();
    filter['tag'] = tag;
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductList(
                filter: filter,
                name: name,
                homeBloc: widget.homeBloc)));
  }
}

String _parseHtmlString(String htmlString) {
  var document = parse(htmlString);

  String parsedString = parse(document.body.text).documentElement.text;

  return parsedString;
}
