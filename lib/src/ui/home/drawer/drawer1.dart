import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../ui/accounts/language/app_localizations.dart';
import '../../../blocs/home_bloc.dart';
import '../../../models/blocks_model.dart';
import '../../../models/category_model.dart';
import '../../accounts/account/account.dart';
import '../../products/product_list.dart';

import '../../options.dart';
import '../category_list.dart';

class MyDrawer extends StatefulWidget {
  final HomeBloc homeBloc;
  final GalleryOptions options;
  final ValueChanged<GalleryOptions> handleOptionsChanged;
  MyDrawer({Key key, this.homeBloc, this.options, this.handleOptionsChanged})
      : super(key: key);
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> with TickerProviderStateMixin {
  final TextStyle _biggerFont =
  const TextStyle(fontSize: 24);

  static final Animatable<Offset> _drawerDetailsTween = Tween<Offset>(
    begin: const Offset(0.0, -1.0),
    end: Offset.zero,
  ).chain(CurveTween(
    curve: Curves.fastOutSlowIn,
  ));

  AnimationController _controller;
  Animation<double> _drawerContentsOpacity;
  Animation<Offset> _drawerDetailsPosition;
  bool _showDrawerContents = true;

  @override
  Future initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 200),
    );
    _drawerContentsOpacity = CurvedAnimation(
      parent: ReverseAnimation(_controller),
      curve: Curves.fastOutSlowIn,
    );
    _drawerDetailsPosition = _controller.drive(_drawerDetailsTween);
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(
              'FLUTTER',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
            ),
            currentAccountPicture: const CircleAvatar(
              backgroundImage: AssetImage(
                'lib/images/menu_header.jpg',
              ),
            ),
            margin: EdgeInsets.zero,
          ),
          MediaQuery.removePadding(
            context: context,
            // DrawerHeader consumes top MediaQuery padding.
            removeTop: true,
            child: Container(
              child: Expanded(
                child: Container(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: Column(
                    children: <Widget>[
                      ListTile(
                        trailing: Icon(Icons.keyboard_arrow_right),
                        onTap: () {
                          Navigator.pop(context);
                        },
                        leading: Icon(CupertinoIcons.home),
                        title: Text(AppLocalizations.of(context).translate("home")),
                      ),
                      ListTile(
                        trailing: Icon(Icons.keyboard_arrow_right),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    UserAccount(options: widget.options,
                                        handleOptionsChanged: widget.handleOptionsChanged,
                                        homeBloc: widget.homeBloc),
                              ));
                        },
                        leading: Icon(CupertinoIcons.person),
                        title: Text(AppLocalizations.of(context).translate("account")),
                      ),
                      Expanded(child: buildList()),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildList() {
    return StreamBuilder<BlocksModel>(
        stream: widget.homeBloc.allBlocks,
        builder: (context, snapshot) {
          return Center(
            child: snapshot.hasData ? CategoryList(categories: snapshot.data.categories, onTapCategory: _onTap) :
            CircularProgressIndicator(),
          );
        }
    );
  }

  _onTap(Category category) {
    var filter = new Map<String, dynamic>();
    filter['id'] = category.id.toString();
      Navigator.push(
        context,
          MaterialPageRoute(
              builder: (context) => ProductList(
                  filter: filter,
                  name: category.name,
                  homeBloc: widget.homeBloc)));
  }
}
