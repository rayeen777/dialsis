import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../ui/accounts/language/app_localizations.dart';
import '../../../blocs/home_bloc.dart';
import '../../../models/blocks_model.dart';
import '../../../models/category_model.dart';
import '../../accounts/account/account.dart';
import '../../products/product_list.dart';
import '../../options.dart';
import '../category_list.dart';

class MyDrawer extends StatefulWidget {
  final HomeBloc homeBloc;
  final GalleryOptions options;
  final ValueChanged<GalleryOptions> handleOptionsChanged;
  MyDrawer({Key key, this.homeBloc, this.options, this.handleOptionsChanged})
      : super(key: key);
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> with TickerProviderStateMixin {
  final TextStyle _biggerFont =
  const TextStyle(fontSize: 24);

  static final Animatable<Offset> _drawerDetailsTween = Tween<Offset>(
    begin: const Offset(0.0, -1.0),
    end: Offset.zero,
  ).chain(CurveTween(
    curve: Curves.fastOutSlowIn,
  ));

  AnimationController _controller;
  Animation<double> _drawerContentsOpacity;
  Animation<Offset> _drawerDetailsPosition;
  bool _showDrawerContents = true;

  @override
  Future initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 200),
    );
    _drawerContentsOpacity = CurvedAnimation(
      parent: ReverseAnimation(_controller),
      curve: Curves.fastOutSlowIn,
    );
    _drawerDetailsPosition = _controller.drive(_drawerDetailsTween);
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          MediaQuery.removePadding(
            context: context,
            // DrawerHeader consumes top MediaQuery padding.
            removeTop: true,
            child: Container(
              child: Expanded(
                child: Container(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 50,
                      ),
                      ListTile(
                        trailing: Icon(Icons.keyboard_arrow_right),
                        onTap: () {
                          Navigator.pop(context);
                        },
                        leading: Container(
                            width: 32,
                            height: 32,
                            child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2.0),
                                ),
                                elevation: 0.0,
                                color: Colors.red,
                                child: Icon(Icons.home, color: Colors.white, size: 18,)
                            )
                        ),
                        title: Text(AppLocalizations.of(context).translate("home")),
                        dense: true,
                      ),
                      ListTile(
                        trailing: Icon(Icons.keyboard_arrow_right),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    UserAccount(options: widget.options,
                                        handleOptionsChanged: widget.handleOptionsChanged,
                                        homeBloc: widget.homeBloc),
                              ));
                        },
                        leading: Container(
                            width: 32,
                            height: 32,
                            child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2.0),
                                ),
                                elevation: 0.0,
                                color: Colors.blue,
                                child: Icon(Icons.person, color: Colors.white, size: 18,)
                            )
                        ),
                        title: Text(AppLocalizations.of(context).translate("account")),
                        dense: true,
                      ),
                      ListTile(
                        title: Text(AppLocalizations.of(context).translate("tab_categories"), style: Theme.of(context).textTheme.subhead,),
                      ),
                      Expanded(child: buildList()),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildList() {
    return StreamBuilder<BlocksModel>(
        stream: widget.homeBloc.allBlocks,
        builder: (context, snapshot) {
          return Center(
            child: snapshot.hasData ? CategoryList(categories: snapshot.data.categories, onTapCategory: _onTap) :
            CircularProgressIndicator(),
          );
        }
    );
  }

  _onTap(Category category) {
    var filter = new Map<String, dynamic>();
    filter['id'] = category.id.toString();
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductList(
                filter: filter,
                name: category.name,
                homeBloc: widget.homeBloc)));
  }
}
