import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:woocommerce/src/models/UserRepo.dart';
import 'package:woocommerce/src/models/customer_model.dart';
import '../../../ui/accounts/language/app_localizations.dart';
import '../../../blocs/home_bloc.dart';
import '../../../models/blocks_model.dart';
import '../../../models/category_model.dart';
import '../../accounts/account/account.dart';
import '../../../ui/products/product_list.dart';
import 'package:html/parser.dart';
import '../../options.dart';
import 'package:url_launcher/url_launcher.dart';

class MyDrawer extends StatefulWidget {
  final HomeBloc homeBloc;
  final GalleryOptions options;
  final ValueChanged<GalleryOptions> handleOptionsChanged;
  final Customer user;
  MyDrawer({Key key, this.homeBloc, this.options, this.handleOptionsChanged,this.user})
      : super(key: key);
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 0,
      child: Container(
        padding: EdgeInsets.only(top: 16.0),
        color: Colors.black87,
        child: StreamBuilder<BlocksModel>(
          stream: widget.homeBloc.allBlocks,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<Category> mainCategories = snapshot.data.categories.where((cat) => cat.parent == 0).toList();
              return CustomScrollView(
              slivers: <Widget>[
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      Container(
                        padding:EdgeInsets.only(top: 12),
                        child: ListTile(
                          title: Container(
                              height: 40,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                 widget.user!=null ? widget.user.firstName : ' Guest',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      fontFamily: 'Lexend_Deca',
                                      letterSpacing: 0.5,
                                      color: Colors.cyan,
                                      fontSize: 20,
                                    ),),
                                  Text(
                                  widget.user!=null ? ' '+widget.user.lastName : ' User',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        fontFamily: 'Lexend_Deca',
                                        letterSpacing: 0.5,
                                        color: Colors.yellow,
                                        fontSize: 20
                                    ),),
                                ],
                              )
                          ),
                          leading: Padding(
                            padding: const EdgeInsets.only(top: 2.0),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.cyan,
                                  borderRadius: BorderRadius.all(Radius.circular(2.0))
                              ),
                              width: 30,
                              height: 30,
                              child: Icon(FontAwesomeIcons.shoppingBasket, size: 20, color: Colors.orangeAccent[100]),
                            ),
                          ),
//                          subtitle: Padding(
//                            padding: const EdgeInsets.only(bottom: 8.0),
//                            child: Text(
//                                '5% Discount on all orders',
//                                style: TextStyle(
//                                    fontWeight: FontWeight.w800,
//                                    fontFamily: 'Lexend_Deca',
//                                    letterSpacing: 0.5,
//                                    color: Colors.grey,
//                                    fontSize: 12
//                                )
//                            ),
//                          ),
                          onTap: () {
                            // Update the state of the app
                            // ...
                            // Then close the drawer
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      Divider(
                        color: Colors.grey,
                        thickness: 0.4,
                        height: 3,
                      ),
                    ]
                  )
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      ListTile(
                        leading: _buildLeadingIcon(icon: FontAwesomeIcons.comment, bgColor: Colors.white, iconColor: Colors.red),
                        title: Text(AppLocalizations.of(context).translate("chat"),
                            style: menuItemStyle()),
                        onTap: () => _openWhatsApp(
                            '+917007229963'),
                      ),
                      ListTile(
                        leading: _buildLeadingIcon(icon: Icons.person, bgColor: Colors.white, iconColor: Colors.red),
                        title: Text(AppLocalizations.of(context).translate("my_account"),
                            style: menuItemStyle()),
                        onTap: () async {
                          var user = await UserRepo().getUser();
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    UserAccount(options: widget.options,
                                        handleOptionsChanged: widget.handleOptionsChanged,
                                        homeBloc: widget.homeBloc,user: user,),
                              ));
                        },
                      ),
                      ListTile(
                        leading: _buildLeadingIcon(icon: Icons.perm_contact_calendar, bgColor: Colors.white, iconColor: Colors.red),
                        title: Text("Prime Member",
                            style: menuItemStyle()),
                        onTap: ()  {

                        },
                      ),
                      ListTile(
                        leading: _buildLeadingIcon(icon: Icons.call, bgColor: Colors.red, iconColor: Colors.white),
                        title: Text(AppLocalizations.of(context).translate("customer_support"),
                            style: menuItemStyle()),
                        onTap: () => _callNumber(
                            '+917007229963'),
                      ),
                      Divider(
                        color: Colors.grey,
                        thickness: 0.4,
                        height: 3,
                      ),
                      ListTile(
                        leading: _buildLeadingIcon(icon: Icons.home, bgColor: Colors.red, iconColor: Colors.white),
                        title: Text(AppLocalizations.of(context).translate("home"),
                            style: menuItemStyle()),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                      ListTile(
                        leading: _buildLeadingIcon(icon: FontAwesomeIcons.tag, bgColor: Colors.orangeAccent, iconColor: Colors.white),
                        title: Text(AppLocalizations.of(context).translate("on_sale"),
                            style: menuItemStyle()),
                        onTap: () {
                          var filter = new Map<String, dynamic>();
                          filter['on_sale'] = '1';
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ProductList(
                                      filter: filter,
                                      name: AppLocalizations.of(context).translate("on_sale"),
                                      homeBloc: widget.homeBloc)));
                        },
                      ),
                      ListTile(
                        leading: _buildLeadingIcon(icon: Icons.lens, bgColor: Colors.lightGreen, iconColor: Colors.white),
                        title: Text(AppLocalizations.of(context).translate("new"),
                            style: menuItemStyle()),
                        onTap: () {
                          var filter = new Map<String, dynamic>();
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ProductList(
                                      filter: filter,
                                      name: AppLocalizations.of(context).translate("new"),
                                      homeBloc: widget.homeBloc)));
                        },
                      ),
                      ListTile(
                        leading: _buildLeadingIcon(icon: FontAwesomeIcons.shoppingBasket, bgColor: Colors.red, iconColor: Colors.white),
                        title: Text(AppLocalizations.of(context).translate("favourite_store"),
                            style: menuItemStyle()),
                        onTap: () {
                          _onTapTag(tag: 'favourite', name: AppLocalizations.of(context).translate("favourite_store"));
                        },
                      ),
                      Divider(
                        color: Colors.grey,
                        thickness: 0.4,
                        height: 3,
                      ),
                    ]
                  ),
                ),
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                      return _buildTile(mainCategories[index]);
                    },
                    childCount: mainCategories.length,
                  ),
                ),
                SliverToBoxAdapter(
                  child: Divider(
                    color: Colors.grey,
                    thickness: 0.4,
                    height: 3,
                  ),
                )
              ],
            );
            } else {
              return Center(child: CircularProgressIndicator(),);
            }
          }
        ),
      ),
    );
  }
  Color leadingIconColor() => Theme.of(context).brightness == Brightness.light ? Colors.blueGrey : Colors.grey;
  Color trailingIconColor() => Theme.of(context).brightness == Brightness.light ? Colors.grey : Colors.grey;

  Widget _buildTile(Category category) {
    return Padding(
      padding: EdgeInsets.all(0.0),
      child: Column(
        children: <Widget>[
          ListTile(
            onTap: () {
              _onTap(category);
            },
            leading: leadingIcon(category),
            trailing: Icon(Icons.arrow_forward_ios, size: 14, color: Colors.white,),
            title: Text(
                _parseHtmlString(category.name),
                style: menuItemStyle()
            ),
          ),
          _divider(context),
        ],
      ),
    );
  }

  TextStyle menuItemStyle() {
    return TextStyle(
        fontWeight: FontWeight.w300,
        letterSpacing: 0.5,
        color: Colors.white,
        fontSize: 18
    );
  }

  Container leadingIcon(Category category) {
    return Container(
      width: 30,
      height: 30,
      child: CachedNetworkImage(
        imageUrl: category.image != null ? category.image : '',
        imageBuilder: (context, imageProvider) => Card(
          clipBehavior: Clip.antiAlias,
          elevation: 0.0,
          margin: EdgeInsets.all(0.0),
          //shape: StadiumBorder(),
          child: Ink.image(
            child: InkWell(
              onTap: () {
                //onCategoryClick(category);
              },
            ),
            image: imageProvider,
            fit: BoxFit.cover,
          ),
        ),
        placeholder: (context, url) => Card(
          clipBehavior: Clip.antiAlias,
          elevation: 0.0,
          //shape: StadiumBorder(),
        ),
        errorWidget: (context, url, error) => Card(
          elevation: 0.0,
          color: Colors.white,
          //shape: StadiumBorder(),
        ),
      ),
    );
  }

  _onTap(Category category) {
    var filter = new Map<String, dynamic>();
    filter['id'] = category.id.toString();
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductList(
                filter: filter,
                name: category.name,
                homeBloc: widget.homeBloc)));
  }

  void _onTapTag({String tag, String name}) {
    var filter = new Map<String, dynamic>();
    filter['tag'] = tag;
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductList(
                filter: filter,
                name: name,
                homeBloc: widget.homeBloc)));
  }

  _divider(BuildContext context) {
    return Divider(
      color: Theme.of(context).brightness == Brightness.light ? Colors.black45 : Colors.white,
      thickness: 0.2,
      height: 2,
      indent: 60,
    );
  }

  _buildLeadingIcon({IconData icon, Color bgColor, Color iconColor}) {
    return Container(
        width: 30,
        height: 30,
        padding: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
            color: bgColor,
            borderRadius: BorderRadius.all(Radius.circular(2.0))
        ),
        child: Icon(icon, color: iconColor, size: 20, )
    );
  }

  Future _openWhatsApp(String number) async {
    final url = 'https://wa.me/' + number;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future _callNumber(String number) async {
    final url = 'tel:' + number;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

String _parseHtmlString(String htmlString) {
  var document = parse(htmlString);

  String parsedString = parse(document.body.text).documentElement.text;

  return parsedString;
}