import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../../blocs/search_bloc.dart';
import '../../models/product_model.dart';
import '../../ui/products/product_item.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;


class SearchProducts extends SearchDelegate<List<Product>> {
  final Function onProductClick;
  final String currency;
  final SearchBloc searchBloc = SearchBloc();

  @override
  void initState() {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        searchBloc.loadMoreSearchResults(query);

      }
    });

  }

  ScrollController _scrollController = new ScrollController();

  SearchProducts(this.onProductClick, this.currency);




  @override
  List<Widget> buildActions(BuildContext context) {
    return [

      IconButton(
          icon: Icon(Icons.mic),
          onPressed: () {

            Fluttertoast.showToast(
                msg: "Listing.....",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 16.0
            );
customDialog(context);
          }),
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = '';
          }),
    ];
  }

  customDialog(BuildContext context,
      ) async {

    stt.SpeechToText speech = stt.SpeechToText();
    bool available = await speech.initialize( onStatus: (r){
      print(r);
    }, onError: (e){
      print(e.toString());
    } );
    if ( available ) {
      speech.listen( onResult: (result){
        if(result.recognizedWords.isNotEmpty && result.finalResult)
        {

          query = result.recognizedWords;
        }


      } );
    }
    else {
      print("The user has denied the use of speech recognition.");
    }

    Future.delayed(Duration(seconds: 4), () {
      Navigator.of(context).pop(true);
    });

    return showDialog(
          context: context,
          barrierDismissible: true,
          builder: (_) => new AlertDialog(


              content: Container(height: 200,width: 200,
              child: Center(child: Icon(Icons.mic,size: 40,color: Colors.red,)))));
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    if (query.isNotEmpty) {
      searchBloc.fetchSearchResults(query);
    }
    return StreamBuilder<bool>(
      stream: searchBloc.searchLoading,
      builder: (context, snapshotLoading) {
        return StreamBuilder<List<Product>>(
          stream: searchBloc.searchResults,
          builder: (context, AsyncSnapshot<List<Product>> snapshot) {
            if(snapshotLoading.hasData && snapshotLoading.data) {
              return Center(child: CircularProgressIndicator());
            }
            else if(snapshot.hasData && query.isNotEmpty) {
              if(snapshot.data.length == 0) {
                return Center(child: Text('NO RESULTS!'));
              }
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: buildProductList(snapshot, context),
              );
            } else if(snapshot.hasData && snapshot.data.length == 0) {
              return Center(
                  child: Text('Please type something to search')
              );
            } else return Center(child: Container());
          },
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Container();
    /*if (query.isNotEmpty) {
      searchBloc.fetchSearchResults(query);
    }
    return StreamBuilder<bool>(
      stream: searchBloc.searchLoading,
      builder: (context, snapshotLoading) {
        return StreamBuilder<List<ProductModel>>(
          stream: searchBloc.searchResults,
          builder: (context, AsyncSnapshot<List<ProductModel>> snapshot) {
            if(snapshotLoading.hasData && snapshotLoading.data) {
              return Center(child: CircularProgressIndicator());
            }
            else if(snapshot.hasData && query.isNotEmpty) {
              if(snapshot.data.length == 0) {
                return Center(child: Text('NO RESULTS!'));
              }
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: buildProductList(snapshot, context),
              );
            } else if(snapshot.hasData && snapshot.data.length == 0) {
              return Center(
                  child: Text('Please type something to search')
              );
            } else return Center(child: Container());
          },
        );
      },
    );*/
  }

  @override
  ThemeData appBarTheme(BuildContext context) {
    assert(context != null);
    final ThemeData theme = Theme.of(context);
    assert(theme != null);
    return theme.copyWith(
      primaryColor: Theme.of(context).primaryColor,
      primaryColorBrightness: Theme.of(context).primaryColorBrightness,
      primaryTextTheme: Theme.of(context).primaryTextTheme,
    );
  }

  Widget buildProductList(snapshot, context) {
    return GridView.builder(
        itemCount: snapshot.data.length,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 18 / 25,
            mainAxisSpacing: 4.0,
            crossAxisSpacing: 4.0),
        itemBuilder: (BuildContext context, int index) {
          return ProductItem(
              product: snapshot.data[index], onProductClick: onProductClick);
          ;
        });
  }
}



