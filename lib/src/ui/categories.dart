import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import '../../src/ui/accounts/language/app_localizations.dart';
import 'products/product_list.dart';
import './../blocs/home_bloc.dart';
import '../models/category_model.dart';
import './../models/blocks_model.dart' hide Image, Key;
import './../ui/products/product_list.dart';


class Categories extends StatefulWidget {
  final HomeBloc homeBloc;

  Categories({Key key, this.homeBloc}) : super(key: key);

  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {

  List<Category> mainCategories;
  List<Category> subCategories;
  Category selectedCategory;
  int mainCategoryId = 0;
  int selectedCategoryIndex = 0;

  @override
  Widget build(BuildContext context) {
    //final ThemeData localTheme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        title: Text(AppLocalizations.of(context).translate("tab_categories"),),
      ),
      body: Container(
          child: Column(
          children: <Widget>[
          Flexible(
              child: StreamBuilder(
            stream: widget.homeBloc.allBlocks,
              builder: (context, AsyncSnapshot<BlocksModel> snapshot) {
                if (snapshot.hasData && snapshot.data.categories != null) {

                  mainCategories = snapshot.data.categories.where((cat) => cat.parent == 0).toList();
                  selectedCategory = mainCategories[selectedCategoryIndex];
                  subCategories = snapshot.data.categories.where((cat) => cat.parent == selectedCategory.id).toList();

                  return buildList(snapshot);
                } else if (snapshot.hasError) {
                  return Text(snapshot.error.toString());
                }
                return Center(child: CircularProgressIndicator());
              },
          )),
        ],
      )),
    );
  }

  buildList(AsyncSnapshot<BlocksModel> snapshot) {

    Color backgroundColor = Theme.of(context).focusColor.withOpacity(0.05);

    return Container(
      child: Row(
        children: <Widget>[
          Container(
            width: 120,
            //color: Theme.of(context).canvasColor,
            child: ListView.builder(
                itemCount: mainCategories.length,
                itemBuilder: (BuildContext ctxt, int index) {
                  return InkWell(
                    onTap: () {
                      setState(() {
                        mainCategoryId = mainCategories[index].id;
                        selectedCategoryIndex = index;
                      });
                    },
                    child: Container(
                      decoration: selectedCategory.id == mainCategories[index].id
                          ? BoxDecoration(
                        border: Border(
                          left: BorderSide( //                   <--- left side
                            color: Theme.of(context).accentColor,
                            width: 2.0,
                          ),
                        ),
                      ) : null,
                      child: Container(
                        color: selectedCategory.id == mainCategories[index].id
                            ? backgroundColor
                            : null,
                        padding: EdgeInsets.fromLTRB(8, 16, 8, 16),
                        child: Text(
                          mainCategories[index].name,
                          style: selectedCategory.id == mainCategories[index].id
                              ? TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w800)
                              : TextStyle(fontSize: 12),
                        ),
                      ),
                    ),
                  );
                }),
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                border: Border(
                  left: BorderSide( //                   <--- left side
                    color: backgroundColor,
                    width: 8.0,
                  ),
                  top: BorderSide( //                    <--- top side
                    color: backgroundColor,
                    width: 8.0,
                  ),
                  right: BorderSide( //                    <--- top side
                    color: backgroundColor,
                    width: 8.0,
                  ),
                ),
              ),
              //padding: EdgeInsets.all(8.0),
              //color: Theme.of(context).canvasColor,
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            var filter = new Map<String, dynamic>();
                            filter['id'] = selectedCategory.id.toString();
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProductList(
                                        filter: filter, homeBloc: widget.homeBloc, name: selectedCategory.name)));
                          },
                          child: Container(
                            height:100,
                            child: CachedNetworkImage(
                              imageUrl: selectedCategory.image,
                              imageBuilder: (context, imageProvider) => Ink.image(
                                child: InkWell(
                                  onTap: () {
                                    var filter = new Map<String, dynamic>();
                                    filter['id'] = selectedCategory.id.toString();
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => ProductList(
                                                filter: filter, homeBloc: widget.homeBloc, name: selectedCategory.name)));
                                  },
                                ),
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                              placeholder: (context, url) =>
                                  Container(color: Colors.black12),
                              errorWidget: (context, url, error) => Container(color: Colors.black12),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border(
                              top: BorderSide( //                   <--- left side
                                color: backgroundColor,
                                width: 8.0,
                              ),
                            )
                        ),
                        //color: Theme.of(context).focusColor,
                        child: GridView.builder(
                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,childAspectRatio: 7/9),
                            itemCount: subCategories.length,
                            itemBuilder: (BuildContext ctxt, int index) {
                              return Container(
                                  child: CategoryItem(subCategories[index], index, snapshot.data.categories));
                            }),
                      ))

                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget CategoryItem(Category category, int i, List<Category> categories) {
    return InkWell(
      onTap: () {
        var filter = new Map<String, dynamic>();
        filter['id'] = category.id.toString();
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductList(
                    filter: filter, homeBloc: widget.homeBloc, name: category.name)));
      },
      child: Container(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Container(
              child: Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 60,
                  height: 60,
                  child: CachedNetworkImage(
                    imageUrl: category.image,
                    imageBuilder: (context, imageProvider) => Card(
                      clipBehavior: Clip.antiAlias,
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      /*
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                          StadiumBorder(
                            side: BorderSide(
                              color: Colors.black12,
                              //width: 1.0,
                            ),
                          ),
                         */
                      child: Ink.image(
                        child: InkWell(
                          onTap: () {
                            var filter = new Map<String, dynamic>();
                            filter['id'] = category.id.toString();
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProductList(
                                        filter: filter, homeBloc: widget.homeBloc, name: category.name)));
                          },
                        ),
                        image: imageProvider,
                        fit: BoxFit.cover,
                      ),
                    ),
                    placeholder: (context, url) => Card(clipBehavior: Clip.antiAlias,
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),),
                    errorWidget: (context, url, error) => Container(color: Colors.black12),
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 5.0),
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    category.name,
                    textAlign: TextAlign.center,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.body2.copyWith(
                      fontSize: 10.0,
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
