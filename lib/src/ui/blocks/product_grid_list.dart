import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../../models/blocks_model.dart';
import 'hex_color.dart';
import 'package:html/parser.dart';

class ProductGridList extends StatefulWidget {
  final Block block;
  final Function onProductClick;
  ProductGridList({Key key, this.block, this.onProductClick}) : super(key: key);
  @override
  _ProductGridListState createState() => _ProductGridListState();
}

class _ProductGridListState extends State<ProductGridList> {
  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: EdgeInsets.fromLTRB(
        double.parse(widget.block.paddingLeft.toString()),
        0.0,
        double.parse(widget.block.paddingRight.toString()),
        double.parse(widget.block.paddingBottom.toString()),
      ),
      sliver: SliverGrid(
        // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: int.parse(widget.block.layoutGridCol.toString()),
            childAspectRatio:
                widget.block.childWidth / widget.block.childHeight,
            mainAxisSpacing: widget.block.paddingBetween,
            crossAxisSpacing: widget.block.paddingBetween),
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                    double.parse(widget.block.borderRadius.toString())),
                color: HexColor(widget.block.bgColor),
              ),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                      double.parse(widget.block.borderRadius.toString())),
                ),
                margin: EdgeInsets.all(0.0),
                clipBehavior: Clip.antiAlias,
                elevation: widget.block.elevation.toDouble(),
                child: InkWell(
                  splashColor: HexColor(widget.block.bgColor).withOpacity(0.1),
                  onTap: () {
                    widget.onProductClick(widget.block.products[index]);
                  },
                  child: Column(
                    children: <Widget>[
                      AspectRatio(
                        aspectRatio: 18.0 / 16.0,
                        child: CachedNetworkImage(
                          imageUrl: widget.block.products[index].images[0].src,
                          imageBuilder: (context, imageProvider) => Ink.image(
                            child: InkWell(
                              splashColor: HexColor(widget.block.bgColor)
                                  .withOpacity(0.1),
                              onTap: () {
                                widget.onProductClick(
                                    widget.block.products[index]);
                              },
                            ),
                            image: imageProvider,
                            fit: BoxFit.cover,
                          ),
                          placeholder: (context, url) =>
                              Container(color: Colors.black12),
                          errorWidget: (context, url, error) =>
                              Container(color: Colors.black12),
                        ),
                      ),
                      SizedBox(height: 10.0),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(4.0, 0, 4, 4),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              widget.block.products[index].name,
                              maxLines: 1,
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: 4.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.baseline,
                              textBaseline: TextBaseline.alphabetic,
                              children: <Widget>[
                                Text(
                                    (widget.block.products[index]
                                                    .formattedPrice !=
                                                null &&
                                            widget.block.products[index]
                                                .formattedPrice.isNotEmpty)
                                        ? _parseHtmlString(widget.block
                                            .products[index].formattedPrice)
                                        : '',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                    )),
                                SizedBox(width: 4.0),
                                Text(
                                    (widget.block.products[index].salePrice !=
                                                null &&
                                            widget.block.products[index]
                                                    .salePrice !=
                                                0)
                                        ? _parseHtmlString(widget
                                            .block
                                            .products[index]
                                            .formattedSalesPrice)
                                        : '',
                                    style: Theme.of(context)
                                        .textTheme
                                        .caption
                                        .copyWith(
                                          decoration:
                                              TextDecoration.lineThrough,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                        )),
                              ],
                            ),
                            SizedBox(height: 8.0),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
          childCount: widget.block.products.length,
        ),
      ),
    );
  }
}

String _parseHtmlString(String htmlString) {
  var document = parse(htmlString);

  String parsedString = parse(document.body.text).documentElement.text;

  return parsedString;
}
