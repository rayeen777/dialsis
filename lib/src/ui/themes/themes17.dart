import 'package:flutter/material.dart';
import '../colors.dart';

TextTheme _buildTextTheme(TextTheme base) {
  return base.copyWith(
    title: base.title.copyWith(
      fontFamily: 'GoogleSans',
      fontSize: 18.0,
    ),
    headline: base.headline.copyWith(
      fontWeight: FontWeight.w500,
    ),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
    ),
  );
}

ThemeData buildDarkTheme17() {
  const Color primaryColor = Color(0xFF0175c2);
  const Color secondaryColor = Color(0xFF13B9FD);
  final ColorScheme colorScheme = const ColorScheme.dark().copyWith(
    primary : primaryColor,
    secondary: secondaryColor,
  );
  final ThemeData base = ThemeData(
    brightness: Brightness.dark,
  );
  return base.copyWith(
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: mButtonColor,
      textTheme: ButtonTextTheme.primary,
    ),
  );
}

ThemeData buildLightTheme17() {
  final ThemeData base = ThemeData(
    primaryColorBrightness: Brightness.dark,
    brightness: Brightness.light,
    primarySwatch: Colors.red,
    //Color(0xFFffab40),
  );
  return base.copyWith(
    accentColor: Colors.red[700],
    primaryColor: Color(0xFF4E0D3A),
    //indicatorColor: Colors.red,
   // buttonColor: mButtonColor,
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor:Color(0xFFE30425),
      textTheme: ButtonTextTheme.primary,
    ),
    scaffoldBackgroundColor: Color(0xFFfafafa),//use Color.fromRGBO(R-red,G-green,B-Blue,O-Opacity)
    backgroundColor: Color(0xFFc62828),
    cardColor: Color(0xFFe0e0e0),
    textSelectionColor: mAccentColor,
    errorColor: mErrorColor,
    textTheme: _buildNewTextTheme(base.textTheme),
    primaryTextTheme: _buildPrimaryTextTheme(base.primaryTextTheme),
    accentTextTheme: _buildNewTextTheme(base.accentTextTheme),
    //primaryIconTheme: base.iconTheme.copyWith(
    //    color: mPrimaryTextColor,
    //),
    inputDecorationTheme: InputDecorationTheme(
      //border: OutlineInputBorder(),
    ),
  );
}

TextTheme _buildNewTextTheme(TextTheme base) {
  return base.copyWith(
    headline: base.headline.copyWith(
      fontWeight: FontWeight.w500,
    ),
    title: base.title.copyWith(
        fontSize: 18.0
    ),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
      //color: Colors.black
    ),
  ).apply(
    fontFamily: 'RedHatDisplay',
      //displayColor: mTextDisplayColor,
     //bodyColor: Color(0xFFE30425),
  );
}

TextTheme _buildPrimaryTextTheme(TextTheme base) {
  return base.copyWith(
    headline: base.headline.copyWith(
      fontWeight: FontWeight.w500,
    ),
    title: base.title.copyWith(
        fontSize: 18.0
    ),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
    ),
  ).apply(
    fontFamily: 'RedHatDisplay',
    //displayColor: mPrimaryTextColor,
    //bodyColor: mPrimaryTextColor,
  );
}

/*
ThemeData _buildDarkTheme() {
  const Color primaryColor = Color(0xFF0175c2);
  const Color secondaryColor = Color(0xFF13B9FD);
  final ColorScheme colorScheme = const ColorScheme.dark().copyWith(
    primary: primaryColor,
    secondary: secondaryColor,
  );
  final ThemeData base = ThemeData(
    brightness: Brightness.dark,
    accentColorBrightness: Brightness.dark,
    //primaryColor: primaryColor,
    primaryColorDark: const Color(0xFF0050a0),
    primaryColorLight: secondaryColor,
    buttonColor: primaryColor,
    indicatorColor: Colors.white,
    toggleableActiveColor: const Color(0xFF6997DF),
    accentColor: secondaryColor,
    canvasColor: const Color(0xFF202124),
    scaffoldBackgroundColor: const Color(0xFF202124),
    backgroundColor: const Color(0xFF202124),
    errorColor: const Color(0xFFB00030),
    buttonTheme: ButtonThemeData(
      colorScheme: colorScheme,
      textTheme: ButtonTextTheme.primary,
    ),
  );
  return base.copyWith(
    textTheme: _buildTextTheme(base.textTheme),
    primaryTextTheme: _buildTextTheme(base.primaryTextTheme),
    accentTextTheme: _buildTextTheme(base.accentTextTheme),
  );
}

ThemeData _buildLightTheme() {
  const Color primaryColor = Colors.white;//Color(0xFF0175c2);
  const Color secondaryColor = Colors.black87;//Color(0xFF13B9FD);
  final ColorScheme colorScheme = const ColorScheme.light().copyWith(
    primary: Color(0xFF0175c2),
    secondary: secondaryColor,
  );
  final ThemeData base = ThemeData(
    brightness: Brightness.light,
    accentColorBrightness: Brightness.dark,
    colorScheme: colorScheme,
    primaryColor: primaryColor,
    buttonColor: Color(0xFF0175c2),
    indicatorColor: Colors.white,
    toggleableActiveColor: const Color(0xFF1E88E5),
    splashColor: Colors.white24,
    splashFactory: InkRipple.splashFactory,
    accentColor: secondaryColor,
    canvasColor: Colors.white,
    scaffoldBackgroundColor: Colors.white,
    backgroundColor: Colors.white,
    errorColor: const Color(0xFFB00020),
    buttonTheme: ButtonThemeData(
      colorScheme: colorScheme,
      textTheme: ButtonTextTheme.primary,
    ),
  );
  return base.copyWith(
    textTheme: _buildTextTheme(base.textTheme),
    primaryTextTheme: _buildTextTheme(base.primaryTextTheme),
    accentTextTheme: _buildTextTheme(base.accentTextTheme),
  );
}
 */
