import 'package:flutter/material.dart';
import '../colors.dart';

TextTheme _buildTextTheme(TextTheme base) {
  return base.copyWith(
    title: base.title.copyWith(
      fontFamily: 'GoogleSans',
      fontSize: 18.0,
    ),
    headline: base.headline.copyWith(
      fontWeight: FontWeight.w500,
    ),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
    ),
  );
}

ThemeData buildDarkTheme8() {
  const Color primaryColor = Color(0xFF0175c2);
  const Color secondaryColor = Color(0xFF13B9FD);
  final ColorScheme colorScheme = const ColorScheme.dark().copyWith(
    primary : primaryColor,
    secondary: secondaryColor,
  );
  final ThemeData base = ThemeData(
    brightness: Brightness.dark,
  );
  return base.copyWith(
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: mButtonColor,
      textTheme: ButtonTextTheme.primary,
    ),
  );
}

ThemeData buildLightTheme8() {
  final ThemeData base = ThemeData(
    primaryColorBrightness: Brightness.dark,
    brightness: Brightness.light,
    primarySwatch: Colors.red,
    //Color(0xFFffab40),
  );
  return base.copyWith(
    accentColor: Color(0xFF212121),
    primaryColor: Color(0xFF305961),
    //indicatorColor: Colors.red,
    // buttonColor: mButtonColor,
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor:Color(0xFFE43A18),
      textTheme: ButtonTextTheme.primary,
    ),
    scaffoldBackgroundColor: Color(0xFFA4B6B9),//use Color.fromRGBO(R-red,G-green,B-Blue,O-Opacity)
    backgroundColor: Color(0xFFff0266),
    cardColor: Color(0xFFCCCED6),
    textSelectionColor: mAccentColor,
    errorColor: mErrorColor,
    textTheme: _buildNewTextTheme(base.textTheme),
    primaryTextTheme: _buildPrimaryTextTheme(base.primaryTextTheme),
    accentTextTheme: _buildNewTextTheme(base.accentTextTheme),
    //primaryIconTheme: base.iconTheme.copyWith(
    //    color: mPrimaryTextColor,
    //),
    inputDecorationTheme: InputDecorationTheme(
      //border: OutlineInputBorder(),
    ),
  );
}

TextTheme _buildNewTextTheme(TextTheme base) {
  return base.copyWith(
    headline: base.headline.copyWith(
      fontWeight: FontWeight.w500,
    ),
    title: base.title.copyWith(
        fontSize: 18.0
    ),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
      //color: Colors.black
    ),
  ).apply(
    fontFamily: 'Montserrat',
    // displayColor: mTextDisplayColor,
    bodyColor: Color(0xFF212121),
  );
}

TextTheme _buildPrimaryTextTheme(TextTheme base) {
  return base.copyWith(
    headline: base.headline.copyWith(
      fontWeight: FontWeight.w500,
    ),
    title: base.title.copyWith(
        fontSize: 18.0
    ),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
    ),
  ).apply(
    fontFamily: 'Montserrat',
    //displayColor: mPrimaryTextColor,
    //bodyColor: mPrimaryTextColor,
  );
}
