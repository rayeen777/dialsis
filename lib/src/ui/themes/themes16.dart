import 'package:flutter/material.dart';
import '../colors.dart';

TextTheme _buildTextTheme(TextTheme base) {
  return base.copyWith(
    title: base.title.copyWith(
      fontFamily: 'GoogleSans',
      fontSize: 18.0,
    ),
    headline: base.headline.copyWith(
      fontWeight: FontWeight.w500,
    ),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
    ),
  );
}

ThemeData buildDarkTheme16() {
  const Color primaryColor = Color(0xFF0175c2);
  const Color secondaryColor = Color(0xFF13B9FD);
  final ColorScheme colorScheme = const ColorScheme.dark().copyWith(
    primary : primaryColor,
    secondary: secondaryColor,
  );
  final ThemeData base = ThemeData(
    brightness: Brightness.dark,
  );
  return base.copyWith(
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: mButtonColor,
      textTheme: ButtonTextTheme.primary,
    ),
  );
}

ThemeData buildLightTheme16() {
  final ThemeData base = ThemeData(
    primaryColorBrightness: Brightness.dark,
    brightness: Brightness.light,
    primarySwatch: Colors.lightGreen,
  );
  return base.copyWith(
    accentColor: Color(0xFF212121),
    primaryColor: Color(0xFF2E4527),
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor:Color(0xFF9DC376),
      textTheme: ButtonTextTheme.primary,
    ),
    scaffoldBackgroundColor: Color(0xFFfefefe),//use Color.fromRGBO(R-red,G-green,B-Blue,O-Opacity)
    backgroundColor: Color(0xFFff0266),
    cardColor: Color(0xFF9DC376),
    textSelectionColor: mAccentColor,
    errorColor: mErrorColor,
    textTheme: _buildNewTextTheme(base.textTheme),
    primaryTextTheme: _buildPrimaryTextTheme(base.primaryTextTheme),
    accentTextTheme: _buildNewTextTheme(base.accentTextTheme),
  );
}

TextTheme _buildNewTextTheme(TextTheme base) {
  return base.copyWith(
    headline: base.headline.copyWith(
      fontWeight: FontWeight.w500,
    ),
    title: base.title.copyWith(
        fontSize: 18.0
    ),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
    ),
  ).apply(
    fontFamily: 'Rubik',
    bodyColor: Color(0xFF212121),
  );
}

TextTheme _buildPrimaryTextTheme(TextTheme base) {
  return base.copyWith(
    headline: base.headline.copyWith(
      fontWeight: FontWeight.w500,
    ),
    title: base.title.copyWith(
        fontSize: 18.0
    ),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
    ),
  ).apply(
    fontFamily: 'Rubik',
  );
}
