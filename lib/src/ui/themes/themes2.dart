import 'package:flutter/material.dart';

TextTheme _buildTextTheme(TextTheme base) {
  return base.copyWith(
    title: base.title.copyWith(
      fontFamily: 'Raleway',
      fontSize: 18.0,
    ),
    headline: base.headline.copyWith(
      fontWeight: FontWeight.w500,
    ),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
    ),
  );
}

ThemeData buildDarkTheme2() {
  const Color primaryColor = Color(0xFF0175c2);
  const Color secondaryColor = Color(0xFF13B9FD);
  final ColorScheme colorScheme = const ColorScheme.dark().copyWith(
    primary : primaryColor,
    secondary: secondaryColor,
  );
  final ThemeData base = ThemeData(
    brightness: Brightness.dark,
  );
  return base.copyWith(
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: _kPrimaryColor,
      textTheme: ButtonTextTheme.primary,
    ),
  );
}

IconThemeData _customIconTheme(IconThemeData original) {
  return original.copyWith(
      color: _kPrimaryColorLight,
  );
}

IconThemeData _primaryIconTheme(IconThemeData original) {
  return original.copyWith(color: _kOnPrimaryColor);
}

ThemeData buildLightTheme2() {
  final ThemeData base = ThemeData(
    primarySwatch: Colors.green,
    brightness: Brightness.light
  );
  return base.copyWith(
    colorScheme: buttonColorScheme,
    primaryColor: _kPrimaryColor,
    primaryColorLight: _kPrimaryColorLight,
    primaryColorDark: _kPrimaryColorDark,
    primaryColorBrightness: Brightness.dark,
    accentColor: _secondaryColor,
    accentColorBrightness: Brightness.dark,
    buttonColor: _kPrimaryColor,
    scaffoldBackgroundColor: Color(0xFFFFFBE6),
    cardColor: _kCardColor,
    dividerColor: _kDividerColor,
    textSelectionColor: _kPrimaryColor,
    errorColor: _kErrorColor,
    buttonTheme: const ButtonThemeData(
      colorScheme: buttonColorScheme,
      textTheme: ButtonTextTheme.normal,
    ),
    primaryIconTheme: _primaryIconTheme(base.iconTheme),
    inputDecorationTheme:
    const InputDecorationTheme(border: OutlineInputBorder()),
    textTheme: _buildShrineTextTheme(base.textTheme),
    primaryTextTheme: _buildPrimaryTextTheme(base.primaryTextTheme),
    accentTextTheme: _buildShrineTextTheme(base.accentTextTheme),
    iconTheme: _customIconTheme(base.iconTheme),
  );
}

TextTheme _buildPrimaryTextTheme(TextTheme base) {
  return base
      .copyWith(
    headline: base.headline.copyWith(fontWeight: FontWeight.w500),
    title: base.title.copyWith(fontSize: 18.0),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
    ),
    body2: base.body2.copyWith(
      fontWeight: FontWeight.w500,
      fontSize: 16.0,
    ),
    button: base.button.copyWith(
      fontWeight: FontWeight.w500,
      fontSize: 14.0,
    ),
  ).apply(
    fontFamily: 'Raleway',
    displayColor: _kOnPrimaryColor,
    bodyColor: _kOnPrimaryColor,
  );
}

TextTheme _buildShrineTextTheme(TextTheme base) {
  return base
      .copyWith(
    headline: base.headline.copyWith(fontWeight: FontWeight.w500),
    title: base.title.copyWith(fontSize: 18.0),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
    ),
    body2: base.body2.copyWith(
      fontWeight: FontWeight.w500,
      fontSize: 16.0,
    ),
    button: base.button.copyWith(
      fontWeight: FontWeight.w500,
      fontSize: 14.0,
    ),
  ).apply(
    fontFamily: 'Rubik',
    displayColor: _kBodyTextColor,
    bodyColor: _kBodyTextColor,
  );
}

const ColorScheme buttonColorScheme = ColorScheme(
  primary: _kPrimaryColor,
  primaryVariant: _kPrimaryVariantColor,
  secondary: _secondaryColor,
  secondaryVariant: _secondaryVariantColor,
  surface: _kSurfaceColor,
  background: _kBackground,
  error: _kErrorColor,
  onPrimary: _kOnPrimaryColor,
  onSecondary: _onSecondaryColor,
  onSurface: _kOnSurfaceColor,
  onBackground: _kOnBackground,
  onError: _kOnErrorColor,
  brightness: Brightness.dark,
);


const _kPrimaryColor = Color(0xFF356859);
const _kPrimaryColorDark = Color(0xFF043d30);
const _kPrimaryColorLight = Color(0xFF629686);
const _kPrimaryVariantColor = Color(0xFF043d30);
const _kOnPrimaryColor = Color(0xFFFFFFFF);

const _secondaryColor = Color(0xFFFD5523);
const _secondaryVariantColor = Color(0xFFc21900);
const _onSecondaryColor = Color(0xFFFFFFFF);

const _kSurfaceColor = Color(0xFFFFFFFF);
const _kOnSurfaceColor = Color(0xFF111111);

const _kBackground = Colors.white;
const _kOnBackground = Colors.black;

const _kErrorColor = Color(0xFFC5032B);
const _kOnErrorColor = Color(0xFFC5032B);

const _kDividerColor = Color(0xFFB9E4C9);

const _kScaffoldColor = Color(0xFFFFFBE6);

const _kBodyIconColor = Color(0xFF356859);

const _kCardColor = Color(0xFFB9E4C9);


const _kBodyTextColor = Color(0xFF37966F);

