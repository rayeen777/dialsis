import 'package:flutter/material.dart';
import '../colors.dart';

ThemeData buildDarkTheme() {
  const Color primaryColor = Color(0xFF0175c2);
  const Color secondaryColor = Color(0xFF13B9FD);
  final ColorScheme colorScheme = const ColorScheme.dark().copyWith(
    primary : primaryColor,
    secondary: secondaryColor,
  );
  final ThemeData base = ThemeData(
    brightness: Brightness.dark,
  );
  return base.copyWith(
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: mButtonColor,
      textTheme: ButtonTextTheme.primary,
    ),
  );
}

ThemeData buildLightTheme() {
  final ThemeData base = ThemeData(
    primaryColorBrightness: mPrimaryColorBrightness,
    brightness: mBrightness,
  );
  return base.copyWith(
    accentColor: mAccentColor,
    primaryColor: mPrimaryColor,
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: mButtonColor,
      textTheme: ButtonTextTheme.primary,
    ),
  );
}