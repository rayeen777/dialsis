import 'package:flutter/material.dart';

class GalleryTheme {
  const GalleryTheme(this.name, this.data);

  final String name;
  final ThemeData data;
}