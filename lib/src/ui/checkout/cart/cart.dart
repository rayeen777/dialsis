import 'package:flutter/material.dart';
import '../../accounts/language/app_localizations.dart';
import '../../../blocs/home_bloc.dart';
import '../../../models/cart/cart_model.dart';
import 'package:intl/intl.dart';
import '../../../models/checkout/checkout_form_model.dart';
import '../../../models/country_model.dart';
import '../../../resources/countires.dart';
import '../../color_override.dart';
import '../address.dart';
import 'package:html/parser.dart';

const double _leftColumnWidth = 60.0;

class CartPage extends StatefulWidget {
  final HomeBloc homeBloc;

  CartPage({Key key, this.homeBloc}) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {

  TextEditingController _couponCodeController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    widget.homeBloc.getCart();
    widget.homeBloc.getCheckoutForm();
    widget.homeBloc.checkoutForm.listen((onData) => setAddressCountry(onData));
  }

  List<Widget> _createShoppingCartRows(AsyncSnapshot<CartModel> snapshot) {
    int id;
    return snapshot.data.cartContents
        .map(
          (CartContent content) => ShoppingCartRow(
            product: content,
            quantity: content.quantity,
            onPressed: () {
              widget.homeBloc.removeItemFromCart(content.key);
            },
            onIncreaseQty: () {
              widget.homeBloc.increaseQty(content.key, content.quantity);
            },
            onDecreaseQty: () {
              widget.homeBloc.decreaseQty(content.key, content.quantity);
            },
          ),
        )
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData localTheme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        title: Text(AppLocalizations.of(context).translate("cart")),
      ),
      body: SafeArea(
        child: StreamBuilder<CartModel>(
            stream: widget.homeBloc.cart,
            builder: (context, snapshot) {
              if (snapshot.hasData && snapshot.data.cartContents.length != 0) {
                return buildCart(localTheme, snapshot);
              } else if (snapshot.hasData &&
                  snapshot.data.cartContents.length == 0) {
                return Center(
                  child: Text(
                      AppLocalizations.of(context).translate("no items in cart")),
                );
              } else
                return Center(
                  child: CircularProgressIndicator(),
                );
            }),
      ),
    );
  }

  Stack buildCart(ThemeData localTheme, AsyncSnapshot<CartModel> snapshot) {
    return Stack(
      children: <Widget>[
        ListView(
          children: <Widget>[
            SizedBox(height: 16.0),
            Row(
              children: <Widget>[
                SizedBox(
                  width: _leftColumnWidth,
                ),
                Text(
                  AppLocalizations.of(context).translate("cart"),
                  style: localTheme.textTheme.subhead
                      .copyWith(fontWeight: FontWeight.w600),
                ),
                const SizedBox(width: 16.0),
                Text(
                    '${snapshot.data.cartContents.length} ${AppLocalizations.of(context).translate("items")}'),
              ],
            ),
            const SizedBox(height: 16.0),
            Column(
              children: _createShoppingCartRows(snapshot),
            ),
            Row(
              children: <Widget>[
                SizedBox(
                  width: _leftColumnWidth,
                ),
                Container(
                  width: MediaQuery.of(context).size.width - _leftColumnWidth,
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0.0, 0.0, 16.0, 16.0),
                          child: PrimaryColorOverride(
                            child: TextFormField(
                              controller: _couponCodeController,
                              decoration: InputDecoration(
                                  hintText: AppLocalizations.of(context).translate("coupon_code"),
                                  suffix: FlatButton(
                                    child: Text(AppLocalizations.of(context).translate("apply").toUpperCase()),
                                    onPressed: () {
                                      if (_formKey.currentState.validate()) {
                                        _formKey.currentState.save();
                                        widget.homeBloc.applyCoupon(_couponCodeController.text);
                                      }
                                    },
                                  )
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return AppLocalizations.of(context).translate("please_enter_coupon_code");
                                }
                                return null;
                              },
                              onSaved: (value) {
                                widget.homeBloc.formData['billing_first_name'] = value;
                              },
                            ),
                          ),
                        ),
                      ]
                    ),
                  ),
                ),
              ],
            ),
            ShoppingCartSummary(
                cartTotals: snapshot.data.cartTotals,
                currency: snapshot.data.currency),
            const SizedBox(height: 100.0),
          ],
        ),
        Positioned(
          bottom: 16.0,
          left: 16.0,
          right: 16.0,
          child: RaisedButton(
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(2.0)),
            ),
            //color: Colors.red,
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 12.0),
              child: Text(AppLocalizations.of(context).translate("checkout")),
            ),
            onPressed: () {
              //model.clearCart();
              //ExpandingBottomSheet.of(context).close();
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          Address(homeBloc: widget.homeBloc)));
            },
          ),
        ),
      ],
    );
  }

  setAddressCountry(CheckoutFormModel onData) {
    if (onData.billingCountry != null && onData.billingCountry.isNotEmpty) {
      widget.homeBloc.initialSelectedCountry = onData.billingCountry;
    }
    if (onData.billingState != null && onData.billingState.isNotEmpty) {
      List<CountryModel> countries =
          countryModelFromJson(JsonStrings.listOfSimpleObjects);
      CountryModel country =
          countries.singleWhere((item) => item.value == onData.billingCountry);
      if (country.regions != null &&
          country.regions.any((item) => item.value == onData.billingState)) {
        widget.homeBloc.formData['billing_state'] = onData.billingState;
      } else
        widget.homeBloc.formData['billing_state'] = null;
    }
  }
}

class ShoppingCartSummary extends StatelessWidget {
  const ShoppingCartSummary({
    @required this.cartTotals,
    @required this.currency,
  });

  final CartTotals cartTotals;
  final String currency;

  @override
  Widget build(BuildContext context) {
    final smallAmountStyle = Theme.of(context).textTheme.body1;
    final largeAmountStyle = Theme.of(context).textTheme.title;

    return Row(
      children: <Widget>[
        const SizedBox(width: _leftColumnWidth),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Column(
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Text(AppLocalizations.of(context).translate("total")),
                    ),
                    Text(
                      _parseHtmlString(cartTotals.total),
                      style: largeAmountStyle,
                    ),
                  ],
                ),
                const SizedBox(height: 16.0),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(AppLocalizations.of(context).translate("subtotal")),
                    ),
                    Text(
                      _parseHtmlString(cartTotals.subtotal),
                      style: smallAmountStyle,
                    ),
                  ],
                ),
                const SizedBox(height: 4.0),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(AppLocalizations.of(context).translate("shipping")),
                    ),
                    Text(
                      _parseHtmlString(cartTotals.shippingTotal),
                      style: smallAmountStyle,
                    ),
                  ],
                ),
                const SizedBox(height: 4.0),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(AppLocalizations.of(context).translate("discount")),
                    ),
                    Text(
                      _parseHtmlString(cartTotals.discountTotal),
                      style: smallAmountStyle,
                    ),
                  ],
                ),
                const SizedBox(height: 4.0),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(AppLocalizations.of(context).translate("tax")),
                    ),
                    Text(
                      _parseHtmlString(cartTotals.totalTax),
                      style: smallAmountStyle,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class ShoppingCartRow extends StatelessWidget {
  const ShoppingCartRow(
      {@required this.product,
      @required this.quantity,
      this.onPressed,
      this.onIncreaseQty,
      this.onDecreaseQty});

  final CartContent product;
  final int quantity;
  final VoidCallback onPressed;
  final VoidCallback onIncreaseQty;
  final VoidCallback onDecreaseQty;

  @override
  Widget build(BuildContext context) {
    final NumberFormat formatter = NumberFormat.simpleCurrency(
      decimalDigits: 2,
      locale: Localizations.localeOf(context).toString(),
    );
    final ThemeData localTheme = Theme.of(context);

    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: Row(
        key: ValueKey<int>(product.productId),
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: _leftColumnWidth,
            child: IconButton(
              icon: const Icon(Icons.remove_circle_outline),
              onPressed: onPressed,
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: Column(
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Image.network(
                        product.thumb,
                        //package: product.assetPackage,
                        fit: BoxFit.cover,
                        width: 75.0,
                        height: 75.0,
                      ),
                      const SizedBox(width: 16.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              product.name,
                              style: localTheme.textTheme.subhead
                                  .copyWith(fontWeight: FontWeight.w600),
                            ),
                            Text(_parseHtmlString(product.formattedPrice)),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                IconButton(
                                  icon: const Icon(Icons.add),
                                  onPressed: onIncreaseQty,
                                ),
                                product.loadingQty
                                    ? Container(
                                        width: 20,
                                        height: 20,
                                        child: CircularProgressIndicator(
                                          strokeWidth: 2.0,
                                        ))
                                    : Container(
                                        width: 20,
                                        height: 16,
                                        child: Center(
                                            child: Text(
                                                product.quantity.toString()))),
                                IconButton(
                                  icon: const Icon(Icons.remove),
                                  onPressed: onDecreaseQty,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 16.0),
                  const Divider(
                    color: Colors.black26,
                    height: 10.0,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

String _parseHtmlString(String htmlString) {
  var document = parse(htmlString);
  String parsedString = parse(document.body.text).documentElement.text;
  return parsedString;
}
