import 'package:flutter/material.dart';
import '../../ui/accounts/language/app_localizations.dart';
import '../../ui/checkout/place_picker.dart';
import '../../blocs/home_bloc.dart';
import '../../models/checkout/checkout_form_model.dart' hide Country;
import '../../ui/checkout/payment_method.dart';
import 'package:html/parser.dart';
import '../color_override.dart';
import 'checkout_one_page.dart';

class Address extends StatefulWidget {
  final HomeBloc homeBloc;

  Address({Key key, this.homeBloc}) : super(key: key);

  @override
  _AddressState createState() => _AddressState();
}

class _AddressState extends State<Address> {

  List<Region> regions;
  TextEditingController _billingAddress1Controller = TextEditingController();
  TextEditingController _billingCityController = TextEditingController();
  TextEditingController _billingPostCodeController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
    widget.homeBloc.getCheckoutForm();
    widget.homeBloc.updateOrderReview();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        title: Text(AppLocalizations.of(context).translate("address")),
      ),
      body: StreamBuilder<CheckoutFormModel>(
          stream: widget.homeBloc.checkoutForm,
          builder: (context, snapshot) {
            return snapshot.hasData
                ? buildCheckoutForm(context, snapshot)
                : Center(child: CircularProgressIndicator());
          }),
    );
  }

  buildCheckoutForm(BuildContext context, AsyncSnapshot<CheckoutFormModel> snapshot) {

    if(snapshot.data.countries.indexWhere((country) => country.value == snapshot.data.billingCountry) != -1) {
      regions = snapshot.data.countries.singleWhere((country) => country.value == snapshot.data.billingCountry).regions;
    } else if(snapshot.data.countries.indexWhere((country) => country.value == snapshot.data.billingCountry) == -1) {
      snapshot.data.billingCountry = snapshot.data.countries.first.value;
    } if(regions != null) {
      snapshot.data.billingState = regions.any((z) => z.value == snapshot.data.billingState) ? snapshot.data.billingState
          : regions.first.value;
    }

    if(_billingAddress1Controller.text.isEmpty)
    _billingAddress1Controller.text = snapshot.data.billingAddress1;
    if(_billingCityController.text.isEmpty)
    _billingCityController.text = snapshot.data.billingCity;
    if(_billingPostCodeController.text.isEmpty)
    _billingPostCodeController.text = snapshot.data.billingPostcode;

    return ListView(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                FlatButton(
                  colorBrightness: Brightness.light,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:<Widget>[
                      Icon(Icons.add_location),
                      Text("Pick Delivery location")
                    ], ),
                  onPressed: () {
                    showPlacePicker(snapshot);
                  },
                ),
                PrimaryColorOverride(
                  child: TextFormField(
                    initialValue: snapshot.data.billingFirstName,
                    decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("firstname")),
                    validator: (value) {
                      if (value.isEmpty) {
                        return AppLocalizations.of(context).translate("please_enter_first_name");
                      }
                      return null;
                    },
                    onSaved: (value) {
                      widget.homeBloc.formData['billing_first_name'] = value;
                    },
                  ),
                ),
                SizedBox(height: 12.0),
                PrimaryColorOverride(
                  child: TextFormField(
                    initialValue: snapshot.data.billingLastName,
                    decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("lastname")),
                    validator: (value) {
                      if (value.isEmpty) {
                        return AppLocalizations.of(context).translate("please_enter_last_name");
                      }
                      return null;
                    },
                    onSaved: (value) {
                      widget.homeBloc.formData['billing_last_name'] = value;
                    },
                  ),
                ),
                SizedBox(height: 12.0),
                PrimaryColorOverride(
                  child: TextFormField(
                    controller: _billingAddress1Controller,
                    decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("address")),
                    validator: (value) {
                      if (value.isEmpty) {
                        return AppLocalizations.of(context).translate("please_enter_address");
                      }
                      return null;
                    },
                    onSaved: (value) {
                      widget.homeBloc.formData['billing_address_1'] = value;
                    },
                  ),
                ),
                /*SizedBox(height: 12.0),
                PrimaryColorOverride(
                  child: TextFormField(
                    initialValue: snapshot.data.billingAddress2,
                    decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("address")+'2'),
                    onSaved: (value) {
                      widget.homeBloc.formData['billing_address_2'] = value;
                    },
                  ),
                ),*/
                SizedBox(height: 12.0),
                PrimaryColorOverride(
                  child: TextFormField(
                    controller: _billingCityController,
                    decoration: InputDecoration(
                      labelText: AppLocalizations.of(context).translate("city"),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return AppLocalizations.of(context).translate("please_enter_city");
                      }
                      return null;
                    },
                    onSaved: (value) {
                      widget.homeBloc.formData['billing_city'] = value;
                    },
                  ),
                ),
                SizedBox(height: 12.0),
                PrimaryColorOverride(
                  child: TextFormField(
                    controller: _billingPostCodeController,
                    decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("postcode")),
                    validator: (value) {
                      if (value.isEmpty) {
                        return AppLocalizations.of(context).translate("please_enter_post_code");
                      }
                      return null;
                    },
                    onSaved: (value) {
                      widget.homeBloc.formData['billing_postcode'] = value;
                    },
                  ),
                ),
                SizedBox(height: 12.0),
                PrimaryColorOverride(
                  child: TextFormField(
                    initialValue: snapshot.data.billingEmail,
                    decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("email")),
                    onSaved: (value) {
                      widget.homeBloc.formData['billing_email'] = value;
                    },
                  ),
                ),
                SizedBox(height: 12.0),
                PrimaryColorOverride(
                  child: TextFormField(
                    initialValue: snapshot.data.billingPhone,
                    decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("phonenumber")),
                    validator: (value) {
                      if (value.isEmpty) {
                        return AppLocalizations.of(context).translate("please_enter_phone_number");
                      }
                      return null;
                    },
                    onSaved: (value) {
                      widget.homeBloc.formData['billing_phone'] = value;
                    },
                  ),
                ),
                SizedBox(height: 20,),
                DropdownButton<String>(
                  value: snapshot.data.billingCountry,
                  hint: Text(AppLocalizations.of(context).translate("country")),
                  isExpanded: true,
                  icon: Icon(Icons.arrow_drop_down),
                  iconSize: 24,
                  elevation: 16,
                  underline: Container(
                    height: 2,
                    color: Theme.of(context).dividerColor,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      snapshot.data.billingCountry = newValue;
                    });
                  },
                  items: snapshot.data.countries
                      .map<DropdownMenuItem<String>>(
                          (value) {
                        return DropdownMenuItem<String>(
                          value: value.value != null ? value.value : '',
                          child: Text(_parseHtmlString(value.label)),
                        );
                      }).toList(),
                ),
                regions != null ? Column(
                  children: <Widget>[
                    SizedBox(height: 20,),
                    DropdownButton<String>(
                      value: snapshot.data.billingState,
                      hint: Text(AppLocalizations.of(context).translate("state")),
                      isExpanded: true,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 24,
                      elevation: 16,
                      underline: Container(
                        height: 2,
                        color: Theme.of(context).dividerColor,
                      ),
                      onChanged: (String newValue) {
                        setState(() {
                          snapshot.data.billingState =
                              newValue;
                        });
                      },
                      items: regions
                          .map<DropdownMenuItem<String>>(
                              (value) {
                            return DropdownMenuItem<String>(
                              value: value.value != null ? value.value : '',
                              child: Text(_parseHtmlString(value.label)),
                            );
                          }).toList(),
                    ),
                  ],
                ) : PrimaryColorOverride(
                  child: TextFormField(
                    initialValue:
                    widget.homeBloc.formData['billing_state'],
                    decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("state")),
                    validator: (value) {
                      if (value.isEmpty) {
                        return AppLocalizations.of(context).translate("please_enter_state");
                      }
                    },
                    onSaved: (val) => setState(() =>
                    widget.homeBloc.formData['billing_state'] = val),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              RaisedButton(
                onPressed: () {
                  widget.homeBloc.formData['security'] =
                      snapshot.data.nonce.updateOrderReviewNonce;
                  widget.homeBloc
                      .formData['woocommerce-process-checkout-nonce'] =
                      snapshot.data.wpnonce;
                  widget.homeBloc.formData['wc-ajax'] = 'update_order_review';
                  widget.homeBloc.formData['billing_country'] = snapshot.data.billingCountry;
                  if (_formKey.currentState.validate()) {
                    _formKey.currentState.save();

                    if(regions != null) {
                      widget.homeBloc.formData['billing_state'] = snapshot.data.billingState;
                    }

                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CheckoutOnePage(
                              homeBloc: widget.homeBloc,
                            )));
                  }
                },
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(2.0)),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Text(
                    AppLocalizations.of(context).translate("continue"),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  void showPlacePicker(AsyncSnapshot<CheckoutFormModel> snapshot) async {
    LocationResult result = await Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => PlacePicker()));
    if(result != null) {
      setState(() {
        _billingAddress1Controller.text = result.formattedAddress;
        _billingCityController.text = result.city;
        _billingPostCodeController.text = result.pinCode;
      });
      if(snapshot.data.countries.indexWhere((country) => country.value == result.country) != -1) {
        setState(() {
          snapshot.data.billingCountry = result.country;
        });
        regions = snapshot.data.countries.singleWhere((country) => country.value == result.country).regions;
      } else if(snapshot.data.countries.length != 0) {
        snapshot.data.billingCountry = snapshot.data.countries.first.value;
      } if(regions != null) {
        snapshot.data.billingState = regions.any((z) => z.value == result.state) ? result.state
            : regions.first.value;
      }
    }
  }
}

String _parseHtmlString(String htmlString) {
  var document = parse(htmlString);

  String parsedString = parse(document.body.text).documentElement.text;

  return parsedString;
}

