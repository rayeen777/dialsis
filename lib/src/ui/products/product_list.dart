import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../ui/accounts/language/app_localizations.dart';
import '../../blocs/home_bloc.dart';
import '../../models/category_model.dart';
import '../../ui/products/product_item.dart';
import '../checkout/cart/cart.dart';
import '../../models/product_model.dart';
import '../../blocs/products_bloc.dart';
import 'product_detail/product_detail.dart';
import 'filter_product.dart';

class ProductList extends StatefulWidget {
  final HomeBloc homeBloc;
  final ProductsBloc productsBloc = ProductsBloc();
  final Map<String, dynamic> filter;
  final String name;

  ProductList({Key key, this.filter, this.name, this.homeBloc})
      : super(key: key);

  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList>
    with SingleTickerProviderStateMixin {
  ScrollController _scrollController = new ScrollController();
  TabController _controller;
  Category selectedCategory;
  List<Category> subCategories;

  @override
  void initState() {
    super.initState();
    widget.productsBloc.filter = widget.filter;
    subCategories = widget.homeBloc.categories.where((cat) => cat.parent == widget.productsBloc.filter['id']).toList();
    _controller =
        TabController(vsync: this, length: subCategories.length);
    _controller.index = 0;
    if(subCategories.length != 0) {
      widget.productsBloc.filter['id'] = subCategories[_controller.index].id.toString();
    }
    widget.productsBloc.fetchAllProducts();
    widget.productsBloc.fetchProductsAttributes();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        widget.productsBloc.loadMore();
      }
    });
    _controller.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    widget.productsBloc.filter['id'] = subCategories[_controller.index].id.toString();
    widget.productsBloc.updateCategory();
    _scrollController.jumpTo(0.0);
    setState(() {
      selectedCategory = subCategories[_controller.index];
    });
  }

  _onSelectSubcategory(int id) {
    widget.productsBloc.filter['id'] = id.toString();
    widget.productsBloc.updateCategory();
    _scrollController.jumpTo(0.0);
    setState(() {
      selectedCategory = subCategories[_controller.index];
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        /*bottom: widget.subcategories.length != 0
            ? TabBar(
                indicatorColor: Theme.of(context).accentColor,
                isScrollable: true,
                controller: _controller,
                tabs: widget.subcategories
                    .map<Widget>((Category category) => Tab(
                        text: category.name
                            .replaceAll(new RegExp(r'&amp;'), '&')))
                    .toList(),
              )
            : null,*/
        elevation: 1.0,
        title: widget.name != null ? Text(widget.name) :  Text(widget.name),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.tune,
              semanticLabel: 'filter',
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute<DismissDialogAction>(
                    builder: (BuildContext context) => FilterProduct(
                      productsBloc: widget.productsBloc,
                      homeBloc: widget.homeBloc,
                      categories: subCategories,
                        onSelectSubcategory: _onSelectSubcategory
                    ),
                    fullscreenDialog: true,
                  ));
            },
          ),
          Stack(
            alignment: AlignmentDirectional.center,
            children: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.shopping_basket,
                  semanticLabel: 'filter',
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute<DismissDialogAction>(
                        builder: (BuildContext context) =>
                            CartPage(homeBloc: widget.homeBloc),
                        fullscreenDialog: true,
                      ));
                },
              ),
              Positioned(
                // draw a red marble
                top: 2,
                right: 2.0,
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute<DismissDialogAction>(
                          builder: (BuildContext context) => CartPage(
                            homeBloc: widget.homeBloc,
                          ),
                          fullscreenDialog: true,
                        ));
                  },
                  child: StreamBuilder<int>(
                      stream: widget.homeBloc.cartCount,
                      builder: (context, snapshot) {
                        if (snapshot.hasData && snapshot.data != 0) {
                          return Card(
                              elevation: 0,
                              clipBehavior: Clip.antiAlias,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(4.0),
                              ),
                              //padding: EdgeInsets.all(2),
                              color: Colors.redAccent,
                              child: Container(
                                  padding: EdgeInsets.all(2),
                                  constraints: BoxConstraints(minWidth: 20.0),
                                  child: Center(
                                      child: Text(
                                    snapshot.data.toString(),
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w800,
                                        color: Colors.white,
                                        backgroundColor: Colors.redAccent),
                                  ))));
                        } else
                          return Container();
                      }),
                ),
              )
            ],
          ),
        ],
      ),
      body: StreamBuilder(
          stream: widget.productsBloc.allProducts,
          builder: (context, AsyncSnapshot<List<Product>> snapshot) {
            if (snapshot.hasData) {
              if(snapshot.data.length != 0) {
                return CustomScrollView(
                  controller: _scrollController,
                  slivers: buildLisOfBlocks(snapshot),
                );
              } else {
                return StreamBuilder<bool>(
                  stream: widget.productsBloc.isLoadingProducts,
                  builder: (context, snapshot) {
                    if(snapshot.hasData && snapshot.data == true) {
                     return Center(child: CircularProgressIndicator());
                    }
                    else return Center(child: Text(AppLocalizations.of(context).translate("no products")),);
                  }
                );
              }
            } else if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            } return Center(child: CircularProgressIndicator());
          }),
    );
  }

  List<Widget> buildLisOfBlocks(AsyncSnapshot<List<Product>> snapshot) {
    List<Widget> list = new List<Widget>();
    list.add(buildSubcategories());
    if (snapshot.data != null) {
      list.add(buildProductGridList(snapshot));
      list.add(SliverPadding(
          padding: EdgeInsets.all(0.0),
          sliver: SliverList(
              delegate: SliverChildListDelegate([
            Container(
                height: 60,
                child: StreamBuilder(
                    stream: widget.productsBloc.hasMoreItems,
                    builder: (context, AsyncSnapshot<bool> snapshot) {
                      return snapshot.hasData && snapshot.data == false
                          ? Center(child: Text('No more products!'))
                          : Center(child: CircularProgressIndicator());
                    }
                    //child: Center(child: CircularProgressIndicator())
                    ))
          ]))));
    }

    return list;
  }

  Widget buildProductGridList(AsyncSnapshot<List<Product>> snapshot) {
    return SliverPadding(
      padding: const EdgeInsets.all(4.0),
      sliver: SliverGrid(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
          childAspectRatio: 0.68,
          crossAxisCount: 2,
        ),
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return ProductItem(
                product: snapshot.data[index], onProductClick: onProductClick);
          },
          childCount: snapshot.data.length,
        ),
      ),
    );
  }

  Widget buildSubcategories() {
    return subCategories.length != 0 ? SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 0.0),
        height: 140,
        width: 120,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: subCategories.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                padding: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                height: 100,
                width: 100,
                child: Column(
                  children: <Widget>[
                    Card(
                      shape: StadiumBorder(),
                      margin: EdgeInsets.all(5.0),
                      clipBehavior: Clip.antiAlias,
                      elevation: 0.5,
                      child: InkWell(
                        onTap: () {
                          var filter = new Map<String, dynamic>();
                          filter['id'] = subCategories[index].id.toString();
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ProductList(
                                      filter: filter, homeBloc: widget.homeBloc, name: subCategories[index].name)));
                        },
                        child: Column(
                          children: <Widget>[
                            AspectRatio(
                              aspectRatio: 18 / 18,
                              child: subCategories[index].image != null ? Image.network(
                                subCategories[index].image,
                                fit: BoxFit.cover,
                              ) : Container(),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 10.0),
                    InkWell(
                      onTap: () {
                        var filter = new Map<String, dynamic>();
                        filter['id'] = subCategories[index].id.toString();
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProductList(
                                    filter: filter, homeBloc: widget.homeBloc, name: subCategories[index].name)));
                      },
                      child: Text(
                        subCategories[index].name,
                        maxLines: 1,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 12),
                      ),
                    ),
                  ],
                ),
              );
            }),
      ),
    ) : SliverToBoxAdapter();
  }

  onProductClick(data) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ProductDetail(product: data, homeBloc: widget.homeBloc);
    }));
  }
}
