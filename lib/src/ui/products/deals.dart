import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../ui/accounts/language/app_localizations.dart';
import '../../blocs/home_bloc.dart';
import '../../ui/products/product_item.dart';
import '../../models/product_model.dart';
import 'product_detail/product_detail.dart';
import 'package:flutter/rendering.dart';

class Deals extends StatefulWidget {
  final HomeBloc homeBloc;

  Deals({Key key, this.homeBloc})
      : super(key: key);

  @override
  _DealsState createState() => _DealsState();
}

class _DealsState extends State<Deals>
    with SingleTickerProviderStateMixin {
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        widget.homeBloc.loadMore();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        title: Text(AppLocalizations.of(context).translate("deals")),
      ),
      body: StreamBuilder(
          stream: widget.homeBloc.allDealsProducts,
          builder: (context, AsyncSnapshot<List<Product>> snapshot) {
            if (snapshot.hasData) {
              if(snapshot.data.length != 0) {
                return CustomScrollView(
                  controller: _scrollController,
                  slivers: buildLisOfBlocks(snapshot),
                );
              } else {
                return Center(child: Text(AppLocalizations.of(context).translate("no_deals")),);
              }
            } else if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            } return Center(child: CircularProgressIndicator());
          }),
    );
  }

  List<Widget> buildLisOfBlocks(AsyncSnapshot<List<Product>> snapshot) {
    List<Widget> list = new List<Widget>();
    if (snapshot.data != null) {
      list.add(buildProductGridList(snapshot));
      list.add(SliverPadding(
          padding: EdgeInsets.all(0.0),
          sliver: SliverList(
              delegate: SliverChildListDelegate([
            Container(
                height: 60,
                child: StreamBuilder(
                    stream: widget.homeBloc.hasMoreDealsItems,
                    builder: (context, AsyncSnapshot<bool> snapshot) {
                      return snapshot.hasData && snapshot.data == false
                          ? Center(child: Text('No more products!'))
                          : Center(child: CircularProgressIndicator());
                    }
                    //child: Center(child: CircularProgressIndicator())
                    ))
          ]))));
    }

    return list;
  }

  Widget buildProductGridList(AsyncSnapshot<List<Product>> snapshot) {
    return SliverPadding(
      padding: const EdgeInsets.all(4.0),
      sliver: SliverGrid(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
          childAspectRatio: 0.68,
          crossAxisCount: 2,
        ),
        delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
            return ProductItem(
                product: snapshot.data[index], onProductClick: onProductClick);
          },
          childCount: snapshot.data.length,
        ),
      ),
    );
  }

  onProductClick(data) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ProductDetail(product: data, homeBloc: widget.homeBloc);
    }));
  }
}
