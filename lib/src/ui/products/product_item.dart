import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../../models/product_model.dart';
import 'package:html/parser.dart';

class ProductItem extends StatelessWidget {

  final Product product;
  final void Function(Product category) onProductClick;

  const ProductItem({
    Key key,
    this.product,
    this.onProductClick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    if(product.regularPrice == null || product.regularPrice.isNaN) {
      product.regularPrice = product.price;
    }
    return Card(
      elevation: 0.0,
      clipBehavior: Clip.antiAlias,
      child: Container(
        child: Column(
          children: <Widget>[
            AspectRatio(
              aspectRatio: 1,
              child: Container(
                child: CachedNetworkImage(
                  imageUrl: product.images[0].src,
                  imageBuilder: (context, imageProvider) => Ink.image(
                    child: InkWell(
                      onTap: () {
                        onProductClick(product);
                      },
                    ),
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                  placeholder: (context, url) => Container(
                    color: Colors.white12,
                  ),
                  errorWidget: (context, url, error) => Container(color: Colors.black12),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(4.0, 10, 4, 4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    product.name,
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.body1,
                  ),
                  SizedBox(height: 4.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    textBaseline: TextBaseline.alphabetic,
                    children: <Widget>[
                      Text(
                          (product.salePrice !=
                              null && product.salePrice != 0)
                              ? _parseHtmlString(product.formattedSalesPrice)
                              : '',style: TextStyle(fontWeight: FontWeight.w600, fontSize:16,)),
                      SizedBox(width: 4.0),
                      Text((product.formattedPrice !=
                          null && product.formattedPrice.isNotEmpty)
                          ? _parseHtmlString(product.formattedPrice)
                          : '', style: Theme.of(context).textTheme.body1.copyWith(
                        decoration: TextDecoration.lineThrough,
                        fontSize: 12,
                        fontWeight: FontWeight.w200,
                      )),
                    ],
                  ),
                  SizedBox(height: 8.0),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

String _parseHtmlString(String htmlString) {
  var document = parse(htmlString);

  String parsedString = parse(document.body.text).documentElement.text;

  return parsedString;
}
