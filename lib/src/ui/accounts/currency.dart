import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../blocs/home_bloc.dart';
import '../../models/blocks_model.dart';
import 'language/app_localizations.dart';

class CurrencyPage extends StatefulWidget {
  final HomeBloc homeBloc;
  CurrencyPage({
    Key key,
    this.homeBloc,
  }) : super(key: key);
  @override
  _CurrencyPageState createState() => _CurrencyPageState();
}

class _CurrencyPageState extends State<CurrencyPage> {

  String _selectedCurrency;

  @override
  void initState() {
    super.initState();
    _selectedCurrency = widget.homeBloc.selectedCurrency;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
          title: Text(AppLocalizations.of(context).translate("currency"))
      ),
      body: StreamBuilder(
          stream: widget.homeBloc.allBlocks,
          builder: (context, AsyncSnapshot<BlocksModel> snapshot) {
            if (snapshot.hasData && snapshot.data.currencies != null) {
              //_selectedCurrency = snapshot.data.currency;
              return buildCurrencyItems(snapshot.data.currencies);
            } else
              return Container();
          }));
  }

  Widget buildCurrencyItems(List<Currency> currency) {
    return ListView.builder
      (
        itemCount: currency.length,
        itemBuilder: (BuildContext ctxt, int index) {
          return StreamBuilder<BlocksModel>(
              stream: widget.homeBloc.allBlocks,
            builder: (context, snapshot) {
              return snapshot.hasData ? Column(
                children: <Widget>[
                  ListTile(
                    trailing: Radio<String>(
                      value: currency[index].code,
                      groupValue: _selectedCurrency,
                      onChanged: (value) async {
                        setState(() {
                          _selectedCurrency = currency[index].code;
                        });
                        widget.homeBloc.selectedCurrency = currency[index].code;
                        await widget.homeBloc.switchCurrency(currency[index].code);
                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        await prefs.setString('currency', currency[index].code);
                        widget.homeBloc.fetchAllBlocks();
                      },
                    ),
                    title: Text(currency[index].code),
                    onTap: () async {
                      setState(() {
                        _selectedCurrency = currency[index].code;
                      });
                      widget.homeBloc.selectedCurrency = currency[index].code;
                      await widget.homeBloc.switchCurrency(currency[index].code);
                      SharedPreferences prefs = await SharedPreferences.getInstance();
                      await prefs.setString('currency', currency[index].code);
                      widget.homeBloc.fetchAllBlocks();
                    },
                  ),
                  Divider(height: 0,)
                ],
              ) : Container();
            }
          );
        }
    );
  }
}


