import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './../../blocs/home_bloc.dart';
import './../../ui/themes/gallary_theme.dart';
import './../../ui/themes/themes.dart';

import '../options.dart';
import 'language/app_localizations.dart';

class DemoThemes extends StatefulWidget {
  final HomeBloc homeBloc;

  final GalleryOptions options;
  final ValueChanged<GalleryOptions> handleOptionsChanged;

  DemoThemes(
      {Key key, this.homeBloc, this.handleOptionsChanged, this.options})
      : super(key: key);
  @override
  _DemoThemesState createState() => _DemoThemesState();
}

class _DemoThemesState extends State<DemoThemes> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        title: Text(AppLocalizations.of(context).translate("Themes")),
      ),
      body: ListView.builder(
          itemCount: 18,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: <Widget>[
                ListTile(
                  onTap: () {
                    widget.handleOptionsChanged(
                      widget.options.copyWith(
                        theme: themes[index],
                      ),
                    );
                  },
                  leading: ExcludeSemantics(
                    child: RotatedBox(
                      quarterTurns: 1,
                      child: Icon(IconData(0xf37b,
                          fontFamily: CupertinoIcons.iconFont,
                          fontPackage: CupertinoIcons.iconFontPackage)),
                    ),
                  ),
                  title: Text(AppLocalizations.of(context).translate("Themes") + index.toString()),
                ),
                Divider(height: 0.0,)
              ],
            );
          })
    );
  }

  List<GalleryTheme> themes = [
    kLightGalleryTheme,
    kLightGalleryTheme1,
    kLightGalleryTheme2,
    kLightGalleryTheme3,
    kLightGalleryTheme4,
    kLightGalleryTheme5,
    kLightGalleryTheme6,
    kLightGalleryTheme7,
    kLightGalleryTheme8,
    kLightGalleryTheme9,
    kLightGalleryTheme10,
    kLightGalleryTheme11,
    kLightGalleryTheme12,
    kLightGalleryTheme13,
    kLightGalleryTheme14,
    kLightGalleryTheme15,
    kLightGalleryTheme16,
    kLightGalleryTheme17,
  ];
}
