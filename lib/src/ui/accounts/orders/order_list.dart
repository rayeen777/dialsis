import 'package:flutter/material.dart';
import '../../../ui/accounts/language/app_localizations.dart';
import './../../../blocs/home_bloc.dart';
import './../../../models/orders_model.dart';
import 'package:intl/intl.dart';

import 'order_detail.dart';

class OrderList extends StatefulWidget {
  final HomeBloc homeBloc;
  OrderList({Key key, this.homeBloc}) : super(key: key);
  @override
  _OrderListState createState() => _OrderListState();
}

class _OrderListState extends State<OrderList> {
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    widget.homeBloc.getOrders();

    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
              _scrollController.position.maxScrollExtent &&
          widget.homeBloc.hasMoreOrders) {
        widget.homeBloc.loadMoreOrders();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        title: Text(AppLocalizations.of(context).translate("order_list")),
      ),
      body: StreamBuilder(
          stream: widget.homeBloc.allOrders,
          builder: (context, AsyncSnapshot<List<Order>> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.length == 0) {
                return Center(child: Text(AppLocalizations.of(context).translate("There_are_no_orders")));
              } else {
                return CustomScrollView(
                  controller: _scrollController,
                  slivers: <Widget>[
                    buildList(snapshot),
                    buildLoadMore(),
                  ],
                );
              }
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }),
    );
  }
  buildList(AsyncSnapshot<List<Order>> snapshot) {
    var formatter1 = new DateFormat('yyyy-MM-dd  hh:mm a');
    return SliverPadding(
      padding: EdgeInsets.all(8.0),
      sliver: SliverList(
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            final NumberFormat formatter = NumberFormat.currency(
                decimalDigits: snapshot.data[index].decimals, locale: Localizations.localeOf(context).toString(), name: snapshot.data[index].currency);
            return Card(
                elevation: 1.0,
                child: InkWell(
                  borderRadius: BorderRadius.circular(4.0),
                  onTap: () => openDetailPage(snapshot.data, index),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child:
                        ListTile(
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(AppLocalizations.of(context).translate("id")+ '-' + snapshot.data[index].id.toString(), style: Theme.of(context).textTheme.subtitle,),
                                Text(formatter.format(
                                    double.parse(snapshot.data[index].total,)), style: Theme.of(context).textTheme.subtitle,),
                              ],
                            ),
                            subtitle: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(height: 5,),
                                Text(snapshot.data[index].status.toUpperCase()),
                                SizedBox(height: 5,),
                                Text(formatter1
                                    .format(snapshot.data[index].dateCreated)),
                              ],
                            )),
                  ),
                ));
          },
          childCount: snapshot.data.length,
        ),
      ),
    );
  }

  openDetailPage(List<Order> data, int index) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return OrderDetail(order: data[index]);
    }));
  }

  buildLoadMore() {
    return SliverPadding(
        padding: EdgeInsets.all(0.0),
        sliver: SliverList(
            delegate: SliverChildListDelegate([
          Container(
              height: 60,
              child: StreamBuilder(
                  stream: widget.homeBloc.hasMoreOrderItems,
                  builder: (context, AsyncSnapshot<bool> snapshot) {
                    return snapshot.hasData && snapshot.data != false
                        ? Center(child: Text(AppLocalizations.of(context).translate("no_more_orders")+'!'))
                        : Center(child: CircularProgressIndicator());
                  }
                  //child: Center(child: CircularProgressIndicator())
                  ))
        ])));
  }
}
