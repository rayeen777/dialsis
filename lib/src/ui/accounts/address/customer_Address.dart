import 'package:flutter/material.dart';
import '../../../ui/accounts/language/app_localizations.dart';
import './../../../blocs/home_bloc.dart';
import './../../../models/customer_model.dart';
import './../../../ui/accounts/address/edit_address.dart';


class CustomerAddress extends StatefulWidget {
  HomeBloc homeBloc;
  CustomerAddress({Key key, this.homeBloc}) : super(key: key);
  @override
  _CustomerAddressState createState() => _CustomerAddressState();
}

class _CustomerAddressState extends State<CustomerAddress> {
  @override
  void initState() {
    super.initState();
    widget.homeBloc.getCustomerDetails();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        title: Text(AppLocalizations.of(context).translate("address")),
      ),
      body: StreamBuilder(
          stream: widget.homeBloc.customerDetail,
          builder: (context, AsyncSnapshot<Customer> snapshot) {
            if (snapshot.hasData) {
              return buildList(snapshot);
            } else if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            }
            return Center(child: CircularProgressIndicator());
          }),
    );
  }

  Widget buildList(AsyncSnapshot<Customer> snapshot) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 1.0,
        child: InkWell(
          borderRadius: BorderRadius.circular(4.0),
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => EditAddress(homeBloc: widget.homeBloc, customer: snapshot)));
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListTile(
              title: Text(AppLocalizations.of(context).translate("your address"), style: Theme.of(context).textTheme.subtitle),
              subtitle: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                    '''${snapshot.data.billing.firstName} ${snapshot.data.billing.lastName} ${snapshot.data.billing.address1} ${snapshot.data.billing.address2} ${snapshot.data.billing.city} ${snapshot.data.billing.state} ${snapshot.data.billing.postcode} ${snapshot.data.billing.country} ${snapshot.data.billing.email} ${snapshot.data.billing.phone}
                        '''),
              ),
            ),
          ),
        ),
      ),
    );
  }


}
