import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import '../../../ui/accounts/language/app_localizations.dart';
import '../../../models/checkout/checkout_form_model.dart';
import '../../../ui/checkout/place_picker.dart';
import '../../../blocs/home_bloc.dart';
import './../../../models/customer_model.dart';
import '../../color_override.dart';
import 'package:html/parser.dart';

class EditAddress extends StatefulWidget {
  HomeBloc homeBloc;
  AsyncSnapshot<Customer> customer;

  EditAddress({Key key, this.homeBloc, this.customer}) : super(key: key);
  @override
  _EditAddressState createState() => _EditAddressState();
}

class _EditAddressState extends State<EditAddress> {
  final _formKey = GlobalKey<FormState>();

  List<Region> regions;
  TextEditingController _address1Controller = TextEditingController();
  TextEditingController _cityController = TextEditingController();
  TextEditingController _postCodeController = TextEditingController();

  @override
  void initState() {
    super.initState();
    widget.homeBloc.getCheckoutForm();
    _address1Controller.text = widget.customer.data.billing.address1;
    _cityController.text = widget.customer.data.billing.city;
    _postCodeController.text = widget.customer.data.billing.postcode;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 1.0,
          title: Text(AppLocalizations.of(context).translate("edit_address")),
        ),
        body: StreamBuilder<CheckoutFormModel>(
            stream: widget.homeBloc.checkoutForm,
            builder: (context, snapshot) {
              return snapshot.hasData
                  ? buildListView(context, snapshot)
                  : Center(child: CircularProgressIndicator());
            }),
    );
  }

  ListView buildListView(BuildContext context, AsyncSnapshot<CheckoutFormModel> snapshot) {

    if(snapshot.data.countries.indexWhere((country) => country.value == widget.customer.data.billing.country) != -1) {
      regions = snapshot.data.countries.singleWhere((country) => country.value == widget.customer.data.billing.country).regions;
    } else if(snapshot.data.countries.indexWhere((country) => country.value == widget.customer.data.billing.country) == -1) {
      widget.customer.data.billing.country = snapshot.data.countries.first.value;
    } if(regions != null) {
      widget.customer.data.billing.state = regions.any((z) => z.value == widget.customer.data.billing.state) ? widget.customer.data.billing.state
          : regions.first.value;
    }

    if(_address1Controller.text.isEmpty)
      _address1Controller.text = snapshot.data.billingAddress1;
    if(_cityController.text.isEmpty)
      _cityController.text = snapshot.data.billingCity;
    if(_postCodeController.text.isEmpty)
      _postCodeController.text = snapshot.data.billingPostcode;

    return ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(16),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    colorBrightness: Brightness.light,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children:<Widget>[
                        Icon(Icons.add_location),
                        Text(AppLocalizations.of(context).translate("pick_delivery_location"))
                      ], ),
                    onPressed: () {
                      showPlacePicker(snapshot);
                    },
                  ),
                  PrimaryColorOverride(
                    child: TextFormField(
                      initialValue: widget.customer.data.firstName,
                      decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("firstname")),
                      validator: (value) {
                        if (value.isEmpty) {
                          return AppLocalizations.of(context).translate("please_enter_first_name");
                        }
                        return null;
                      },
                      onSaved: (value) {
                        widget.customer.data.firstName =
                            value;
                      },
                    ),
                  ),
                  SizedBox(height: 12.0),
                  PrimaryColorOverride(
                    child: TextFormField(
                      initialValue: widget.customer.data.lastName,
                      decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("lastname")),
                      validator: (value) {
                        if (value.isEmpty) {
                          return AppLocalizations.of(context).translate("please_enter_last_name");
                        }
                        return null;
                      },
                      onSaved: (value) {
                        widget.customer.data.lastName =
                            value;
                      },
                    ),
                  ),
                  SizedBox(height: 12.0),
                  PrimaryColorOverride(
                    child: TextFormField(
                      controller: _address1Controller,
                      decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("address")),
                      validator: (value) {
                        if (value.isEmpty) {
                          return AppLocalizations.of(context).translate("please_enter_address");
                        }
                        return null;
                      },
                      onSaved: (value) {
                        widget.customer.data.billing.address1 =
                            value;
                      },
                    ),
                  ),
                  /*SizedBox(height: 12.0),
                  PrimaryColorOverride(
                    child: TextFormField(
                      initialValue: widget.customer.data.billing.address2,
                      decoration: InputDecoration(labelText: 'Address2'),
                      onSaved: (value) {
                        widget.customer.data.billing.address2 =
                            value;
                      },
                    ),
                  ),*/
                  SizedBox(height: 12.0),
                  PrimaryColorOverride(
                    child: TextFormField(
                      controller: _cityController,
                      decoration: InputDecoration(
                        labelText: AppLocalizations.of(context).translate("city"),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return AppLocalizations.of(context).translate("please_enter_city");
                        }
                        return null;
                      },
                      onSaved: (value) {
                        widget.customer.data.billing..city = value;
                      },
                    ),
                  ),
                  SizedBox(height: 12.0),
                  PrimaryColorOverride(
                    child: TextFormField(
                      controller: _postCodeController,
                      decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("postcode")),
                      onSaved: (value) {
                        widget.customer.data.billing.postcode =
                            value;
                      },
                    ),
                  ),
                  SizedBox(height: 12.0),
                  PrimaryColorOverride(
                    child: TextFormField(
                      initialValue: widget.customer.data.billing.email,
                      decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("email")),
                      onSaved: (value) {
                        widget.customer.data.billing.email = value;
                      },
                    ),
                  ),
                  SizedBox(height: 12.0),
                  PrimaryColorOverride(
                    child: TextFormField(
                      initialValue: widget.customer.data.billing.phone,
                      decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("phonenumber")),
                      validator: (value) {
                        if (value.isEmpty) {
                          return AppLocalizations.of(context).translate("please_enter_phone_number");
                        }
                        return null;
                      },
                      onSaved: (value) {
                        widget.customer.data.billing.phone = value;
                      },
                    ),
                  ),
                  SizedBox(height: 20,),
                  DropdownButton<String>(
                    value: widget.customer.data.billing.country,
                    hint: Text(AppLocalizations.of(context).translate("country")),
                    isExpanded: true,
                    icon: Icon(Icons.arrow_drop_down),
                    iconSize: 24,
                    elevation: 16,
                    underline: Container(
                      height: 2,
                      color: Theme.of(context).dividerColor,
                    ),
                    onChanged: (String newValue) {
                      setState(() {
                        widget.customer.data.billing.country =
                            newValue;
                      });
                    },
                    items: snapshot.data.countries
                        .map<DropdownMenuItem<String>>(
                            (value) {
                          return DropdownMenuItem<String>(
                            value: value.value != null ? value.value : '',
                            child: Text(_parseHtmlString(value.label)),
                          );
                        }).toList(),
                  ),
                  regions != null ? Column(
                    children: <Widget>[
                      SizedBox(height: 20,),
                      DropdownButton<String>(
                        value: widget.customer.data.billing.state,
                        hint: Text(AppLocalizations.of(context).translate("state")),
                        isExpanded: true,
                        icon: Icon(Icons.arrow_drop_down),
                        iconSize: 24,
                        elevation: 16,
                        underline: Container(
                          height: 2,
                          color: Theme.of(context).dividerColor,
                        ),
                        onChanged: (String newValue) {
                          setState(() {
                            widget.customer.data.billing.state =
                                newValue;
                          });
                        },
                        items: regions
                            .map<DropdownMenuItem<String>>(
                                (value) {
                              return DropdownMenuItem<String>(
                                value: value.value != null ? value.value : '',
                                child: Text(_parseHtmlString(value.label)),
                              );
                            }).toList(),
                      ),
                    ],
                  ) : PrimaryColorOverride(
                    child: TextFormField(
                      initialValue:
                      widget.homeBloc.formData['billing_state'],
                      decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("state")),
                      validator: (value) {
                        if (value.isEmpty) {
                          return AppLocalizations.of(context).translate("please_enter_state");
                        }
                      },
                      onSaved: (val) => setState(() =>
                      widget.homeBloc.formData['billing_state'] = val),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                RaisedButton(
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(2.0)),
                  ),
                  padding: EdgeInsets.symmetric(vertical: 12.0),
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();
                      widget.homeBloc.addressFormData['billing_first_name'] = widget.customer.data.billing.firstName;
                      widget.homeBloc.addressFormData['billing_last_name'] = widget.customer.data.billing.lastName;
                      widget.homeBloc.addressFormData['billing_address_1'] = widget.customer.data.billing.address1;
                      //widget.homeBloc.addressFormData['billing_address_2'] = widget.customer.data.billing.address2;
                      widget.homeBloc.addressFormData['billing_city'] = widget.customer.data.billing.city;
                      widget.homeBloc.addressFormData['billing_postcode'] = widget.customer.data.billing.postcode;
                      widget.homeBloc.addressFormData['billing_email'] = widget.customer.data.billing.email;
                      widget.homeBloc.addressFormData['billing_phone'] = widget.customer.data.billing.phone;
                      widget.homeBloc.addressFormData['billing_country'] = widget.customer.data.billing.country != null ? widget.customer.data.billing.country : '';
                      widget.homeBloc.addressFormData['billing_state'] = widget.customer.data.billing.state != null ? widget.customer.data.billing.state : '';
                      widget.homeBloc.updateAddress();
                      print(widget.homeBloc.addressFormData);
                      Navigator.pop(context);
                    }
                  },
                  child: Text(
                    AppLocalizations.of(context).translate("continue"),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ],
      );
  }

  void onCountryChange(CountryCode countryCode) {
    widget.homeBloc.addressFormData['billing_country'] = countryCode.code;
  }

  void showPlacePicker(AsyncSnapshot<CheckoutFormModel> snapshot) async {
    LocationResult result = await Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => PlacePicker()));
    if(result != null) {
      setState(() {
        _address1Controller.text = result.formattedAddress;
        _cityController.text = result.city;
        _postCodeController.text = result.pinCode;
      });
      if (snapshot.data.countries.indexWhere((country) =>
      country.value == result.country) != -1) {
        setState(() {
          snapshot.data.billingCountry = result.country;
        });
        regions = snapshot.data.countries
            .singleWhere((country) => country.value == result.country)
            .regions;
      } else if (snapshot.data.countries.length != 0) {
        snapshot.data.billingCountry = snapshot.data.countries.first.value;
      }
      if (regions != null) {
        snapshot.data.billingState =
        regions.any((z) => z.value == result.state) ? result.state
            : regions.first.value;
      }
    }
  }
}

String _parseHtmlString(String htmlString) {
  var document = parse(htmlString);

  String parsedString = parse(document.body.text).documentElement.text;

  return parsedString;
}

