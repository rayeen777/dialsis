import 'package:flutter/material.dart';
import '../../blocs/home_bloc.dart';
import '../../models/blocks_model.dart';
import 'language/app_localizations.dart';

class Pages extends StatefulWidget {
  final HomeBloc homeBloc;

  const Pages({Key key, this.homeBloc}) : super(key: key);
  @override
  _PagesState createState() => _PagesState();
}

class _PagesState extends State<Pages> {
  ScrollController _scrollController = new ScrollController();

  @override
  void initState(){
    super.initState();
    widget.homeBloc.fetchAllBlocks();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate("pages")),
      ),
      body: StreamBuilder(
        stream: widget.homeBloc.allBlocks,
        builder: (context, AsyncSnapshot<BlocksModel> snapshot){
          if(snapshot.hasData){
            if(snapshot.data.pages == 0){
              return Center(child: Text(AppLocalizations.of(context).translate("there_are_no_pages")));
            } else{
              return CustomScrollView(
                controller: _scrollController,
                slivers: <Widget>[
                  buildList(snapshot),
                ],

              );
            }
          }else {
            return Center(child: CircularProgressIndicator());
          }
        },
      )
    );
  }

  buildList(AsyncSnapshot<BlocksModel> snapshot) {
  return SliverPadding(
    padding: EdgeInsets.all(0.0),
    sliver: SliverList(
      delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index){
            return Card(
                elevation: 1.0,
                child: InkWell(
                  borderRadius: BorderRadius.circular(4.0),
                  onTap: () => {},
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child:
                    ListTile(
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(snapshot.data.pages[index].title, style: Theme.of(context).textTheme.subtitle,),
                          ],
                        ),
                    ),
                  ),
                ));

          },
         childCount: snapshot.data.pages.length,
      ),
    ),
  );
  }
}
