import 'package:flutter/material.dart';
import '../../../src/resources/api_provider.dart';
import '../../../src/blocs/home_bloc.dart';
import '../color_override.dart';
import 'language/app_localizations.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class TryDemo extends StatefulWidget {
  final HomeBloc homeBloc;
  TryDemo({Key key, this.homeBloc}) : super(key: key);

  @override
  _TryDemoState createState() => _TryDemoState();
}

class _TryDemoState extends State<TryDemo> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController urlController = new TextEditingController();
  final apiProvider = ApiProvider();
  bool isLoading = false;
  String error = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        title: Text(AppLocalizations.of(context).translate("demo")),
      ),
      body: Center(
        child: Container(
          margin: const EdgeInsets.only(left: 16.0, right: 16.0),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                PrimaryColorOverride(
                  child: TextFormField(
                    controller: urlController,
                    decoration: InputDecoration(
                      hintText: 'http://example.com',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return AppLocalizations.of(context).translate("please_enter_site_url");
                      }
                      return null;
                    },
                  ),
                ),
                SizedBox(height: 22.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    new RaisedButton(
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(2.0)),
                      ),
                      padding: EdgeInsets.all(12.0),
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          setState(() {
                            isLoading = true;
                            error = '';
                          });
                          final response = await http.get(
                            Uri.parse(urlController.text +
                                '/wp-admin/admin-ajax.php?action=mstore_flutter-keys'),
                          );
                          if (response.statusCode == 200) {
                            apiProvider.url = stripSlash(urlController.text);
                            print(apiProvider.url);
                            await widget.homeBloc.fetchAllBlocks();
                            await widget.homeBloc.getCustomerDetails();
                            await widget.homeBloc.fetchAllProducts();
                            Navigator.of(context).pop();
                          } else if (response.statusCode == 400) {
                            setState(() {
                              error = 'Please install plugin and activate';
                            });
                          } else {
                            setState(() {
                              error = 'Please check url';
                            });
                          }
                          setState(() {
                            isLoading = false;
                          });
                        }
                      },
                      child: Text(
                          AppLocalizations.of(context).translate("Continue")),
                    ),
                  ],
                ),
                Container(
                  height: 70,
                  child: Center(
                      child: isLoading ? CircularProgressIndicator()
                       : Text(error,
                          style:
                              TextStyle(color: Theme.of(context).errorColor))),
                ),
                Text('Note - Before contine please read document clearly, install plugin and save settings',
                    style:
                    TextStyle(color: Theme.of(context).accentColor)),
                SizedBox(height: 6.0),
                InkWell(
                    child: new Text('https://mstoreapp.com/documents/flutter/woocommerce/woocommerce/wordpress_plugin_installtion.html', style: TextStyle(
                      color: Colors.blue
                    ),),
                    onTap: () => launch('https://mstoreapp.com/documents/flutter/woocommerce/woocommerce/wordpress_plugin_installtion.html')
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

   String stripSlash(String str) {
    if (str != null && str.length > 0 && str.substring(str.length - 1) == '/') {
      str = str.substring(0, str.length - 1);
    }
    return str;
  }
}
