import 'package:flutter/material.dart';
import '../../ui/accounts/language/app_localizations.dart';
import '../../blocs/home_bloc.dart';
import '../../models/customer_model.dart';
import '../../models/register_model.dart';
import '../color_override.dart';

class RegisterForm extends StatefulWidget {

  final HomeBloc homeBloc;
  RegisterForm({Key key, this.homeBloc}) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _formKey = GlobalKey<FormState>();

  var _register = Register();

  TextEditingController firstNameController = new TextEditingController();

  TextEditingController lastNameController = new TextEditingController();

  TextEditingController phoneNumberController = new TextEditingController();

  TextEditingController emailController = new TextEditingController();

  TextEditingController passwordController = new TextEditingController();
  bool passwordVisible = true;
  @override
  void initState() {
    var passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
        appBar: AppBar(
            title: Text(AppLocalizations.of(context).translate("register"),
                style: Theme.of(context).primaryTextTheme.title),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context),
            )),
        body: SingleChildScrollView(
          child: new Container(
            height: MediaQuery.of(context).size.height,
            margin: const EdgeInsets.only(left: 16.0, right: 16.0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  PrimaryColorOverride(
                    child: TextFormField(
                      controller: firstNameController,
                      decoration: InputDecoration(
                        labelText:  AppLocalizations.of(context).translate("firstname"),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return AppLocalizations.of(context).translate("please_enter_first_name");
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: 12.0),
                  PrimaryColorOverride(
                    child: TextFormField(
                      controller: lastNameController,
                      decoration: InputDecoration(
                        labelText:  AppLocalizations.of(context).translate("lastname"),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return AppLocalizations.of(context).translate("please_enter_last_name");
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: 12.0),
                  PrimaryColorOverride(
                    child: TextFormField(
                      controller: phoneNumberController,
                      decoration: InputDecoration(
                        labelText:  AppLocalizations.of(context).translate("phonenumber"),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return AppLocalizations.of(context).translate("please_enter_phone_number");
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: 12.0),
                  PrimaryColorOverride(
                    child: TextFormField(
                      controller: emailController,
                      decoration: InputDecoration(
                        labelText:  AppLocalizations.of(context).translate("email/username"),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return AppLocalizations.of(context).translate("please_enter_a_valid_email");
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: 12.0),
                  PrimaryColorOverride(
                    child: TextFormField(
                      controller: passwordController,
                      obscureText: passwordVisible,
                      decoration: InputDecoration(
                        labelText: AppLocalizations.of(context).translate("password"),
                        suffixIcon: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              passwordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Theme.of(context).primaryColorDark,
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(
                                      () {
                                    passwordVisible = !passwordVisible;
                                  });
                            }),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return AppLocalizations.of(context).translate("please_enter_password");
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: 22.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      new RaisedButton(
                        //elevation: 8.0,
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(2.0)),
                        ),
                        padding: EdgeInsets.all(12.0),
                        onPressed: () async {
                          //var create = new Map<String, dynamic>();
                          if (_formKey.currentState.validate()) {
                            _register.firstName = firstNameController.text;
                            _register.lastName = lastNameController.text;
                            _register.phoneNumber = phoneNumberController.text;
                            _register.email = emailController.text;
                            _register.password = passwordController.text;
                            Customer user = await widget.homeBloc.register(_register.toJson());
                            Navigator.of(context).pop();
                          }
                        },
                        child:  Text(AppLocalizations.of(context).translate("register")),
                      ),
                    ],
                  ),
                  StreamBuilder(
                    stream: widget.homeBloc.isLoginLoading,
                    builder: (context, snapshot2) {
                      return StreamBuilder(
                        stream: widget.homeBloc.registerError,
                        builder: (context, snapshot1) {
                          if (snapshot1.hasData && snapshot2.data == 'false') {
                            if(snapshot1.data.data[0].code == 'existing_user_login')
                              return Text('Sorry, that username already exists!', style: TextStyle(color: Theme.of(context).errorColor));
                            else if (snapshot1.data.data[0].code == 'incorrect_password');
                            return Text('Incorrect password', style: TextStyle(color: Theme.of(context).errorColor));
                          } else if (snapshot1.hasError) {
                            return Text(snapshot1.error.toString());
                          }
                          if (snapshot2.hasData && snapshot2.data == 'true')
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Center(child: CircularProgressIndicator()),
                            );
                          return Center(child: null);
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
