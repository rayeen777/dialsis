import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import './../../../models/app_state_model.dart';
import './../../../ui/accounts/language/app_localizations.dart';
import './../../../blocs/home_bloc.dart';
import './../../../models/blocks_model.dart';
import './../../../resources/api_provider.dart';
import '../../options.dart';

class LanguagePage extends StatefulWidget {
  final HomeBloc homeBloc;

  final GalleryOptions options;
  final ValueChanged<GalleryOptions> handleOptionsChanged;

  LanguagePage(
      {Key key, this.homeBloc, this.handleOptionsChanged, this.options})
      : super(key: key);
  @override
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {

  final apiProvider = ApiProvider();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 1.0,
          title: Text(
            AppLocalizations.of(context).translate("title_select_language"),
          ),
        ),
        body: StreamBuilder(
            stream: widget.homeBloc.allBlocks,
            builder: (context, AsyncSnapshot<BlocksModel> snapshot) {
              if (snapshot.hasData && snapshot.data.languages != null) {
                return buildLanguageItems(snapshot.data.languages);
              } else
                return Container();
            }));
  }

  Widget buildLanguageItems(List<Language> languages) {
    return ListView.builder(
        itemCount: languages.length,
        itemBuilder: (BuildContext ctxt, int index) {
          TextDirection textDirection = TextDirection.ltr;
          if (languages[index].code == 'ar') {
            textDirection = TextDirection.rtl;
          }
          return Column(
            children: <Widget>[
              new ListTile(
                trailing: Radio<String>(
                  value: languages[index].code,
                  groupValue: apiProvider.lan,
                  onChanged: (value) async {
                    apiProvider.lan = languages[index].code;
                    widget.homeBloc.fetchAllBlocks();
                    ScopedModel.of<AppStateModel>(context).changeLanguage(Locale(languages[index].code));
                  },
                ),
                title: Text(languages[index].nativeName),
                onTap: () async {
                  apiProvider.lan = languages[index].code;
                  widget.homeBloc.fetchAllBlocks();
                  ScopedModel.of<AppStateModel>(context).changeLanguage(Locale(languages[index].code));
                },
              ),
              Divider(
                height: 0,
              )
            ],
          );
        });
  }
}
