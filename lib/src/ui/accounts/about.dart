import 'package:flutter/material.dart';
import '../../ui/accounts/language/app_localizations.dart';

class AboutUs extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        title: Text(AppLocalizations.of(context).translate("about_us")),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                Text('''Who We Are?
              ''', style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w500), textAlign: TextAlign.justify),
                Text('''
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              ''', style: TextStyle(fontSize: 18.0,), textAlign: TextAlign.justify),
                Text('''Why do we use it?
              ''', style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w500), textAlign: TextAlign.justify),
                Text('''
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              ''', style: TextStyle(fontSize: 18.0), textAlign: TextAlign.justify),
                Text('''Where can I get some?
              ''', style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w500), textAlign: TextAlign.justify),
                Text('''
              There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
              ''', style: TextStyle(fontSize: 18.0), textAlign: TextAlign.justify),
              ],
            ),
          ),
        ],
    ));
  }
}
