import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:woocommerce/src/models/UserRepo.dart';
import '../../ui/accounts/language/app_localizations.dart';
import '../../blocs/home_bloc.dart';
import '../../models/customer_model.dart';
import '../color_override.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//**** UNCOMMENT THSI FOR GOOGLE AND FB SIGN IN *****//
//import 'package:google_sign_in/google_sign_in.dart';
//import 'package:flutter_facebook_login/flutter_facebook_login.dart';


//**** UNCOMMENT THSI FOR GOOGLE AND FB SIGN IN *****//
//final GoogleSignIn _googleSignIn = GoogleSignIn();

bool isLoggedIn = false;

class LoginForm extends StatefulWidget {
  final HomeBloc homeBloc;
  LoginForm({Key key, this.homeBloc}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController usernameControler = new TextEditingController();
  TextEditingController passwordControler = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();

  String _phoneNumber;

  String verificationId;

  String smsOTP;

  String errorMessage = '';
  bool passwordVisible = true;
  @override
  void initState() {
    var passwordVisible = false;
  }
  @override
  Widget build(BuildContext context) {


    return Scaffold(
        appBar: AppBar(

          backgroundColor: Theme.of(context).primaryColor,
          title: Text(AppLocalizations.of(context).translate('login')),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.only(left: 16.0, right: 16.0),
            height: MediaQuery.of(context).size.height,
            child: new Form(
              key: _formKey,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  PrimaryColorOverride(
                    child: TextFormField(
                      controller: usernameControler,
                      decoration: InputDecoration(
                        labelText: AppLocalizations.of(context).translate('username'),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter username';
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: 12.0),
                  PrimaryColorOverride(
                    child: TextFormField(
                      controller: passwordControler,
                      obscureText: passwordVisible,
                      decoration: InputDecoration(
                        labelText: AppLocalizations.of(context).translate('password'),
                        suffixIcon: IconButton(
                          icon: Icon(
                            // Based on passwordVisible state choose the icon
                            passwordVisible
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Theme.of(context).primaryColorDark,
                          ),
                          onPressed: () {
                            // Update the state i.e. toogle the state of passwordVisible variable
                            setState(
                                    () {
                                  passwordVisible = !passwordVisible;
                                });
                          }),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return AppLocalizations.of(context).translate('please_enter_password');
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: 15.0),
                  FlatButton(
                    onPressed: (){
                      _onTapLink('https://live.maktechgroup.in/my-account/lost-password/');
                    },
                    textColor: Colors.blue,
                    child: Text('Forgot Password'),
                  ),
                  SizedBox(height: 22.0),
                  RaisedButton(
                    elevation: 2.0,
                    onPressed: () async {
                      var login = new Map<String, dynamic>();
                      if (_formKey.currentState.validate()) {
                        login["username"] = usernameControler.text;
                        login["password"] = passwordControler.text;
                        onLogin(login);
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(AppLocalizations.of(context).translate('login')),
                    ),
                  ),

                  // PHONE LOGIN
                  /*
                  TextFormField(
                    controller: phoneController,
                    decoration: InputDecoration(
                      labelText: 'phone number',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter phone number';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 24.0),
                  RaisedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        // verifyPhone();
                      }
                    },
                    child: Text('Verify'),
                    textColor: Colors.white,
                    elevation: 7,
                    color: Colors.blue,
                  ),
                  */

                  //Social Login
                  /*
                  SizedBox(height: 34.0),
                  SizedBox(height: 24.0),
                  Align(
                      alignment: Alignment.center,
                      child: Text('--    ' +
                          AppLocalizations.of(context).translate('or_continue_with') +
                          '   --')),
                  SizedBox(height: 24.0),
                  ButtonBar(
                      alignment: MainAxisAlignment.center,
                      children: <Widget>[
                        RaisedButton(
                          elevation: 2.0,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(2.0)),
                          ),
                          color: Color(0xFF3b5998),
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(14.0, 12.0, 14.0, 12.0),
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  FontAwesomeIcons.facebookF,
                                  size: 16.0,
                                ),
                                SizedBox(width: 4.0),
                                Text(
                                    AppLocalizations.of(context).translate('facebook')),
                              ],
                            ),
                          ),
                          onPressed: () => _fbLogin(),
                        ),
                        RaisedButton(
                          elevation: 2.0,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(2.0)),
                          ),
                          color: Color(0xFFEA4335),
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(20.0, 12.0, 20.0, 12.0),
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  FontAwesomeIcons.google,
                                  size: 16.0,
                                ),
                                SizedBox(width: 4.0),
                                Text(AppLocalizations.of(context).translate('google')),
                              ],
                            ),
                          ),
                          onPressed: () => _handleSignIn(),
                        ),
                      ]),
                   */

                  // PHONE AUTH
                  /* RaisedButton(
                    elevation: 2.0,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2.0)),
                    ),
                    color: Colors.cyan,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(20.0, 12.0, 20.0, 12.0),
                      child: Row(mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(FontAwesomeIcons.phone, size: 16.0,),
                          SizedBox(width: 4.0),
                          Text("PHONE"),
                        ],
                      ),
                    ),
                    onPressed: () {
                            if (_formKey.currentState.validate()) {
                              verifyPhone();
                            }
                          },
                  ),*/

                  StreamBuilder(
                    stream: widget.homeBloc.isLoginLoading,
                    builder: (context, snapshot2) {
                      return StreamBuilder(
                        stream: widget.homeBloc.error,
                        builder: (context, snapshot1) {
                          if (snapshot1.hasData && snapshot2.data == 'false') {
                            if (snapshot1.data.data[0].code ==
                                    'invalid_username' ||
                                snapshot1.data.data[0].code == 'invalid_email')
                              return Center(
                                  child: Text(AppLocalizations.of(context).translate('invalid_username'),
                                      style: TextStyle(
                                          color: Theme.of(context).errorColor)));
                            else if (snapshot1.data.data[0].code ==
                                'incorrect_password') ;
                            return Center(
                                child: Text(AppLocalizations.of(context).translate('incorrect_password'),
                                    style: TextStyle(
                                        color: Theme.of(context).errorColor)));
                          } else if (snapshot1.hasError) {
                            return Text(snapshot1.error.toString());
                          }
                          if (snapshot2.hasData && snapshot2.data == 'true')
                            return Center(child: CircularProgressIndicator());
                          return Center(child: null);
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  void onLogin(Map<String, dynamic> login) async {
    Customer user = await widget.homeBloc.login(login);
    await UserRepo().saveUser(user);
    print(user.firstName);
    if (user != null) {
      Navigator.of(context).pop();
    }
  }

  ///Google SignIn
//**** UNCOMENT THSI FOR GOOGLE AND FB SIGN IN *****//
/*
  _handleSignIn() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();

    var login = new Map<String, dynamic>();
    login["name"] = googleUser.displayName;
    login["email"] = googleUser.email;
    Customer user = await widget.homeBloc.googleLogin(login);
    if (user != null) {
      Navigator.of(context).pop();
    }
  }

  ///Fb SignIn

  _fbLogin() async {
    final facebookLogin = FacebookLogin();
    final result = await facebookLogin.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        _sendTokenToServer(result.accessToken.token);
        _showLoggedInUI();
        break;
      case FacebookLoginStatus.cancelledByUser:
        _showCancelledMessage();
        break;
      case FacebookLoginStatus.error:
        _showErrorOnUI(result.errorMessage);
        break;
    }
  }

  Future _sendTokenToServer(token) async {
    Customer user = await widget.homeBloc.facebooklogin(token);
    if (user != null) {
      Navigator.of(context).pop();
    }
  }

  void _showLoggedInUI() {}

  void _showCancelledMessage() {}

  void _showErrorOnUI(String errorMessage) {
    print(errorMessage);
  }
*/
//Phone Authentication
/*
  signIn() async {
    try {
      final AuthCredential credential = PhoneAuthProvider.getCredential(
        verificationId: verificationId,
        smsCode: smsOTP,
      );
      final FirebaseUser user =
          (await _auth.signInWithCredential(credential)).user;
      final FirebaseUser currentUser = await _auth.currentUser();
      assert(user.uid == currentUser.uid);
      Navigator.of(context).pop();
      //Navigator.of(context).pushReplacementNamed('/homepage');
    } catch (e) {
      handleError(e);
    }
  }

  Future<void> verifyPhone() async {
    print('phone');
    print(phoneController.text);
    final PhoneCodeSent smsOTPSent = (String verId, [int forceCodeResend]) {
      this.verificationId = verId;
      smsOTPDialog(context).then((value) {
        //
      });
    };
    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: this._phoneNumber, // PHONE NUMBER TO SEND OTP
          codeAutoRetrievalTimeout: (String verId) {
            //Starts the phone number verification process for the given phone number.
            //Either sends an SMS with a 6 digit code to the phone number specified, or sign's the user in and [verificationCompleted] is called.
            this.verificationId = verId;
          },
          codeSent:
          smsOTPSent, // WHEN CODE SENT THEN WE OPEN DIALOG TO ENTER OTP.
          timeout: const Duration(seconds: 20),
          verificationCompleted: (AuthCredential phoneAuthCredential) {
            print(phoneAuthCredential);
          },
          verificationFailed: (AuthException exceptio) {
            print('${exceptio.message}');
          });
    } catch (e) {
      handleError(e);
    }
  }

  Future<bool> smsOTPDialog(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            title: Text('Enter SMS Code'),
            content: Container(
              height: 85,
              child: Column(children: [
                TextField(
                  onChanged: (value) {
                    this.smsOTP = value;
                  },
                ),
                (errorMessage != ''
                    ? Text(
                  errorMessage,
                  style: TextStyle(color: Colors.red),
                )
                    : Container())
              ]),
            ),
            contentPadding: EdgeInsets.all(10),
            actions: <Widget>[
              FlatButton(
                child: Text('Done'),
                onPressed: () {
                  _auth.currentUser().then((user) {
                    if (user != null) {
                      Navigator.of(context).pop();
                      Navigator.of(context).pushReplacementNamed('/homepage');
                    } else {
                      signIn();
                    }
                  });
                },
              )
            ],
          );
        });
  }

  handleError(PlatformException error) {
    print(error);
    switch (error.code) {
      case 'ERROR_INVALID_VERIFICATION_CODE':
        FocusScope.of(context).requestFocus(new FocusNode());
        setState(() {
          errorMessage = 'Invalid Code';
        });
        // Navigator.of(context).pop();
        smsOTPDialog(context).then((value) {
          print('sign in');
        });
        break;
      default:
        setState(() {
          errorMessage = error.message;
        });

        break;
    }
  }
*/
  Future _onTapLink(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
