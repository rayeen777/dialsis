import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_noon/drawers.dart';
//import 'package:flutter_noon/productPage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rate_my_app/rate_my_app.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:woocommerce/src/blocs/home_bloc.dart';
import 'package:woocommerce/src/models/UserRepo.dart';
import 'package:woocommerce/src/models/blocks_model.dart';
import 'package:woocommerce/src/models/customer_model.dart';
import 'package:woocommerce/src/models/post_model.dart';
import 'package:woocommerce/src/ui/accounts/address/customer_Address.dart';
import 'package:woocommerce/src/ui/accounts/currency.dart';
import 'package:woocommerce/src/ui/accounts/language/app_localizations.dart';
import 'package:woocommerce/src/ui/accounts/language/language.dart';
import 'package:woocommerce/src/ui/accounts/login.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:woocommerce/src/ui/accounts/orders/order_list.dart';
import 'package:woocommerce/src/ui/accounts/wishlist.dart';
import 'package:woocommerce/src/ui/themes/themes.dart';
import '../../options.dart';
import '../post_detail.dart';
import '../register.dart';
import 'dart:io' show Platform;

import '../themes.dart';
import '../try_demo.dart';
//import 'package:flutter/dart:html';

RateMyApp _rateMyApp = RateMyApp(
  preferencesPrefix: 'rateMyApp_',
  minDays: 7,
  minLaunches: 10,
  remindDays: 7,
  remindLaunches: 10,
);

class UserAccount extends StatefulWidget {
  final GalleryOptions options;
  final HomeBloc homeBloc;
  final ValueChanged<GalleryOptions> handleOptionsChanged;
  final Customer user;

  static final ThemeData lightTheme = ThemeData(
    primaryColor: Colors.blue,
    brightness: Brightness.light,
  );

  static final ThemeData darkTheme = ThemeData(
    primaryColor: Colors.black,
    brightness: Brightness.dark,
  );

  UserAccount({Key key, this.options, this.homeBloc, this.handleOptionsChanged,this.user})
      : super(key: key);
  @override
  _UserAccountState createState() => _UserAccountState();
}

class _UserAccountState extends State<UserAccount> {
  @override
  int _cIndex = 0;
  bool switchControl = false;
  Widget build(BuildContext context) {
    final bool isDark = Theme.of(context).brightness == Brightness.dark;
    return Scaffold(
      backgroundColor: isDark
          ? Theme.of(context).scaffoldBackgroundColor
          : Color(0xFFF5F7FF),
      appBar: AppBar(
        backgroundColor: isDark ? Colors.black : Colors.yellow[500],
        elevation: 0.5,
        centerTitle: true,
        title: Padding(
          padding: const EdgeInsets.only(top: 14.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    widget.user!=null ? widget.user.firstName : "Dial",
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontFamily: 'Lexend_Deca',
                      letterSpacing: 0.5,
                      color: Colors.cyan,
                      fontSize: 20,
                    ),
                  ),
                  Text(
                    widget.user!=null ? ' '+widget.user.lastName : ' SIS',
                    style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontFamily: 'Lexend_Deca',
                        letterSpacing: 0.5,
                        color: Colors.redAccent,
                        fontSize: 20),
                  ),
                ],
              ),
            ],
          ),
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(30.0),
          child: Theme(
            data: Theme.of(context).copyWith(accentColor: Colors.white),
            child: Container(
                height: 30.0,
                alignment: Alignment.center,
                child: Text(
                  'Your favourite online marketplace',
                  style: TextStyle(fontSize: 14.0),
                )),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 130,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: isDark ? Colors.black : Colors.white,
              ),
              child: StreamBuilder(
                  stream: widget.homeBloc.customerDetail,
                  builder: (context, snapshot) {
                    if (snapshot.hasData && snapshot.data.id > 0) {
                      return Padding(
                        padding: const EdgeInsets.all(14.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                                height: 60,
                                width: 60,
                                decoration: BoxDecoration(
                                    color: Colors.amber,
                                    shape: BoxShape.circle),
                                child: IconButton(
                                  icon: Icon(
                                    Icons.person_outline,
                                    size: 35,
                                    color: Colors.black,
                                  ),
                                  onPressed: () => null,
                                )),
                            SizedBox(height: 5),
                            Text(
                                AppLocalizations.of(context)
                                    .translate('welcome'),
                                style: TextStyle(fontWeight: FontWeight.w800))
                          ],
                        ),
                      );
                    } else {
                      return Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(14.0),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Container(
                                        height: 60,
                                        width: 60,
                                        decoration: BoxDecoration(
                                            color: Colors.amber,
                                            shape: BoxShape.circle),
                                        child: IconButton(
                                          icon: Icon(
                                            Icons.person_outline,
                                            size: 35,
                                            color: Colors.black,
                                          ),
                                          onPressed: () => Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      LoginForm(
                                                          homeBloc: widget
                                                              .homeBloc))),
                                        )),
                                    SizedBox(height: 5),
                                    Text(
                                        AppLocalizations.of(context)
                                            .translate('login'),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w800))
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(14.0),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Container(
                                        height: 60,
                                        width: 60,
                                        decoration: BoxDecoration(
                                          color: Colors.amber,
                                          shape: BoxShape.circle,
                                        ),
                                        child: IconButton(
                                          icon: Icon(Icons.person_outline,
                                              size: 35, color: Colors.black),
                                          onPressed: () => Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      RegisterForm(
                                                          homeBloc: widget
                                                              .homeBloc))),
                                        )),
                                    SizedBox(height: 5),
                                    Text(
                                        AppLocalizations.of(context)
                                            .translate("register"),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w800))
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      );
                    }
                  }),
            ),
            Divider(
              height: 1,
              thickness: 0.5,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 12, bottom: 10),
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                    AppLocalizations.of(context)
                        .translate("settings")
                        .toUpperCase(),
                    style: TextStyle(
                        fontSize: 14,
                        color: Colors.blueGrey,
                        fontWeight: FontWeight.w800)),
              ),
            ),
            Divider(
              height: 1,
              thickness: 0.1,
            ),
            StreamBuilder<BlocksModel>(
                stream: widget.homeBloc.allBlocks,
                builder: (context, snapshot) {
                  if ((snapshot.hasData &&
                      snapshot.data.languages.length > 0)) {
                    return Column(
                      children: <Widget>[
                        Container(
                          color: isDark ? Colors.black : Colors.white,
                          child: ListTile(
                            leading: Icon(Icons.mic_none),
                            dense: true,
                            title: Text(
                                AppLocalizations.of(context)
                                    .translate("language"),
                                style: TextStyle(
                                  fontWeight: FontWeight.w800,
                                  fontSize: 16,
                                )),
                            trailing: Container(
                              width: 140,
                              height: 40,
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Text('العربية',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            fontFamily: 'Cairo')),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    // Image.asset('assets/noon.png', width:30, height: 30,),
                                    Icon(
                                      Icons.arrow_forward_ios,
                                      size: 15,
                                    ),
                                  ]),
                            ),
                            onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LanguagePage(
                                        homeBloc: widget.homeBloc,
                                        options: widget.options,
                                        handleOptionsChanged:
                                            widget.handleOptionsChanged))),
                          ),
                        ),
                        Divider(
                          height: 1,
                          thickness: 0.1,
                        ),
                      ],
                    );
                  } else {
                    return Container();
                  }
                }),
            StreamBuilder<BlocksModel>(
                stream: widget.homeBloc.allBlocks,
                builder: (context, snapshot) {
                  if ((snapshot.hasData &&
                      snapshot.data.currencies.length > 0)) {
                    return Column(
                      children: <Widget>[
                        Container(
                          color: isDark ? Colors.black : Colors.white,
                          child: ListTile(
                            dense: true,
                            leading: Icon(Icons.attach_money),
                            //dense: true,
                            title: Text(
                                AppLocalizations.of(context)
                                    .translate("currency"),
                                style: TextStyle(
                                  fontWeight: FontWeight.w800,
                                  fontSize: 16,
                                )),
                            trailing: Container(
                              width: 140,
                              height: 40,
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Text(widget.homeBloc.selectedCurrency,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            fontFamily: 'Cairo')),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    // Image.asset('assets/noon.png', width:30, height: 30,),
                                    Icon(
                                      Icons.arrow_forward_ios,
                                      size: 15,
                                    ),
                                  ]),
                            ),
                            onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CurrencyPage(
                                        homeBloc: widget.homeBloc))),
                          ),
                        ),
                        Divider(
                          height: 1,
                          thickness: 0.1,
                        ),
                      ],
                    );
                  } else {
                    return Container();
                  }
                }),
            Container(
              color: isDark ? Colors.black : Colors.white,
              child: ListTile(
                dense: true,
                leading: Icon(
                  FontAwesomeIcons.paintRoller,
                  size: 15,
                ),
                title:
                    Text((AppLocalizations.of(context).translate("dark_theme")),
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: 16,
                        )),
                trailing: Switch(
                  // activeColor: isDark ? Colors.white30 : Colors.black26,
                  value: widget.options.theme == kDarkGalleryTheme,
                  inactiveThumbColor: Colors.black,
                  inactiveTrackColor: Colors.grey,
                  onChanged: (bool value) async {
                    widget.handleOptionsChanged(
                      widget.options.copyWith(
                        theme: value == true
                            ? kDarkGalleryTheme
                            : kLightGalleryTheme,
                      ),
                    );
                    var prefs = await SharedPreferences.getInstance();
                    await prefs.setString('is_dark', value.toString());
                    await prefs.setString('is_dark', value.toString());
                  },
                  activeTrackColor: isDark ? Colors.white30 : Colors.black26,
                ),
              ),
            ),
            Divider(
              height: 1,
              thickness: 0.5,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 12, bottom: 10),
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                    AppLocalizations.of(context)
                        .translate("my_account")
                        .toUpperCase(),
                    style: TextStyle(
                        fontSize: 14,
                        color: Colors.blueGrey,
                        fontWeight: FontWeight.w800)),
              ),
            ),
            Container(
              color: isDark ? Colors.black : Colors.white,
              child: ListTile(
                leading: Icon(CupertinoIcons.shopping_cart),
                dense: true,
                title: Text("My Orders",
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 16,
                    )),
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  size: 15,
                ),
                onTap: () {
                  if (widget.homeBloc.user?.id != null &&
                      widget.homeBloc.user?.id != 0) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                OrderList(homeBloc: widget.homeBloc)));
                  } else {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                LoginForm(homeBloc: widget.homeBloc)));
                  }
                },
              ),
            ),
            Divider(
              height: 1,
              thickness: 0.1,
            ),
            Container(
              color: isDark ? Colors.black : Colors.white,
              child: ListTile(
                leading: Icon(CupertinoIcons.heart),
                dense: true,
                title: Text(AppLocalizations.of(context).translate("wishlist"),
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 16,
                    )),
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  size: 15,
                ),
                onTap: () {
                  if (widget.homeBloc.user?.id != null &&
                      widget.homeBloc.user?.id != 0) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => WishList(
                                  homeBloc: widget.homeBloc,
                                )));
                  } else {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                LoginForm(homeBloc: widget.homeBloc)));
                  }
                },
              ),
            ),
            Divider(
              height: 1,
              thickness: 0.1,
            ),
            Container(
              color: isDark ? Colors.black : Colors.white,
              child: ListTile(
                leading: Icon(CupertinoIcons.location),
                dense: true,
                title: Text(AppLocalizations.of(context).translate("address"),
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 16,
                    )),
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  size: 15,
                ),
                onTap: () {
                  if (widget.homeBloc.user?.id != null &&
                      widget.homeBloc.user?.id != 0) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                CustomerAddress(homeBloc: widget.homeBloc)));
                  } else {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                LoginForm(homeBloc: widget.homeBloc)));
                  }
                },
              ),
            ),
            Divider(
              height: 1,
              thickness: 0.1,
            ),
            Container(
              color: isDark ? Colors.black : Colors.white,
              child: ListTile(
                dense: true,
                leading: Icon(CupertinoIcons.share),
                title: Text(AppLocalizations.of(context).translate("share_app"),
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 16,
                    )),
                trailing: Container(
                  width: 70,
                  height: 40,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          width: 30,
                          height: 25,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          size: 15,
                        ),
                      ]),
                ),
                onTap: () => _shareApp(),
              ),
            ),
            Divider(
              height: 1,
              thickness: 0.1,
            ),
            Container(
              color: isDark ? Colors.black : Colors.white,
              child: ListTile(
                dense: true,
                leading: Icon(CupertinoIcons.flag),
                //dense: true,
                title: Text(AppLocalizations.of(context).translate("rate_app"),
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 16,
                    )),
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  size: 15,
                ),
                onTap: () => rateApp(),
              ),
            ),
            Divider(
              height: 1,
              thickness: 0.5,
            ),
            StreamBuilder(
                stream: widget.homeBloc.customerDetail,
                builder: (context, snapshot) {
                  if (snapshot.hasData && snapshot.data.id > 0) {
                    return Column(
                      children: <Widget>[
                        Container(
                          color: isDark ? Colors.black : Colors.white,
                          child: ListTile(
                            dense: true,
                            leading: Icon(IconData(0xf4ca,
                                fontFamily: CupertinoIcons.iconFont,
                                fontPackage: CupertinoIcons.iconFontPackage)),
                            title: Text(
                                AppLocalizations.of(context)
                                    .translate("logout"),
                                style: TextStyle(
                                  fontWeight: FontWeight.w800,
                                  fontSize: 16,
                                )),
                            trailing: Icon(
                              Icons.arrow_forward_ios,
                              size: 15,
                            ),
                            onTap: () => logout(),
                          ),
                        ),
                        Divider(
                          height: 1,
                          thickness: 0.5,
                        ),
                      ],
                    );
                  } else {
                    return Container();
                  }
                }),
            StreamBuilder<BlocksModel>(
                stream: widget.homeBloc.allBlocks,
                builder: (context, snapshot) {
                  if (snapshot.hasData &&
                      snapshot.data.pages.length != 0 &&
                      snapshot.data.pages[0].url.isNotEmpty) {
                    return Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 20, left: 12, bottom: 10),
                          child: Container(
                            alignment: Alignment.centerLeft,
                            child: Text(
                                AppLocalizations.of(context)
                                    .translate("info")
                                    .toUpperCase(),
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.blueGrey,
                                    fontWeight: FontWeight.w800)),
                          ),
                        ),
                        ListView.builder(
                            shrinkWrap: true,
                            itemCount: snapshot.data.pages.length,
                            itemBuilder: (BuildContext ctxt, int index) {
                              return snapshot.data.pages[index].url.isNotEmpty
                                  ? Column(
                                      children: <Widget>[
                                        Container(
                                          color: isDark
                                              ? Colors.black
                                              : Colors.white,
                                          child: ListTile(
                                            dense: true,
                                            onTap: () {
                                              var post = Post();
                                              post.id = int.parse(snapshot
                                                  .data.pages[index].url);
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          PostDetail(
                                                              post: post)));
                                            },
                                            leading: ExcludeSemantics(
                                              child: Icon(CupertinoIcons.info),
                                            ),
                                            trailing: Icon(
                                              Icons.arrow_forward_ios,
                                              size: 15,
                                            ),
                                            title: Text(
                                              snapshot.data.pages[index].title,
                                              style: TextStyle(
                                                fontWeight: FontWeight.w800,
                                                fontSize: 16,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Divider(height: 0.0),
                                      ],
                                    )
                                  : Container();
                            }),
                      ],
                    );
                  } else {
                    return Container();
                  }
                }),
//            Padding(
//              padding: const EdgeInsets.only(top: 20, left: 12, bottom: 10),
//              child: Container(
//                alignment: Alignment.centerLeft,
//                child: Text(
//                    'DEMO',
//                    style: TextStyle(
//                        fontSize: 14,
//                        color: Colors.blueGrey,
//                        fontWeight: FontWeight.w800)),
//              ),
//            ),
//            Container(
//              color: isDark ? Colors.black : Colors.white,
//              child: ListTile(
//                dense: true,
//                leading: Icon(IconData(0xf37b,
//                    fontFamily: CupertinoIcons.iconFont,
//                    fontPackage: CupertinoIcons.iconFontPackage)),
//                //dense: true,
//                title: Text(AppLocalizations.of(context).translate("Themes"),
//                    style: TextStyle(
//                      fontWeight: FontWeight.w800,
//                      fontSize: 16,
//                    )),
//                trailing: Icon(
//                  Icons.arrow_forward_ios,
//                  size: 15,
//                ),
//                onTap: () => Navigator.push(
//                    context,
//                    MaterialPageRoute(
//                        builder: (context) => DemoThemes(
//                            options: widget.options,
//                            handleOptionsChanged:
//                            widget.handleOptionsChanged))),
//              ),
//            ),
//            Divider(
//              height: 1,
//              thickness: 0.5,
//            ),
//            Container(
//              color: isDark ? Colors.black : Colors.white,
//              child: ListTile(
//                dense: true,
//                leading: Icon(IconData(0xf3a2,
//                    fontFamily: CupertinoIcons.iconFont,
//                    fontPackage: CupertinoIcons.iconFontPackage)),
//                //dense: true,
//                title: Text(AppLocalizations.of(context).translate("Try demo for your site"),
//                    style: TextStyle(
//                      fontWeight: FontWeight.w800,
//                      fontSize: 16,
//                    )),
//                trailing: Icon(
//                  Icons.arrow_forward_ios,
//                  size: 15,
//                ),
//                onTap: () => Navigator.push(
//                    context,
//                    MaterialPageRoute(
//                        builder: (context) => TryDemo(homeBloc: widget.homeBloc))),
//              ),
//            ),
//            Divider(
//              height: 1,
//              thickness: 0.5,
//            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                    //padding: EdgeInsets.only(top: 80,) ,
                    width: 220,
                    height: 40,
                    //color: Colors.red,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(
                            FontAwesomeIcons.facebookF,
                            size: 22,
                            color: Colors.blueGrey,
                          ),
                          onPressed: () {
                            _onTapLink('https://www.facebook.com/Skillindi/');
                          },
                        ),
                        IconButton(
                          icon: Icon(
                            FontAwesomeIcons.twitter,
                            size: 22,
                            color: Colors.blueGrey,
                          ),
                          onPressed: () {
                            _onTapLink('https://twitter.com/skilled_india');
                          },
                        ),
                        IconButton(
                          icon: Icon(
                            FontAwesomeIcons.youtube,
                            size: 22,
                            color: Colors.blueGrey,
                          ),
                          onPressed: () {
                            _onTapLink('https://www.youtube.com/playlist?list=PLLN6rG27uClffYAt9ndXXraUX8G4CDPY-');
                          },
                        ),
                        IconButton(
                          icon: Icon(
                            FontAwesomeIcons.whatsapp,
                            size: 22,
                            color: Colors.blueGrey,
                          ),
                          onPressed: () {
                            _onTapLink('https://wa.me/+917007229963&text=How May I Help You');
                          },
                        )
                      ],
                    )),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future _onTapLink(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _shareApp() {
    if (Platform.isIOS) {
      //Add ios App link here
      Share.share(
          'check out thia app https://play.google.com/store/apps/details?id=com.mstoreapp.woocommerce');
    } else {
      //Add android app link here
      Share.share(
          'check out thia app https://play.google.com/store/apps/details?id=com.mstoreapp.woocommerce');
    }
  }

  rateApp() {
    _rateMyApp.showStarRateDialog(
      context,
      title: 'Rate this app',
      message:
          'You like this app ? Then take a little bit of your time to leave a rating :',
      onRatingChanged: (stars) {
        return [
          FlatButton(
            child: Text('OK'),
            onPressed: () {
              print('Thanks for the ' +
                  (stars == null ? '0' : stars.round().toString()) +
                  ' star(s) !');
              // You can handle the result as you want (for instance if the user puts 1 star then open your contact page, if he puts more then open the store page, etc...).
              _rateMyApp.doNotOpenAgain = true;
              _rateMyApp.save().then((v) => Navigator.pop(context));
            },
          ),
        ];
      },
      ignoreIOS: false,
      dialogStyle: DialogStyle(
        titleAlign: TextAlign.center,
        messageAlign: TextAlign.center,
        messagePadding: EdgeInsets.only(bottom: 20),
      ),
      starRatingOptions: StarRatingOptions(),
    );
  }

  void logout() async {
    await UserRepo().clearPreference();
    widget.homeBloc.logout();
  }

  void toggleSwitch(bool value) {
    if (switchControl == false) {
      setState(() {
        switchControl = true;
      });
      print('Switch is ON');
      // Put your code here which you want to execute on Switch ON event.

    } else {
      setState(() {
        switchControl = false;
      });
      print('Switch is OFF');
      // Put your code here which you want to execute on Switch OFF event.
    }
  }
}

class _ThemeItem extends StatelessWidget {
  const _ThemeItem(this.options, this.onOptionsChanged);

  final GalleryOptions options;
  final ValueChanged<GalleryOptions> onOptionsChanged;

  @override
  Widget build(BuildContext context) {
    return _BooleanItem(
      AppLocalizations.of(context).translate('dark_theme'),
      options.theme == kDarkGalleryTheme,
      (bool value) {
        onOptionsChanged(
          options.copyWith(
            theme: value == true ? kDarkGalleryTheme : kLightGalleryTheme,
          ),
        );
      },
      switchKey: const Key('dark_theme'),
    );
  }
}

class _BooleanItem extends StatelessWidget {
  const _BooleanItem(this.title, this.value, this.onChanged, {this.switchKey});

  final String title;
  final bool value;
  final ValueChanged<bool> onChanged;
  // [switchKey] is used for accessing the switch from driver tests.
  final Key switchKey;

  @override
  Widget build(BuildContext context) {
    final bool isDark = Theme.of(context).brightness == Brightness.dark;
    return _OptionsItem(
      child: Row(
        children: <Widget>[
          Expanded(child: Text(title)),
          Switch(
            key: switchKey,
            value: value,
            onChanged: onChanged,
            activeColor: const Color(0xFF39CEFD),
            activeTrackColor: isDark ? Colors.white30 : Colors.black26,
          ),
        ],
      ),
    );
  }
}

class _OptionsItem extends StatelessWidget {
  const _OptionsItem({Key key, this.child}) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    final double textScaleFactor = MediaQuery.textScaleFactorOf(context);

    return MergeSemantics(
      child: Container(
        // constraints: BoxConstraints(minHeight: _kItemHeight * textScaleFactor),
        // padding: _kItemPadding,
        alignment: AlignmentDirectional.centerStart,
        child: DefaultTextStyle(
          style: DefaultTextStyle.of(context).style,
          maxLines: 2,
          overflow: TextOverflow.fade,
          child: IconTheme(
            data: Theme.of(context).primaryIconTheme,
            child: child,
          ),
        ),
      ),
    );
  }


}
