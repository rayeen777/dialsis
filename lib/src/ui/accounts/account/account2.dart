import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../models/blocks_model.dart';
import '../../../models/post_model.dart';
import 'package:rate_my_app/rate_my_app.dart';
import '../post_detail.dart';
import '../themes.dart';
import '../../../blocs/home_bloc.dart';
import 'dart:io' show Platform;
import '../login.dart';
import '../orders/order_list.dart';
import '../register.dart';
import '../wishlist.dart';
import '../../options.dart';
import '../address/customer_Address.dart';
import '../currency.dart';
import '../language/language.dart';
import 'package:share/share.dart';

import '../language/app_localizations.dart';
import '../try_demo.dart';

RateMyApp _rateMyApp = RateMyApp(
  preferencesPrefix: 'rateMyApp_',
  minDays: 7,
  minLaunches: 10,
  remindDays: 7,
  remindLaunches: 10,
);

class UserAccount extends StatefulWidget {
  final GalleryOptions options;
  final HomeBloc homeBloc;
  final ValueChanged<GalleryOptions> handleOptionsChanged;

  UserAccount({Key key, this.options, this.homeBloc, this.handleOptionsChanged})
      : super(key: key);

  @override
  _UserAccountState createState() => _UserAccountState();
}

class _UserAccountState extends State<UserAccount> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        title: Text(AppLocalizations.of(context).translate("account"),),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          /*SliverAppBar(
            elevation: 1.0,
            floating: true,
            pinned: true,
            snap: true,
            backgroundColor: Theme.of(context).canvasColor,
            flexibleSpace: FlexibleSpaceBar(
              //centerTitle: true,
              title: Container(), //Text('ACCOUNT'),
              /*Container(
                  padding: EdgeInsets.only(top: 10.0),
                  height: 50,
                  width: 50,
                  child:
                      Image.asset('assets/images/logo.png', fit: BoxFit.cover)
              ),*/
              background: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Container(
                    color: Theme.of(context).primaryColorDark,
                    child: Image.asset('assets/images/account.jpg',
                        fit: BoxFit.cover),
                  ),
                ],
              ),
            ),
            expandedHeight: 150,
          ),*/
          StreamBuilder(
              stream: widget.homeBloc.customerDetail,
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data.id > 0) {
                  return buildLoggedInList(context);
                } else {
                  return buildLoggedOutList(context);
                }
              }),
          buildCommonList(context),
          StreamBuilder<BlocksModel>(
              stream: widget.homeBloc.allBlocks,
              builder: (context, snapshot) {
                return snapshot.hasData
                    ? buildPageList(snapshot)
                    : SliverToBoxAdapter();
              }),
          StreamBuilder(
              stream: widget.homeBloc.customerDetail,
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data.id > 0) {
                  return buildLogoutList(context);
                } else {
                  return SliverToBoxAdapter();
                }
              }),
          //buildOtherList(context),
        ],
      ),
    );
  }

  SliverList buildLoggedOutList(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate([
        ListTile(
          onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => LoginForm(homeBloc: widget.homeBloc))),
          leading: ExcludeSemantics(child: Icon(CupertinoIcons.padlock,)),
          title: Text(
            AppLocalizations.of(context).translate("login"),
          ),
        ),
        Divider(height: 0.0),
        ListTile(
          onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      RegisterForm(homeBloc: widget.homeBloc))),
          leading: ExcludeSemantics(child: Icon(CupertinoIcons.person)),
          title: Text(
            AppLocalizations.of(context).translate("register"),
          ),
        ),
        Divider(height: 0.0),
      ]),
    );
  }

  SliverList buildLoggedInList(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate([
        ListTile(
          onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => WishList(
                        homeBloc: widget.homeBloc,
                      ))),
          leading: ExcludeSemantics(child: Icon(CupertinoIcons.heart)),
          title: Text(
            AppLocalizations.of(context).translate("wishlist"),
          ),
        ),
        Divider(height: 0.0),
        ListTile(
          onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => OrderList(homeBloc: widget.homeBloc))),
          leading: ExcludeSemantics(child: Icon(CupertinoIcons.shopping_cart)),
          title: Text(
            AppLocalizations.of(context).translate("orders"),
          ),
        ),
        Divider(height: 0.0),
        ListTile(
          onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      CustomerAddress(homeBloc: widget.homeBloc))),
          leading: ExcludeSemantics(child: Icon(CupertinoIcons.location)),
          title: Text(
            AppLocalizations.of(context).translate("address"),
          ),
        ),
        Divider(height: 0.0),
      ]),
    );
  }

  SliverList buildCommonList(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate([
        Divider(height: 0.0),
        ListTile(
          onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => GalleryOptionsPage(
                      options: widget.options,
                      onOptionsChanged: widget.handleOptionsChanged,
                    )),
          ),
          leading: ExcludeSemantics(child: Icon(CupertinoIcons.settings)),
          title: Text(
            AppLocalizations.of(context).translate("settings"),
          ),
        ),
        Divider(height: 0.0),
        StreamBuilder<BlocksModel>(
            stream: widget.homeBloc.allBlocks,
            builder: (context, snapshot) {
              if ((snapshot.hasData && snapshot.data.languages.length > 0)) {
                return Column(
                  children: <Widget>[
                    ListTile(
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LanguagePage(
                                  homeBloc: widget.homeBloc,
                                  options: widget.options,
                                  handleOptionsChanged:
                                      widget.handleOptionsChanged))),
                      leading: ExcludeSemantics(
                          child: Icon((IconData(0xf38c,
                              fontFamily: CupertinoIcons.iconFont,
                              fontPackage: CupertinoIcons.iconFontPackage)))),
                      title: Text(
                        AppLocalizations.of(context).translate("language"),
                      ),
                    ),
                    Divider(height: 0.0),
                  ],
                );
              } else {
                return Container();
              }
            }),
        StreamBuilder<BlocksModel>(
            stream: widget.homeBloc.allBlocks,
            builder: (context, snapshot) {
              if ((snapshot.hasData && snapshot.data.currencies.length > 0)) {
                return Column(
                  children: <Widget>[
                    ListTile(
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  CurrencyPage(homeBloc: widget.homeBloc))),
                      leading:
                      ExcludeSemantics(child: Icon(Icons.attach_money)),
                      title: Text(
                        AppLocalizations.of(context).translate("currency"),
                      ),
                    ),
                    Divider(height: 0.0),
                  ],
                );
              } else {
                return Container();
              }
            }),
        ListTile(
          onTap: () => _shareApp(),
          leading: ExcludeSemantics(child: Icon(CupertinoIcons.share)),
          title: Text(
            AppLocalizations.of(context).translate("share_app"),
          ),
        ),
        Divider(height: 0.0),
        ListTile(
          onTap: () => rateApp(),
          leading: ExcludeSemantics(child: Icon(CupertinoIcons.flag)),
          title: Text(
            AppLocalizations.of(context).translate("rate_app"),
          ),
        ),
        Divider(height: 0.0),
      ]),
    );
  }

  SliverList buildLogoutList(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate([
        ListTile(
          onTap: () => logout(),
          leading: ExcludeSemantics(
            child: RotatedBox(
              quarterTurns: 1,
              child: Icon(IconData(0xf4ca,
                  fontFamily: CupertinoIcons.iconFont,
                  fontPackage: CupertinoIcons.iconFontPackage)),
            ),
          ),
          title: Text(
            AppLocalizations.of(context).translate("logout"),
          ),
        ),
        Divider(height: 0.0),
      ]),
    );
  }

  SliverList buildOtherList(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate([
        ListTile(
          onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => DemoThemes(
                      options: widget.options,
                      handleOptionsChanged:
                      widget.handleOptionsChanged))),
          leading: ExcludeSemantics(
            child: RotatedBox(
              quarterTurns: 1,
              child: Icon(IconData(0xf37b,
                  fontFamily: CupertinoIcons.iconFont,
                  fontPackage: CupertinoIcons.iconFontPackage)),
            ),
          ),
          title: Text(
            AppLocalizations.of(context).translate("Themes"),
          ),
        ),
        Divider(height: 0.0),
        ListTile(
          onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => TryDemo(homeBloc: widget.homeBloc))),
          leading: ExcludeSemantics(
            child: Icon(IconData(0xf3a2,
                fontFamily: CupertinoIcons.iconFont,
                fontPackage: CupertinoIcons.iconFontPackage))
          ),
          title: Text(
            AppLocalizations.of(context).translate("Try demo for your site"),
          ),
        ),
        Divider(height: 0.0),
      ]),
    );
  }

  void logout() async {
    widget.homeBloc.logout();
  }

  buildPageList(AsyncSnapshot<BlocksModel> snapshot1) {
    return SliverPadding(
      padding: EdgeInsets.all(0.0),
      sliver: SliverList(
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return snapshot1.data.pages[index].url.isNotEmpty
                ? Column(
                    children: <Widget>[
                      ListTile(
                        onTap: () {
                          var post = Post();
                          post.id = int.parse(snapshot1.data.pages[index].url);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      PostDetail(post: post)));
                        },
                        leading: ExcludeSemantics(
                          child: Icon(CupertinoIcons.info),
                        ),
                        title: Text(
                          snapshot1.data.pages[index].title,
                        ),
                      ),
                      Divider(height: 0.0),
                    ],
                  )
                : Container();
          },
          childCount: snapshot1.data.pages.length,
        ),
      ),
    );
  }

  _shareApp() {
    if (Platform.isIOS) {
      //Add ios App link here
      Share.share(
          'check out thia app https://play.google.com/store/apps/details?id=com.mstoreapp.woocommerce');
    } else {
      //Add android app link here
      Share.share(
          'check out thia app https://play.google.com/store/apps/details?id=com.mstoreapp.woocommerce');
    }
  }

  rateApp() {
    _rateMyApp.showStarRateDialog(
      context,
      title: 'Rate this app',
      message: 'You like this app ? Then take a little bit of your time to leave a rating :',
      onRatingChanged: (stars) {
        return [
          FlatButton(
            child: Text('OK'),
            onPressed: () {
              print('Thanks for the ' + (stars == null ? '0' : stars.round().toString()) + ' star(s) !');
              // You can handle the result as you want (for instance if the user puts 1 star then open your contact page, if he puts more then open the store page, etc...).
              _rateMyApp.doNotOpenAgain = true;
              _rateMyApp.save().then((v) => Navigator.pop(context));
            },
          ),
        ];
      },
      ignoreIOS: false,
      dialogStyle: DialogStyle(
        titleAlign: TextAlign.center,
        messageAlign: TextAlign.center,
        messagePadding: EdgeInsets.only(bottom: 20),
      ),
      starRatingOptions: StarRatingOptions(),
    );
  }

}


