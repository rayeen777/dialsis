//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:woocommerce/SplashWidget.dart';
import 'models/app_state_model.dart';
import 'blocs/home_bloc.dart';
import 'resources/api_provider.dart';
import 'ui/accounts/account/account.dart';
import 'ui/accounts/language/app_localizations.dart';
import 'ui/products/deals.dart';
import 'ui/checkout/cart/cart.dart';
import 'ui/home/home.dart';
import 'ui/categories.dart';
import 'ui/options.dart';
import 'ui/scales.dart';
import 'ui/themes/themes.dart';
import 'package:flutter/foundation.dart' show defaultTargetPlatform;

class App extends StatefulWidget {

  final HomeBloc homeBloc;

  AppStateModel model = AppStateModel();

  final ValueChanged<GalleryOptions> onOptionsChanged;
  App({Key key, this.homeBloc, this.onOptionsChanged}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {

  final apiProvider = ApiProvider();

  //final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    widget.model.options = GalleryOptions(
      theme: kLightGalleryTheme,
      platform: defaultTargetPlatform,
      textScaleFactor: kAllGalleryTextScaleValues[0],
    );
    widget.model.fetchLocale();
    apiProviderInt();
    //_firebaseMessaging.requestNotificationPermissions();
  }

  void apiProviderInt() async {
    await apiProvider.init();
    widget.homeBloc.fetchAllBlocks();
    widget.homeBloc.getCustomerDetails();
    widget.homeBloc.fetchAllProducts();
  }

  void handleOptionsChanged(GalleryOptions newOptions) {
    setState(() {
      widget.model.options = newOptions;
    });
  }

  Widget _applyTextScaleFactor(Widget child) {
    return Builder(
      builder: (BuildContext context) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(
            textScaleFactor: widget.model.options.textScaleFactor.scale,
          ),
          child: child,
        );
      },
    );
  }

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return ScopedModel<AppStateModel>(
      model: widget.model,
      child: ScopedModelDescendant<AppStateModel>(
        builder: (context, child, model) => MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: apiProvider.lan == 'ar'
              ? widget.model.options.theme.data.copyWith(
                  platform: widget.model.options.platform,
                  textTheme: widget.model.options.theme.data.textTheme
                      .apply(fontFamily: 'Cairo'),
                  primaryTextTheme: widget.model.options.theme.data.primaryTextTheme
                      .apply(fontFamily: 'Cairo'),
                  accentTextTheme: widget.model.options.theme.data.accentTextTheme
                      .apply(fontFamily: 'Cairo'),
                )
              : widget.model.options.theme.data.copyWith(
                  platform: widget.model.options.platform,
                ),
          home:SplashWidget(
            homeBloc: widget.homeBloc,
            options: widget.model.options,
            handleOptionsChanged: handleOptionsChanged,
          ),
          locale: widget.model.appLocal,
          supportedLocales: [
            Locale('en', 'US'),
            Locale('ar', ''),
          ],
          localizationsDelegates: [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
        ),
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}

class App2 extends StatefulWidget {
  GalleryOptions options;
  final HomeBloc homeBloc;
  final ValueChanged<GalleryOptions> onOptionsChanged;

  App2({Key key, this.homeBloc, this.options, this.onOptionsChanged})
      : super(key: key);
  @override
  _App2State createState() => _App2State();
}

class _App2State extends State<App2> {
  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  void handleOptionsChanged(GalleryOptions newOptions) {
    setState(() {
      widget.options = newOptions;
    });
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _children = [
      Home(
        homeBloc: widget.homeBloc,
        options: widget.options,
        handleOptionsChanged: widget.onOptionsChanged,
      ),
      Categories(homeBloc: widget.homeBloc),
      Deals(homeBloc: widget.homeBloc),
      // option and on option change function
      CartPage(homeBloc: widget.homeBloc),
      UserAccount(
          options: widget.options,
          handleOptionsChanged: widget.onOptionsChanged,
          homeBloc: widget.homeBloc),
    ];

    return Scaffold(
      backgroundColor: Colors.white,
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        showUnselectedLabels: true,
        showSelectedLabels: true,
        unselectedItemColor: widget.options.theme.name == 'Dark'
            ? Colors.white60
            : widget.options.theme.data.primaryIconTheme.color.withOpacity(0.4),
        selectedItemColor: widget.options.theme.name == 'Dark'
            ? Colors.white
            : widget.options.theme.data.primaryIconTheme.color,
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        backgroundColor: widget.options.theme.name == 'Dark'
            ? Colors.black
            : widget.options.theme.data.primaryColor,
        elevation: 2.0,
        //type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            backgroundColor: widget.options.theme.name == 'Dark'
                ? Colors.black
                : widget.options.theme.data.primaryColor,
            //widget.options.theme.data.primaryColor,
            icon: new Icon(CupertinoIcons.home),
            //  title: new Text('Home'),
            title: Text(
              AppLocalizations.of(context).translate("home"),
            ),
          ),
          BottomNavigationBarItem(
            backgroundColor: widget.options.theme.name == 'Dark'
                ? Colors.black
                : widget.options.theme.data.primaryColor,
            icon: new Icon(IconData(0xf44f,
                fontFamily: CupertinoIcons.iconFont,
                fontPackage: CupertinoIcons.iconFontPackage)),
            //title: new Text(),
            // title: new Text('Category'),
            title: Text(
              AppLocalizations.of(context).translate("tab_categories"),
            ),
          ),
          BottomNavigationBarItem(
            backgroundColor: widget.options.theme.name == 'Dark'
                ? Colors.black
                : widget.options.theme.data.primaryColor,
            icon: new Icon(IconData(0xf3e5,
                fontFamily: CupertinoIcons.iconFont,
                fontPackage: CupertinoIcons.iconFontPackage)),
            //title: new Text(),
            // title: new Text('Category'),
            title: Text(
              AppLocalizations.of(context).translate("deals"),
            ),
          ),
          BottomNavigationBarItem(
            backgroundColor: widget.options.theme.name == 'Dark'
                ? Colors.black
                : widget.options.theme.data.primaryColor,
            icon: Stack(children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                child: new Icon(CupertinoIcons.shopping_cart),
              ),
              new Positioned(
                // draw a red marble
                top: -3.0,
                right: -4.0,
                child: StreamBuilder<int>(
                    stream: widget.homeBloc.cartCount,
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data != 0) {
                        return Card(
                            elevation: 0,
                            clipBehavior: Clip.antiAlias,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(4.0),
                            ),
                            color: Colors.redAccent,
                            child: Container(
                                padding: EdgeInsets.all(2),
                                constraints: BoxConstraints(minWidth: 20.0),
                                child: Center(
                                    child: Text(
                                  snapshot.data.toString(),
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.white,
                                      backgroundColor: Colors.redAccent),
                                ))));
                      } else
                        return Container();
                    }),
              ),
            ]),
            title: Text(
              AppLocalizations.of(context).translate("cart"),
            ),
          ),
          BottomNavigationBarItem(
            backgroundColor: widget.options.theme.name == 'Dark'
                ? Colors.black
                : widget.options.theme.data.primaryColor,
            icon: Icon(CupertinoIcons.person),
            title: Text(
              AppLocalizations.of(context).translate("account"),
            ),
          ),
        ],
      ),
    );
  }
}
