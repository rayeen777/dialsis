import 'dart:convert';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/checkout/stripeSource.dart';
import '../models/checkout/stripe_token.dart';
import '../models/category_model.dart';
import '../models/customer_model.dart';
import '../models/errors/error.dart';
import '../models/errors/register_error.dart';
import '../models/orders_model.dart';
import '../models/blocks_model.dart';
import '../models/cart/addToCartErrorModel.dart';
import '../models/cart/cart_model.dart';
import '../models/checkout/order_result_model.dart';
import '../models/checkout/order_review_model.dart';
import '../models/checkout/checkout_form_model.dart';
import '../models/product_model.dart';
import '../resources/api_provider.dart';
import 'package:http/http.dart';
import 'package:rxdart/rxdart.dart';
import 'package:html/parser.dart';

class HomeBloc  with ChangeNotifier{
  BlocksModel blocks;
  var filter = new Map<String, dynamic>();
  int page = 1;
  int searchPage = 1;
  String initialSelectedCountry = 'US';
  var formData = new Map<String, String>();
  int count = 0;
  CartModel shoppingCart;
  bool hasMoreWishList = false;
  int wishListPage = 0;
  List<Product> wishListProducts = [];
  List<int> wishListIds = [];
  OrderReviewModel orderReviewData;
  String selectedCurrency = 'USD';
  double maxPrice = 1000;
  List<Category> categories = [];

  Client client = Client();
  final apiProvider = ApiProvider();

  final _blocksFetcher = BehaviorSubject<BlocksModel>();
  final _hasMoreItemsFetcher = BehaviorSubject<bool>();
  final _hasMoreWishListFetcher = BehaviorSubject<bool>();
  final _checkoutFormFetcher = BehaviorSubject<CheckoutFormModel>();
  final _orderReviewFetcher = BehaviorSubject<OrderReviewModel>();
  final _cartFetcher = BehaviorSubject<CartModel>();
  final _orderResultFetcher = PublishSubject<OrderResult>();
  final _searchFetcher = BehaviorSubject<List<Product>>();
  final _cartCountFetcher = BehaviorSubject<int>();
  final _isLoadingFetcher = BehaviorSubject<String>();
  final addingToCartFetcher = BehaviorSubject<bool>();
  final _placingOrderFetcher = BehaviorSubject<bool>();
  final _wishListFetcher = BehaviorSubject<List<Product>>();
  final _addToCartErrorFetcher = PublishSubject<AddToCartErrorModel>();


  Observable<BlocksModel> get allBlocks => _blocksFetcher.stream;
  Observable<bool> get hasMoreItems => _hasMoreItemsFetcher.stream;
  Observable<CheckoutFormModel> get checkoutForm => _checkoutFormFetcher.stream;
  Observable<OrderReviewModel> get orderReview => _orderReviewFetcher.stream;
  Observable<CartModel> get cart => _cartFetcher.stream;
  Observable<OrderResult> get orderResult => _orderResultFetcher.stream;
  Observable<List<Product>> get searchResults => _searchFetcher.stream;
  Observable<int> get cartCount => _cartCountFetcher.stream;
  Observable<String> get isLoading => _isLoadingFetcher.stream;
  Observable<bool> get hasMoreItemsItems => _hasMoreWishListFetcher.stream;
  Observable<AddToCartErrorModel> get addToCartError => _addToCartErrorFetcher.stream;
  Observable<bool> get addingToCart => addingToCartFetcher.stream;
  Observable<bool> get placingOrder => _placingOrderFetcher.stream;
  Observable<List<Product>> get wishList => _wishListFetcher.stream;


  final Map<int, int> _productsInCart = <int, int>{};

  Map<int, int> get productsInCart => Map<int, int>.from(_productsInCart);

  fetchAllBlocks() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String blocksString = prefs.getString('blocks');
    String storedCurrecny = prefs.getString('currency');
    if(storedCurrecny != null && storedCurrecny.isNotEmpty && storedCurrecny != '0') {
      selectedCurrency = storedCurrecny;
      await switchCurrency(storedCurrecny);
    }
    if(blocksString != null && blocksString.isNotEmpty && blocksString != '0') {
      try {
        blocks = BlocksModel.fromJson(json.decode(blocksString));
        categories = blocks.categories;
        maxPrice = blocks.maxPrice.toDouble();
        _blocksFetcher.sink.add(blocks);
      } catch (e, s) {}
    }
    _hasMoreItemsFetcher.sink.add(true);
    final response = await apiProvider.fetchBlocks();
    blocks = BlocksModel.fromJson(json.decode(response.body));
    categories = blocks.categories;
    maxPrice = blocks.maxPrice.toDouble();
    user = blocks.user;
    //selectedCurrency = blocks.currency; // Uncomment once backend currency switcher is working with WPML
    _blocksFetcher.sink.add(blocks);
    getCart();
  }

  loadMoreRecentProducts() async {
    page = page + 1;
    filter['page'] = page.toString();
    final response = await apiProvider.fetchProducts(filter);
    if (response.statusCode == 200) {
      List<Product> products = productModelFromJson(response.body);
      blocks.recentProducts.addAll(products);
      _blocksFetcher.sink.add(blocks);
      if(products.length == 0){
        _hasMoreItemsFetcher.sink.add(false);
      }
    } else {
      throw Exception('Failed to load products');
    }
  }

  void getCheckoutForm() async {
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-get_checkout_form', Map());//formData.toJson();
    if (response.statusCode == 200) {
      CheckoutFormModel checkoutForm = CheckoutFormModel.fromJson(json.decode(response.body));
      _checkoutFormFetcher.sink.add(checkoutForm);
    } else {
      throw Exception('Failed to load checkout form');
    }
  }

  Future<OrderResult> placeOrder() async {
    _placingOrderFetcher.sink.add(true);
    for(var i= 0; i < orderReviewData.shipping.length; i++) {
      if(orderReviewData.shipping[i].chosenMethod != false) {
        formData['shipping_method['+ i.toString() +']'] = orderReviewData.shipping[i].chosenMethod;
      }
    }
    final response = await apiProvider.post(
        '/index.php/checkout?wc-ajax=checkout', formData);
    print(response.body);
    _placingOrderFetcher.sink.add(false);
    if (response.statusCode == 200) {
      OrderResult orderResult = OrderResult.fromJson(json.decode(response.body));
      _orderResultFetcher.sink.add(orderResult);
      return orderResult;
    } else {
      throw Exception('Failed to display order Result');
    }
  }

  dispose() {
    _blocksFetcher.close();
    _hasMoreItemsFetcher.close();
    _cartFetcher.close();
    _checkoutFormFetcher.close();
    _orderReviewFetcher.close();
    _orderResultFetcher.close();
    _searchFetcher.close();
    _cartCountFetcher.close();
    _isLoadingFetcher.close();
    _hasMoreWishListFetcher.close();
    _wishListFetcher.close();
    _addToCartErrorFetcher.close();
    addingToCartFetcher.close();
    _placingOrderFetcher.close();

    _errorFetcher.close();
    _registerErrorFetcher.close();
    _isLoginLoadingFetcher.close();
    _customersFetcher.close();
    _ordersFetcher.close();
    _hasMoreOrdersFetcher.close();

    _dealsProductsFetcher.close();
    _hasMoreDealsFetcher.close();
    _isLoadingDealsFetcher.close();
  }

  void updateOrderReview() async {
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-update_order_review', Map());
    if (response.statusCode == 200) {
      orderReviewData = OrderReviewModel.fromJson(json.decode(response.body));
      if(orderReviewData.paymentMethods != null) formData['payment_method'] = orderReviewData.paymentMethods.firstWhere((method) => method.chosen == true).id;
      _orderReviewFetcher.sink.add(orderReviewData);
    } else {
      throw Exception('Failed to load order review');
    }
  }

  void updateOrderReview2() async {
    for(var i= 0; i < orderReviewData.shipping.length; i++) {
      if(orderReviewData.shipping[i].chosenMethod != false) {
        formData['shipping_method['+ i.toString() +']'] = orderReviewData.shipping[i].chosenMethod;
      }
    }
    print(formData);
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-update_order_review', formData);
    if (response.statusCode == 200) {
      orderReviewData = OrderReviewModel.fromJson(json.decode(response.body));
      _orderReviewFetcher.sink.add(orderReviewData);
    } else {
      throw Exception('Failed to load order review');
    }
  }

  void onCountryChange(CountryCode countryCode) {
    formData['billing_country'] = countryCode.code;
  }

  void addToCart(data) async {
    addingToCartFetcher.sink.add(true);
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-add_to_cart', data);
    if (response.statusCode == 200) {
      addingToCartFetcher.sink.add(false);
      getCart();
    } else {
      addingToCartFetcher.sink.add(false);
      AddToCartErrorModel addToCartError = addToCartErrorModelFromJson(response.body);
      Fluttertoast.showToast(msg: addToCartError.data.notice);
      _addToCartErrorFetcher.sink.add(addToCartError);
    }
  }

  void getCart() async {
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-cart', Map());
    if (response.statusCode == 200) {
      shoppingCart = CartModel.fromJson(json.decode(response.body));
      updateCartCount();
      _cartFetcher.sink.add(shoppingCart);
    } else {
      throw Exception('Failed to load cart');
    }
  }

  void removeItemFromCart(String key) {
    shoppingCart.cartContents.removeWhere((item) => item.key == key);
    _cartFetcher.sink.add(shoppingCart);
    apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-remove_cart_item&item_key=' + key, Map());
    updateCartCount();
  }

  void updateCartCount() {
    count = 0;
    for (var i = 0; i < shoppingCart.cartContents.length; i++) {
      count = count + shoppingCart.cartContents[i].quantity;
    }
    _cartCountFetcher.sink.add(count);
  }

  Future<void> applyCoupon(String couponCode) async {
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-apply_coupon', {"coupon_code": couponCode});
    if (response.statusCode == 200) {
      getCart();
      Fluttertoast.showToast(msg: _parseHtmlString(response.body));
    } else {
      throw Exception('Failed to load cart');
    }
  }


  //WishList
  getWishList() async {
    final response =  await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-get_wishlist', Map());
    wishListProducts = productModelFromJson(response.body);
    _wishListFetcher.sink.add(wishListProducts);
    wishListIds = wishListProducts.map((item) => item.id).toList();
    if(wishListProducts.length < 10){
      hasMoreWishList = false;
      _hasMoreWishListFetcher.sink.add(hasMoreWishList);
    }
  }

  Future loadMoreWishList() async {
    wishListPage= wishListPage + 1;
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-get_wishlist', {'page': wishListPage.toString()});
   List<Product> products = productModelFromJson(response.body);
    wishListProducts.addAll(products);
    _wishListFetcher.sink.add(wishListProducts);
    wishListIds = wishListProducts.map((item) => item.id).toList();
    if(products.length == 0 || products.length < 10){
      hasMoreWishList = false;
      _hasMoreWishListFetcher.sink.add(hasMoreWishList);
    }
  }

  void removeItemFromWishList(int id) {
    wishListProducts.removeWhere((item) => item.id == id);
    _wishListFetcher.sink.add(wishListProducts);
    apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-remove_wishlist' , {'product_id': id.toString()});
  }

  void addToWishList(int id) {
      apiProvider.post(
          '/wp-admin/admin-ajax.php?action=mstore_flutter-add_wishlist' , {'product_id': id.toString()});
  }

  Future increaseQty(String key, int quantity) async {
    for (var i = 0; i < shoppingCart.cartContents.length; i++) {
      if(shoppingCart.cartContents[i].key == key){
        shoppingCart.cartContents[i].loadingQty = true;
        _cartFetcher.sink.add(shoppingCart);
        break;
      }
    }
    quantity = quantity + 1;
    var formData = new Map<String, String>();
    formData['cart[' + key + '][qty]'] = quantity.toString();
    formData['_wpnonce'] = shoppingCart.cartNonce;
    formData['update_cart'] = 'Update Cart';
    formData['_wp_http_referer'] = apiProvider.url + '/wp-admin/admin-ajax.php?action=mstore_flutter-cart';
    final response = await apiProvider.post(
        '/index.php/cart/', formData);
    if (response.statusCode == 200 || response.statusCode == 302) {
      getCart();
    } else {
      throw Exception('Failed to load cart');
    }
  }

  Future decreaseQty(String key, int quantity) async {
    for (var i = 0; i < shoppingCart.cartContents.length; i++) {
      if(shoppingCart.cartContents[i].key == key){
        shoppingCart.cartContents[i].loadingQty = true;
        _cartFetcher.sink.add(shoppingCart);
        break;
      }
    }
    quantity = quantity - 1;
    var formData = new Map<String, String>();
    formData['cart[' + key + '][qty]'] = quantity.toString();
    formData['_wpnonce'] = shoppingCart.cartNonce;
    formData['update_cart'] = 'Update Cart';
    formData['_wp_http_referer'] = apiProvider.url + '/wp-admin/admin-ajax.php?action=mstore_flutter-cart';
    final response = await apiProvider.post(
        '/index.php/cart/', formData);
    if (response.statusCode == 200 || response.statusCode == 302) {
      getCart();
    } else {
      throw Exception('Failed to load cart');
    }
  }

  void chooseShipping(String value) {
    orderReviewData.shipping[0].chosenMethod = value;
    _orderReviewFetcher.sink.add(orderReviewData);
  }

  void choosePayment(String value) {
    for(var i= 0; i < orderReviewData.paymentMethods.length; i++) {
      orderReviewData.paymentMethods[i].chosen = false;
    }
    for(var i= 0; i < orderReviewData.paymentMethods.length; i++) {
      if(orderReviewData.paymentMethods[i].id == value){
        orderReviewData.paymentMethods[i].chosen = true;
      }
    }
  }

  void addAddToCarErrorMessage(String message) {
    AddToCartErrorModel addToCartError = new AddToCartErrorModel();
    addToCartError.data = new AddToCartErrorData();
    addToCartError.data.notice = message;
    _addToCartErrorFetcher.sink.add(addToCartError);
  }

  //Account
  Customer user = new Customer();
  List<Order> orders;
  bool loggedIn = true;
  bool hasMoreOrders = true;
  int ordersPage = 0;
  var addressFormData = new Map<String, String>();

  final _errorFetcher = BehaviorSubject<WpErrors>();
  final _registerErrorFetcher = BehaviorSubject<RegisterError>();
  final _isLoginLoadingFetcher = BehaviorSubject<String>();
  var _hasMoreOrdersFetcher = BehaviorSubject<bool>();

  Observable<WpErrors> get error => _errorFetcher.stream;
  Observable<String> get isLoginLoading => _isLoginLoadingFetcher.stream;
  Observable<RegisterError> get registerError => _registerErrorFetcher.stream;
  Observable<bool> get hasMoreOrderItems => _hasMoreOrdersFetcher.stream;

  final _ordersFetcher = BehaviorSubject<List<Order>>();
  Observable<List<Order>> get allOrders => _ordersFetcher.stream;

  final _customersFetcher = BehaviorSubject<Customer>();
  Observable<Customer> get customerDetail => _customersFetcher.stream;

  getOrders() async {
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-orders',  Map());
    orders = orderFromJson(response.body);
    _ordersFetcher.sink.add(orders);
    _hasMoreOrdersFetcher.sink.add(true);
  }

  void loadMoreOrders()  async{
    ordersPage = ordersPage + 1;
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-orders', {'page': ordersPage.toString()});
    List<Order> moreOrders = orderFromJson(response.body);
    orders.addAll(moreOrders);
    _ordersFetcher.sink.add(orders);
    if(moreOrders.length == 0) {
      hasMoreOrders = false;
      _hasMoreOrdersFetcher.sink.add(false);
    }
  }

  getCustomerDetails() async {
    final response = await apiProvider.postWithCookies(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-customer', new Map());
    Customer customers = Customer.fromJson(json.decode(response.body));
    _customersFetcher.sink.add(customers);
  }

  Future updateAddress() async {
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-update-address', addressFormData);
    getCustomerDetails();
  }

  Future<Customer> login(data) async {
    _isLoginLoadingFetcher.sink.add('true');
    final response = await apiProvider.postWithCookies(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-login', data);
    _isLoginLoadingFetcher.sink.add('false');
    if (response.statusCode == 200) {
      user = Customer.fromJson(json.decode(response.body));
      _customersFetcher.sink.add(user);
      _isLoginLoadingFetcher.sink.add('unknown');
      return user;
    } else if (response.statusCode == 400) {
      WpErrors error = WpErrors.fromJson(json.decode(response.body));
      _errorFetcher.sink.add(error);
      throw Exception('Failed to login');
    } else {
      throw Exception('Failed to login');
    }
  }

  Future<Customer> register(data) async {
    _isLoginLoadingFetcher.sink.add('true');
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-create-user', data);
    if (response.statusCode == 200) {
      _isLoginLoadingFetcher.sink.add('false');
      Customer user = Customer.fromJson(json.decode(response.body));
      _customersFetcher.sink.add(user);
      _isLoginLoadingFetcher.sink.add('unknown');
      return user;
    } else if (response.statusCode == 400) {
      _isLoginLoadingFetcher.sink.add('false');
      RegisterError error = RegisterError.fromJson(json.decode(response.body));
      _registerErrorFetcher.sink.add(error);
      throw Exception('Failed to register');
    } else {
      _isLoginLoadingFetcher.sink.add('false');
      throw Exception('Failed to register');
    }
  }

  Future<Customer> googleLogin(data) async {
    _isLoginLoadingFetcher.sink.add('true');
    final response = await apiProvider.postWithCookies(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-google_login', data);
    _isLoginLoadingFetcher.sink.add('false');
    if (response.statusCode == 200) {
      user = Customer.fromJson(json.decode(response.body));
      _customersFetcher.sink.add(user);
      _isLoginLoadingFetcher.sink.add('unknown');
      return user;
    } else if (response.statusCode == 400) {
      WpErrors error = WpErrors.fromJson(json.decode(response.body));
      _errorFetcher.sink.add(error);
      throw Exception('Failed to login');
    } else {
      throw Exception('Failed to login');
    }
  }

  Future<Customer> facebooklogin(token) async {
    _isLoginLoadingFetcher.sink.add('true');
    final response = await apiProvider.postWithCookies(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-facebook_login', {'access_token': token});
    _isLoginLoadingFetcher.sink.add('false');
    if (response.statusCode == 200) {
      user = Customer.fromJson(json.decode(response.body));
      _customersFetcher.sink.add(user);
      _isLoginLoadingFetcher.sink.add('unknown');
      return user;
    } else if (response.statusCode == 400) {
      WpErrors error = WpErrors.fromJson(json.decode(response.body));
      _errorFetcher.sink.add(error);
      throw Exception('Failed to login');
    } else {
      throw Exception('Failed to login');
    }
  }

  Future logout() async {
    user = new Customer();
    wishListIds = [];
    wishListProducts = [];
    _wishListFetcher.sink.add(wishListProducts);
    user.id = 0;
    _customersFetcher.sink.add(user);
    final response = await apiProvider.get(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-logout');
  }

  Future processCredimaxPayment(String redirect) async {
    _placingOrderFetcher.sink.add(true);
    int pos1 = redirect.lastIndexOf("&order=");
    int pos2 = redirect.lastIndexOf("&key=wc_order");
    final orderId = redirect.substring(pos1 + 7, pos2);

    final orderResponse = await apiProvider.post(
        '/wp-admin/admin-ajax.php?action=mstore_flutter-order', {'id': orderId});
    Order newOrder = Order.fromJson(json.decode(orderResponse.body));

    int pos3 = redirect.lastIndexOf("sessionId=");
    int pos4 = redirect.lastIndexOf("&order=");
    String sessionId = redirect.substring(pos3 + 10, pos4);

    String body = '';
    body = 'merchant=E14560950&order.id='+orderId+'&order.amount='+newOrder.total+'&order.currency='+newOrder.currency+'&order.description=Pay+for+order+%23'+orderId+'+via+Credit+Card&order.customerOrderDate=2019-11-17&order.customerReference='+newOrder.customerId.toString()+'&order.reference='+orderId+'&session.id=' + sessionId + '&billing.address.city='+newOrder.billing.city+'&billing.address.country=BHR&billing.address.postcodeZip='+newOrder.billing.postcode+'&billing.address.stateProvince='+newOrder.billing.state+'&billing.address.street='+newOrder.billing.address1+'&billing.address.street2='+newOrder.billing.address2+'&customer.email='+newOrder.billing.email+'&customer.firstName='+newOrder.billing.firstName+'&customer.lastName='+newOrder.billing.lastName+'&customer.phone='+newOrder.billing.phone+'&interaction.merchant.name=Awal+Pets&interaction.merchant.address.line1=Manama&interaction.merchant.address.line2=Bahrain&interaction.displayControl.billingAddress=HIDE&interaction.displayControl.customerEmail=HIDE&interaction.displayControl.orderSummary=HIDE&interaction.displayControl.shipping=HIDE&interaction.cancelUrl=https%3A%2F%2Fawalpets.com%2Fcheckout%2F';

    final response = await apiProvider.processCredimaxPayment(body);
    _placingOrderFetcher.sink.add(false);
    return true;
  }

  Future<bool> switchCurrency(String s) async {
    print(s);
    final response = await apiProvider.post(
        '/wp-admin/admin-ajax.php', {'action': 'wcml_switch_currency', 'currency': s, 'force_switch': '0'});
    print(response.body);
    return true;
  }



  // For Deals

  List<Product> dealsProducts;
  var dealsFilter = new Map<String, dynamic>();
  int dealsPage = 0;

  final _dealsProductsFetcher = BehaviorSubject<List<Product>>();
  final _hasMoreDealsFetcher = BehaviorSubject<bool>();
  final _isLoadingDealsFetcher = BehaviorSubject<bool>();

  Observable<List<Product>> get allDealsProducts => _dealsProductsFetcher.stream;
  Observable<bool> get hasMoreDealsItems => _hasMoreDealsFetcher.stream;
  Observable<bool> get isLoadingDealsProducts => _isLoadingDealsFetcher.stream;

  fetchAllProducts([String query]) async {
    dealsPage = dealsPage + 1;
    dealsFilter['on_sale'] = '1';
    dealsFilter['random'] = 'rand';
    dealsFilter['page'] = dealsPage.toString();
    _hasMoreDealsFetcher.sink.add(true);
    _isLoadingDealsFetcher.sink.add(true);
    _dealsProductsFetcher.sink.add([]);
    dealsProducts = await apiProvider.fetchProductList(dealsFilter);
    _dealsProductsFetcher.sink.add(dealsProducts);
    _isLoadingDealsFetcher.sink.add(false);
    if(dealsProducts.length < 10){
      _hasMoreDealsFetcher.sink.add(false);
    }
  }

  loadMore() async {
    dealsPage = dealsPage + 1;
    dealsFilter['page'] = dealsPage.toString();
    List<Product> moreProducts = await apiProvider.fetchProductList(dealsFilter);
    dealsProducts.addAll(moreProducts);
    _dealsProductsFetcher.sink.add(dealsProducts);
    if(moreProducts.length < 10){
      _hasMoreDealsFetcher.sink.add(false);
    }
  }

  Future<StripeTokenModel> getStripeToken(Map<String, dynamic> stripeTokenParams) async {
    final response = await apiProvider.posAjax(
     'https://api.stripe.com/v1/tokens', stripeTokenParams);
    if (response.statusCode == 200) {
      StripeTokenModel stripeToken = StripeTokenModel.fromJson(json.decode(response.body));
      return stripeToken;
    } else {

    }
  }

  Future<StripeSourceModel> getStripeSource(Map<String, dynamic> stripeSourceParams) async {
    final response = await apiProvider.posAjax(
    'https://api.stripe.com/v1/sources', stripeSourceParams);
    if (response.statusCode == 200) {
      StripeSourceModel stripeSource = StripeSourceModel.fromJson(json.decode(response.body));
      return stripeSource;
    } else {

    }
  }

}

String _parseHtmlString(String htmlString) {
  var document = parse(htmlString);

  String parsedString = parse(document.body.text).documentElement.text;

  return parsedString;
}
