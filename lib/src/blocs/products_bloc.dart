import 'dart:convert';
import './../models/attributes_model.dart';
import './../resources/api_provider.dart';

import 'package:rxdart/rxdart.dart';
import '../models/product_model.dart';

class ProductsBloc {

  List<Product> products;
  int page = 0;
  var filter = new Map<String, dynamic>();
  var selectedRange;

  final apiProvider = ApiProvider();
  final _productsFetcher = PublishSubject<List<Product>>();
  final _attributesFetcher = BehaviorSubject<List<AttributesModel>>();
  final _hasMoreItemsFetcher = BehaviorSubject<bool>();
  final _isLoadingProductsFetcher = BehaviorSubject<bool>();

  String search="";

  Observable<List<Product>> get allProducts => _productsFetcher.stream;
  Observable<List<AttributesModel>> get allAttributes => _attributesFetcher.stream;
  Observable<bool> get hasMoreItems => _hasMoreItemsFetcher.stream;
  Observable<bool> get isLoadingProducts => _isLoadingProductsFetcher.stream;

  List<AttributesModel> attributes;

  fetchAllProducts([String query]) async {
    _hasMoreItemsFetcher.sink.add(true);
    _isLoadingProductsFetcher.sink.add(true);
    _productsFetcher.sink.add([]);
    page = page + 1;
    filter['page'] = page.toString();
    products = await apiProvider.fetchProductList(filter);
    _productsFetcher.sink.add(products);
    _isLoadingProductsFetcher.sink.add(false);
    if(products.length < 10){
      _hasMoreItemsFetcher.sink.add(false);
    }
  }

  loadMore() async {
    page = page + 1;
    filter['page'] = page.toString();
    List<Product> moreProducts = await apiProvider.fetchProductList(filter);
    products.addAll(moreProducts);
    _productsFetcher.sink.add(products);
    if(moreProducts.length < 10){
      _hasMoreItemsFetcher.sink.add(false);
    }
  }

  dispose() {
    _productsFetcher.close();
    _attributesFetcher.close();
    _hasMoreItemsFetcher.close();
  }

  Future fetchProductsAttributes() async {
    final response = await apiProvider.post('/wp-admin/admin-ajax.php?action=mstore_flutter-product-attributes', {'category': filter['id'].toString()});
    if (response.statusCode == 200) {
      attributes = filterModelFromJson(response.body);
      _attributesFetcher.sink.add(attributes);
    } else {
      throw Exception('Failed to load attributes');
    }
  }

  void clearFilter() {
    for(var i = 0; i < attributes.length; i++) {
      for(var j = 0; j < attributes[i].terms.length; j++) {
        attributes[i].terms[j].selected = false;
      }
    }
    _attributesFetcher.sink.add(attributes);
    fetchAllProducts();
  }

  void applyFilter(double minPrice, double maxPrice) {
    page = 0;
    filter = new Map<String, dynamic>();
    filter['min_price'] = minPrice.toString();
    filter['max_price'] = maxPrice.toString();
    for(var i = 0; i < attributes.length; i++) {
      for(var j = 0; j < attributes[i].terms.length; j++) {
        if(attributes[i].terms[j].selected) {
          filter['attribute_term' + j.toString()] = attributes[i].terms[j].termId.toString();
          filter['attributes' + j.toString()] = attributes[i].terms[j].taxonomy;
        }
      }
    }
    fetchAllProducts();
  }

  void updateCategory() {
    page = 0;
    filter = new Map<String, dynamic>();
    fetchAllProducts();
  }
}