import 'package:flutter/material.dart';
import 'package:woocommerce/Ourview.dart';
import 'package:woocommerce/src/app.dart';
import 'package:woocommerce/src/blocs/home_bloc.dart';
import 'package:woocommerce/src/ui/options.dart';

import 'src/models/UserRepo.dart';

class SplashWidget extends StatefulWidget {
  final HomeBloc homeBloc;
  final GalleryOptions options;
  final ValueChanged<GalleryOptions> handleOptionsChanged;
  SplashWidget({Key key, this.homeBloc, this.options, this.handleOptionsChanged}) : super(key: key);

  @override
  _SplashWidgetState createState() => _SplashWidgetState();
}

class _SplashWidgetState extends State<SplashWidget> {

  @override
  void initState() {
    super.initState();
    timer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(context),
    );
  }

  _body(BuildContext context) {
    return Hero(
      tag: 'logo',
      child: Center(
        child: Image.asset('lib/assets/images/dlogo.png'),
      ),
    );
  }

  Future<void> timer() async {
    var user = await UserRepo().getUser();
    await Future.delayed(Duration(seconds: 4));
//  Navigator.push(context, MaterialPageRoute(builder: (context)=>App(homeBloc: HomeBloc())));
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> OurView(homeBloc: widget.homeBloc,options:widget.options,handleOptionsChanged: widget.handleOptionsChanged,user : user)));
    //navigate(context: context,screen: MyHomePage());
  }
}

