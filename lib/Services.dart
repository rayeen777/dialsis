import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:woocommerce/src/blocs/home_bloc.dart';
import 'package:woocommerce/src/ui/accounts/account/account.dart';
import 'package:woocommerce/src/ui/home/drawer/drawer.dart';
import 'package:woocommerce/src/ui/options.dart';


class Service extends StatefulWidget {
  final HomeBloc homeBloc;
  final GalleryOptions options;
  final ValueChanged<GalleryOptions> handleOptionsChanged;
  Service({Key key, this.homeBloc, this.options, this.handleOptionsChanged}) : super(key: key);

  @override
  _ServiceState createState() => _ServiceState();
}

class _ServiceState extends State<Service> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Map data;
  List image;


  Future getData() async {
    http.Response response = await http.get("https://live.maktechgroup.in/services/index.php?api=1");
    data = jsonDecode(response.body);

    setState(() {
      image = data["hits"];
    });

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {


      return
        Scaffold(
          key: _scaffoldKey,
          drawer: MyDrawer(homeBloc: widget.homeBloc, options: widget.options, handleOptionsChanged: widget.handleOptionsChanged),
          appBar: AppBar(
            leading: Builder(
                builder: (BuildContext context){
                  return IconButton(
                    icon: Icon(Icons.menu,color: Colors.black),
                    onPressed: () =>_scaffoldKey.currentState.openDrawer(),
                  );
                }),
            title: Text("Dial SIS",
              style: TextStyle(color: Colors.black),
            ),


            backgroundColor: Colors.white,
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.message,color: Colors.black),
                onPressed: () => _openWhatsApp(
                    '+917007229963','Hi I need Help From DialSIS'),
              ),

              IconButton(
                icon: Icon(Icons.person_outline,color: Colors.black),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            UserAccount(options: widget.options,
                                handleOptionsChanged: widget.handleOptionsChanged,
                                homeBloc: widget.homeBloc),
                      ));
                },
              ),

            ],
          ),
        body: ListView.builder(
          itemCount: image == null ? 0 : image.length,
          itemBuilder: (context, index) {
            return Card( //                           <-- Card widget
              child: ListTile(
                leading: Image.network('https://live.maktechgroup.in/services/uploads/'+image[index]['description'],
                  width: 50.0,
                ),
                title: Text(image[index]['title']),
                onTap: () => _openWhatsApp(
                    '+917007229963','I am Looking For '+image[index]['title']+' Services From Dialsis'),
              ),
            );
          },
        ),

      );
  }
}




// ignore: non_constant_identifier_names
Future _openWhatsApp(String number,String Text) async {


  final url = "https://wa.me/" + number +"?text=" +Text;
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}